﻿var initializeFormValidate = function () {
    // Initialize form validation on the registration form.
    // It has the name attribute "registration"
    $("form[name='form-reset-password']").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
            newPassword: {
                required: true,
                minlength: 5
            },
            confirmPassword: {
                required: true,
                minlength: 5,
                equalTo: "#newPassword"
            }
        },
        // Specify validation error messages
        messages: {
            newPassword: {
                required: "Please provide a new password",
                minlength: "Your password must be at least 5 characters long"
            },
            confirmPassword: {
                required: "Please provide a new password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Password not match. Please check again"
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            WEBPORTAL.Utility.ShowLoading($(".reset-btn"));

            setTimeout(function () {
                $.when(submitPassword()).done(function (data) {
                    WEBPORTAL.Utility.RemoveLoading($(".reset-btn span"));
                });
            }, 500);
        }
    });
};

var submitPassword = function () {
    var deferred = $.Deferred();

    var token = document.getElementById('ViewData_token_').value;
    var newPassword = document.getElementById('newPassword').value;

    var getParams = {
        "token": token,
        "newPassword": newPassword
    };

    $.ajax({
        type: "POST",
        traditional: true,
        async: false,
        cache: false,
        url: '/Account/NewPassword',
        context: document.body,
        data: getParams,
        success: function (result, status, xhr) {
            if (result.code === 200) {
                window.location.href = "/Account/Login";
            } else if (result.code === 202) {
                formMessageHandler(result.message);
            }
            deferred.resolve(result);
        },
        error: function (result, status, xhr) {
            deferred.reject(result);
            formMessageHandler(result);
        }
    });

    return deferred.promise();
};

var formMessageHandler = function(msg) {
    $('<label for="wrap-msg-global" generated="true" class="error">' + msg + '</label>').insertAfter("#wrap-msg-global");
}

$(function () {
    initializeFormValidate();
});