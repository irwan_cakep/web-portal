﻿(function ($) {
    'use strict';

    var initialBankPayment = (function () {
        let grid, bankAccountForm, btnSaveBankAccount, modalBankAccount,
            masterDataBankId, area, masterDataCurrencyId, city, accountName, accountNumber,
            lblMasterDataBankId, lblArea, lblMasterDataCurrencyId, lblCity, lblAccountName, lblAccountNumber;

        var initialDom = function () {
            grid = $('#grid');
            bankAccountForm = $('#vendor-frm-bank-account');
            btnSaveBankAccount = $('#btn-save-bank-account');
            modalBankAccount = $('#addbankModal');
            masterDataBankId = $('#MasterDataBankId');
            area = $('#Area');
            masterDataCurrencyId = $('#MasterDataCurrencyId');
            city = $('#City');
            accountName = $('#AccountName');
            accountNumber = $('#AccountNumber');
            lblMasterDataBankId = $('#lblMasterDataBankId');
            lblMasterDataCurrencyId = $('#lblMasterDataCurrencyId');
            lblArea = $('#lblArea');
            lblCity = $('#lblCity');
            lblAccountName = $('#lblAccountName');
            lblAccountNumber = $('#lblAccountNumber');
        };

        var initialGrid = function () {
            grid.DataTable({
                ajax: { url: "/Vendor/GetVendorBankAccount", dataSrc: '' },
                //processing: true,
                //serverSide: true,
                searching: false,
                //info: false,
                lengthChange: false,
                columns: [
                    { data: "id", title: "Id", visible: false },
                    { data: "currency", title: "Currency" },
                    { data: "bankName", title: "Bank Name" },
                    { data: "branch", title: "Branch" },
                    { data: "city", title: "City" },
                    { data: "accountNumber", title: "Account Number" },
                    { data: "accountName", title: "Account Name" },
                    {
                        data: "status",
                        title: "",
                        createdCell: function (td, cellData, rowData, row, col) {
                            console.log(cellData);
                            switch (cellData) {
                                case "Main":
                                    $(td).html('<span class="badge badge-success">Rekening Utama</span>');
                                    break;
                                default:
                                    $(td).html('<span class="badge badge-light">Jadikan Utama</span>');
                                    break;
                            }
                        }
                    },
                    {
                        data: null,
                        title: "",
                        createdCell: function (td, cellData, rowData, row, col) {
                            $(td).html('<div class="button-list">'
                                + '<a href="#" data-toggle="modal" data-target="#editbankModal" class="btn btn-icon waves-effect waves-light btn-custom"> <i class="fa fa-edit"></i></a>'
                                + '<a href="#" class="btn btn-icon waves-effect waves-light btn-custom"> <i class="fa fa-trash"></i></a>'
                                + '</div>');
                        }
                    }
                ]
            });
        };

        var submitBankAccount = function () {
            btnSaveBankAccount.click(function () {
                WEBPORTAL.Utility.SubmitLoading(btnSaveBankAccount);
                Array.prototype.filter.call(bankAccountForm, function (form) {
                    if (form.checkValidity() === false) {
                        form.classList.add('was-validated');
                        formValidation();

                        setTimeout(function () {
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnSaveBankAccount);
                        }, 300);
                    } else {
                        form.classList.add('was-validated');

                        if (formValidation()) {
                            let model = {
                                MasterDataCurrencyId: masterDataCurrencyId.val(),
                                MasterDataBankId: masterDataBankId.val(),
                                Area: area.val(),
                                City: city.val(),
                                AccountNumber: accountNumber.val(),
                                AccountName: accountName.val()
                            };

                            $.when(WEBPORTAL.Services.POSTLocal(model, URL_CONTEXT_SaveVendorBankAccount)).done(function (result, status, xhr) {
                                if (result.code === 200) {
                                    setTimeout(function () {
                                        $.when(modalBankAccount.modal('hide')).then(resetBankAccountForm());
                                        WEBPORTAL.Utility.SubmitRemoveLoading(btnSaveBankAccount);
                                        WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                                        reloadDataSource();
                                    }, 500);
                                } else {
                                    setTimeout(function () {
                                        modalBankAccount.modal('hide');
                                        WEBPORTAL.Utility.SubmitRemoveLoading(btnSaveBankAccount);
                                        WEBPORTAL.Utility.ConstructNotificationError(result.message);
                                    }, 500);                                   
                                }                               
                            });
                        }                       
                    }                 
                });
            });
        };

        var reloadDataSource = function () {
            //var grid = $('#grid').DataTable({
            //    ajax: { url: "/Vendor/GetVendorBankAccount", dataSrc: '' },
            //});

            grid.DataTable().ajax.reload(); // user paging is not reset on reload
        };

        var formValidation = function () {
            let isBankName = false, isArea = false, isCurrency = false, isCity = false, isAccountName = false, isAccountNumber = false, isFormValid = false;

            // Bank Name
            if (masterDataBankId.val() === '' || masterDataBankId.val() === null) {
                if ($('.bp_slct_err_bank_name').length == 0) {
                    lblMasterDataBankId.after('<div class="invalid-feedback bp_slct_err_bank_name">' + 'Please select one.' + '</div>');

                    isBankName = false;
                }
            } else {
                $('.bp_slct_err_bank_name').remove();
                $('.select-wrapper.mdb-select.md-form.md-outline.select-city input').attr('required', false);

                isBankName = true;
            }

            // Area
            if (area.val() === '' || area.val() === null) {
                if ($('.bp_inpt_err_area').length == 0) {
                    lblArea.after('<div class="invalid-feedback bp_inpt_err_area">' + 'Please fill the required field.' + '</div>');

                    isArea = false;
                }
            } else {
                $('.bp_inpt_err_area').remove();

                isArea = true;
            }

            // Currency
            if (masterDataCurrencyId.val() === '' || masterDataCurrencyId.val() === null) {
                if ($('.bp_slct_err_currency').length == 0) {
                    lblMasterDataCurrencyId.after('<div class="invalid-feedback bp_slct_err_currency">' + 'Please select one.' + '</div>');

                    isCurrency = false;
                }
            } else {
                $('.bp_slct_err_currency').remove();
                $('.select-wrapper.mdb-select.md-form.md-outline.select-city input').attr('required', false);

                isCurrency = true;
            }

            // City
            if (city.val() === '' || city.val() === null) {
                if ($('.bp_inpt_err_city').length == 0) {
                    lblCity.after('<div class="invalid-feedback bp_inpt_err_city">' + 'Please fill the required field.' + '</div>');

                    isCity = false;
                }
            } else {
                $('.bp_inpt_err_city').remove();

                isCity = true;
            }

            // Account Name
            if (accountName.val() === '' || accountName.val() === null) {
                if ($('.bp_inpt_err_account_name').length == 0) {
                    lblAccountName.after('<div class="invalid-feedback bp_inpt_err_account_name">' + 'Please fill the required field.' + '</div>');

                    isAccountName = false;
                }
            } else {
                $('.bp_inpt_err_account_name').remove();

                isAccountName = true;
            }

            // Account Number
            if (accountNumber.val() === '' || accountNumber.val() === null) {
                if ($('.bp_inpt_err_account_number').length == 0) {
                    lblAccountNumber.after('<div class="invalid-feedback bp_inpt_err_account_number">' + 'Please fill the required field.' + '</div>');

                    isAccountNumber = false;
                }
            } else {
                $('.bp_inpt_err_account_number').remove();

                isAccountNumber = true;
            }

            if (isBankName && isArea && isCurrency && isCity && isAccountName && isAccountNumber)
                isFormValid = true;
            else
                isFormValid = false;

            return isFormValid;
        };

        var onChangeBehaviour = function () {
            masterDataBankId.change(function (e) {
                if (this.value !== '' || this.valuee !== null)
                    $('.bp_slct_err_bank_name').remove();
            });

            masterDataCurrencyId.change(function (e) {
                if (this.value !== '' || this.valuee !== null)
                    $('.bp_slct_err_currency').remove();
            });
        };

        var resetBankAccountForm = function () {
            masterDataBankId.val('');
            area.val('');
            masterDataCurrencyId.val('');
            city.val('');
            accountName.val('');
            accountNumber.val('');

            lblArea.removeClass('active');
            lblCity.removeClass('active');
            lblAccountName.removeClass('active');
            lblAccountNumber.removeClass('active');

            bankAccountForm.removeClass('was-validated');
        }

        var run = function () {
            initialDom();
            initialGrid();
            onChangeBehaviour();
            submitBankAccount();
        };

        return {
            run: run
        };
    })();

    initialBankPayment.run();
})(jQuery);