#pragma checksum "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\Vendor\Sent.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5e513d61454f40e151beaf487640ec01b720f5e3"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Vendor_Sent), @"mvc.1.0.view", @"/Views/Vendor/Sent.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Vendor/Sent.cshtml", typeof(AspNetCore.Views_Vendor_Sent))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\_ViewImports.cshtml"
using WebPortal;

#line default
#line hidden
#line 2 "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\_ViewImports.cshtml"
using WebPortal.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5e513d61454f40e151beaf487640ec01b720f5e3", @"/Views/Vendor/Sent.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"73163c1d5b532b31d8db16d15c0af3b6f6d234b6", @"/Views/_ViewImports.cshtml")]
    public class Views_Vendor_Sent : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\Vendor\Sent.cshtml"
  
    ViewData["Title"] = "Sent";

#line default
#line hidden
            BeginContext(42, 754, true);
            WriteLiteral(@"
<div class=""content"">
    <div class=""container-fluid"">

        <div class=""row"">
            <div class=""col-12"">
                <div>
                    <h4 class=""page-title-dashboard"">Sent</h4>
                    <ol class=""breadcrumb"">
                        <li class=""breadcrumb-item""><a href=""vendor-dashboard.html"">Dashboard</a></li>
                        <li class=""breadcrumb-item"">Message</li>
                        <li class=""breadcrumb-item active"">Sent</li>
                    </ol>
                </div>
            </div>

            <!-- Right Sidebar -->
            <div class=""col-lg-12"">
                <div class=""card-box shadow-sm"">
                    <!-- Left sidebar -->
                    ");
            EndContext();
            BeginContext(797, 72, false);
#line 25 "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\Vendor\Sent.cshtml"
               Write(await Html.PartialAsync("~/Views/Vendor/Partial/_SidebarMessage.cshtml"));

#line default
#line hidden
            EndContext();
            BeginContext(869, 6986, true);
            WriteLiteral(@"
                    <!-- End Left sidebar -->

                    <div class=""inbox-rightbar"">

                        <div class="""" role=""toolbar"">
                            <div class=""btn-group"">
                                <button type=""button"" class=""btn btn-sm btn-light waves-effect""><i class=""mdi mdi-email-open font-18 vertical-middle""></i></button>
                                <button type=""button"" class=""btn btn-sm btn-light waves-effect""><i class=""mdi mdi-delete-variant font-18 vertical-middle""></i></button>
                            </div>
                        </div>

                        <div class="""">
                            <div class=""mt-4"">
                                <div class="""">
                                    <ul class=""message-list"">
                                        <li>
                                            <div class=""col-mail col-mail-1"">
                                                <div class=""checkbox-wrapper-mail"">
");
            WriteLiteral(@"                                                    <input type=""checkbox"" id=""chk1"">
                                                    <label for=""chk1"" class=""toggle""></label>
                                                </div>
                                                <a href=""#"" class=""title"">FAP Group</a>
                                            </div>
                                            <div class=""col-mail col-mail-2"">
                                                <a href=""#"" class=""subject"">
                                                    Quotation Request &nbsp;&ndash;&nbsp;
                                                    <span class=""teaser"">Salam hormat, Sehubungan dengan pemintaan barang, kami dari PT AFP Group meminta Anda untuk mengirimkan</span>
                                                </a>
                                                <div class=""date"">11:49 am</div>
                                            </div>
                        ");
            WriteLiteral(@"                </li>

                                        <li>
                                            <div class=""col-mail col-mail-1"">
                                                <div class=""checkbox-wrapper-mail"">
                                                    <input type=""checkbox"" id=""chk3"">
                                                    <label for=""chk3"" class=""toggle""></label>
                                                </div>
                                                <a href=""#"" class=""title"">FAP Group</a>
                                            </div>
                                            <div class=""col-mail col-mail-2"">
                                                <a href=""#"" class=""subject"">
                                                    Quotation Request &nbsp;&ndash;&nbsp;
                                                    <span class=""teaser"">Salam hormat, Sehubungan dengan pemintaan barang, kami dari PT AFP Group meminta Anda unt");
            WriteLiteral(@"uk mengirimkan</span>
                                                </a>
                                                <div class=""date"">11:49 am</div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class=""col-mail col-mail-1"">
                                                <div class=""checkbox-wrapper-mail"">
                                                    <input type=""checkbox"" id=""chk4"">
                                                    <label for=""chk4"" class=""toggle""></label>
                                                </div>
                                                <a href=""#"" class=""title"">FAP Group</a>
                                            </div>
                                            <div class=""col-mail col-mail-2"">
                                                <a href=""#"" class=""subject"">
                    ");
            WriteLiteral(@"                                Quotation Request &nbsp;&ndash;&nbsp;
                                                    <span class=""teaser"">Salam hormat, Sehubungan dengan pemintaan barang, kami dari PT AFP Group meminta Anda untuk mengirimkan</span>
                                                </a>
                                                <div class=""date"">9 Jun</div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class=""col-mail col-mail-1"">
                                                <div class=""checkbox-wrapper-mail"">
                                                    <input type=""checkbox"" id=""chk5"">
                                                    <label for=""chk5"" class=""toggle""></label>
                                                </div>
                                                <a href=""#"" class=""title"">FAP Group</a>");
            WriteLiteral(@"
                                            </div>
                                            <div class=""col-mail col-mail-2"">
                                                <a href=""#"" class=""subject"">
                                                    Quotation Request &nbsp;&ndash;&nbsp;
                                                    <span class=""teaser"">Salam hormat, Sehubungan dengan pemintaan barang, kami dari PT AFP Group meminta Anda untuk mengirimkan</span>
                                                </a>
                                                <div class=""date"">8 Jun</div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                            </div> <!-- card body -->
                        </div> <!-- card -->

                        <div class=""row"">
                            <div class=""col-7"">
                          ");
            WriteLiteral(@"      Showing 1 - 1 of 4
                            </div>
                            <div class=""col-5"">
                                <div class=""btn-group float-right"">
                                    <button type=""button"" class=""btn btn-custom waves-light waves-effect btn-sm""><i class=""fa fa-chevron-left""></i></button>
                                    <button type=""button"" class=""btn btn-custom waves-effect waves-light btn-sm""><i class=""fa fa-chevron-right""></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class=""clearfix""></div>
                </div>

            </div> <!-- end Col -->

        </div><!-- End row -->

    </div> <!-- container -->

</div> <!-- content -->
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
