#pragma checksum "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\Vendor\Profile.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "de111f5a421ecc0cbd1c5462ae4d39d374ce3177"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Vendor_Profile), @"mvc.1.0.view", @"/Views/Vendor/Profile.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Vendor/Profile.cshtml", typeof(AspNetCore.Views_Vendor_Profile))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\_ViewImports.cshtml"
using WebPortal;

#line default
#line hidden
#line 2 "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\_ViewImports.cshtml"
using WebPortal.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"de111f5a421ecc0cbd1c5462ae4d39d374ce3177", @"/Views/Vendor/Profile.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"73163c1d5b532b31d8db16d15c0af3b6f6d234b6", @"/Views/_ViewImports.cshtml")]
    public class Views_Vendor_Profile : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\Vendor\Profile.cshtml"
  
    ViewData["Title"] = "Profile";

#line default
#line hidden
            BeginContext(45, 767, true);
            WriteLiteral(@"
<div class=""content"">
    <div class=""container-fluid"">

        <div class=""row"">
            <div class=""col-12"">
                <div>
                    <h4 class=""page-title-dashboard"">Profile</h4>
                    <ol class=""breadcrumb"">
                        <li class=""breadcrumb-item""><a href=""#"">Dashboard</a></li>
                        <li class=""breadcrumb-item active"">Profile</li>
                    </ol>
                </div>
            </div>
        </div>

        <div class=""row"">
            <div class=""col-12"">
                <div class=""card-box shadow-sm"">
                    <div class=""row position-relative float-right"">
                        <a class=""btn-floating btn-custom waves-effect waves-light""");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 812, "\"", 848, 1);
#line 25 "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\Vendor\Profile.cshtml"
WriteAttributeValue("", 819, Url.Action("Data", "Vendor"), 819, 29, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(849, 4934, true);
            WriteLiteral(@"><i class=""fa fa-edit"" aria-hidden=""true""></i></a>
                    </div>

                    <div class=""row"">
                        <div class=""col-xl-12"">
                            <h4 class=""text-muted mb-4""><i class=""fa fa-database pr-2""></i> Data Vendor</h4>
                        </div>
                        <div class=""col-xl-6"">
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Business Type</label>
                                <p class=""h6 col-12 m-1"">PT</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Company Name/Personal</label>
                                <p class=""h6 col-12 m-1"">Komputer</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Business Field</label>
                                <p class=""");
            WriteLiteral(@"col-12 m-1"">
                                    <ul class=""h6 m-1"">
                                        <li>01111 Pertanian Jagung</li>
                                        <li>02139 Pengusahaan Hutan Bukan Kayu Lainnya</li>
                                        <li>02202 Usaha Pemungutan Kayu</li>
                                        <li>01713 Perburuan dan Penangkapan Reptil</li>
                                        <li>02134 Pengusahaan Bambu</li>
                                    </ul>
                                </p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Address</label>
                                <p class=""h6 col-12 m-1"">Jalan Grogol No. 1 Jakarta Barat</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Mailing Address</label>
                                ");
            WriteLiteral(@"<p class=""h6 col-12 m-1"">Jalan Grogol No. 1 Jakarta Barat</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Country</label>
                                <p class=""h6 col-12 m-1"">Indonesia</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Province</label>
                                <p class=""h6 col-12 m-1"">DKI Jakarta</p>
                            </div>
                        </div>
                        <div class=""col-xl-6"">
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">City</label>
                                <p class=""h6 col-12 m-1"">Jakarta Barat</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Postal Code</label");
            WriteLiteral(@">
                                <p class=""h6 col-12 m-1"">17098</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Company Email</label>
                                <p class=""h6 col-12 m-1"">ptkomputer@admin.com</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Company Phone</label>
                                <p class=""h6 col-12 m-1"">021 123456</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Name</label>
                                <p class=""h6 col-12 m-1"">Marisaa</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">KTP Number</label>
                                <p class=""h6 col-12 m-1");
            WriteLiteral(@""">9987456524212455</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Email</label>
                                <p class=""h6 col-12 m-1"">admin@ptkomputer.com</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Mobile Number</label>
                                <p class=""h6 col-12 m-1"">081320123456</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""card-box shadow-sm"">
                    <div class=""row position-relative float-right"">
                        <a class=""btn-floating btn-custom waves-effect waves-light""");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 5783, "\"", 5826, 1);
#line 108 "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\Vendor\Profile.cshtml"
WriteAttributeValue("", 5790, Url.Action("BankPayment", "Vendor"), 5790, 36, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(5827, 3134, true);
            WriteLiteral(@"><i class=""fa fa-edit"" aria-hidden=""true""></i></a>
                    </div>

                    <div class=""row"">
                        <div class=""col-xl-6"">
                            <h4 class=""text-muted mb-4""><i class=""fa fa-money pr-2""></i> Bank Account</h4>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Currency</label>
                                <p class=""h6 col-12 m-1"">IDR</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Bank Name</label>
                                <p class=""h6 col-12 m-1"">BCA</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Branch</label>
                                <p class=""h6 col-12 m-1"">Grogol</p>
                            </div>
                            <div class=""form-grou");
            WriteLiteral(@"p"">
                                <label class=""col-12 m-1"">City/Country</label>
                                <p class=""h6 col-12 m-1"">Jakarta Barat</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Account Number</label>
                                <p class=""h6 col-12 m-1"">6546546879456</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Account Name</label>
                                <p class=""h6 col-12 m-1"">PT Vendor</p>
                            </div>
                        </div>
                        <div class=""col-xl-6"">
                            <h4 class=""text-muted mb-4""><i class=""fa fa-dollar-sign pr-2""></i> Payment</h4>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Term of Payment</label>
           ");
            WriteLiteral(@"                     <p class=""h6 col-12 m-1"">30 Days</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Down Payment</label>
                                <p class=""h6 col-12 m-1"">20%</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Full Payment</label>
                                <p class=""h6 col-12 m-1"">80%</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Retention</label>
                                <p class=""h6 col-12 m-1"">10%</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""card-box shadow-sm"">
                    <div class=""row position-relative float-right"">
                     ");
            WriteLiteral("   <a class=\"btn-floating btn-custom waves-effect waves-light\"");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 8961, "\"", 8998, 1);
#line 162 "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\Vendor\Profile.cshtml"
WriteAttributeValue("", 8968, Url.Action("Legal", "Vendor"), 8968, 30, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(8999, 3811, true);
            WriteLiteral(@"><i class=""fa fa-edit"" aria-hidden=""true""></i></a>
                    </div>
                    <div class=""row"">
                        <div class=""col-xl-12"">
                            <h4 class=""text-muted mb-4""><i class=""fa fa-database pr-2""></i> Legal</h4>
                        </div>
                        <div class=""col-xl-6"">
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">NIB Number</label>
                                <p class=""h6 col-12 m-1"">1259876532459</p>
                                <p class=""h6 col-12 m-1""><a href=""#"">View</a></p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Izin Usaha Number</label>
                                <p class=""h6 col-12 m-1"">654656546564</p>
                                <p class=""h6 col-12 m-1""><a href=""#"">View</a></p>
                            </div>
            ");
            WriteLiteral(@"                <div class=""form-group"">
                                <label class=""col-12 m-1"">SIUP Number</label>
                                <p class=""h6 col-12 m-1"">654656546564</p>
                                <p class=""h6 col-12 m-1"">20-12-2020</p>
                                <p class=""h6 col-12 m-1""><a href=""#"">View</a></p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">TDP Number</label>
                                <p class=""h6 col-12 m-1"">654656546564</p>
                                <p class=""h6 col-12 m-1""><a href=""#"">View</a></p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Akta Pendirian</label>
                                <p class=""h6 col-12 m-1"">654656546564</p>
                                <p class=""h6 col-12 m-1""><a href=""#"">View</a></p>
                ");
            WriteLiteral(@"            </div>
                        </div>
                        <div class=""col-xl-6"">
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">SK MenKunHam Pendirian</label>
                                <p class=""h6 col-12 m-1"">654656546564</p>
                                <p class=""h6 col-12 m-1""><a href=""#"">View</a></p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Directorship</label>
                                <p class=""h6 col-12 m-1"">John Doe</p>
                                <p class=""h6 col-12 m-1"">Direktur Utama</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Directorship</label>
                                <p class=""h6 col-12 m-1"">Jonathan Doe</p>
                                <p class=""h6 col-12 m-1"">Wa");
            WriteLiteral(@"kil Direktur Utama</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Signer</label>
                                <p class=""h6 col-12 m-1"">Kuasa Direktur</p>
                                <p class=""h6 col-12 m-1"">Monica</p>
                                <p class=""h6 col-12 m-1""><a href=""#"">View</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""card-box shadow-sm"">
                    <div class=""row position-relative float-right"">
                        <a class=""btn-floating btn-custom waves-effect waves-light""");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 12810, "\"", 12845, 1);
#line 223 "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\Vendor\Profile.cshtml"
WriteAttributeValue("", 12817, Url.Action("Tax", "Vendor"), 12817, 28, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(12846, 1810, true);
            WriteLiteral(@"><i class=""fa fa-edit"" aria-hidden=""true""></i></a>
                    </div>
                    <div class=""row"">
                        <div class=""col-xl-12"">
                            <h4 class=""text-muted mb-4""><i class=""fa fa-usd pr-2""></i> Value Added Tax</h4>
                        </div>
                        <div class=""col-xl-6"">
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">NPWP</label>
                                <p class=""h6 col-12 m-1"">1259876532459</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">SK PKP</label>
                                <p class=""h6 col-12 m-1"">1259876532459</p>
                            </div>
                        </div>
                        <div class=""col-xl-6"">
                            <div class=""form-group"">
                                <label class=""col-12 m-");
            WriteLiteral(@"1"">BKP</label>
                                <p class=""h6 col-12 m-1"">BKP Bebas PPN</p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">BKP Bebas PPN</label>
                                <p class=""h6 col-12 m-1"">654656546564</p>
                                <p class=""h6 col-12 m-1"">Jangat/kulit mentah yang tidak disamak (PP 81/2015 Pasal 1.2.c)</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""card-box shadow-sm"">
                    <div class=""row position-relative float-right"">
                        <a class=""btn-floating btn-custom waves-effect waves-light""");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 14656, "\"", 14703, 1);
#line 254 "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\Vendor\Profile.cshtml"
WriteAttributeValue("", 14663, Url.Action("MasterAgreement", "Vendor"), 14663, 40, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(14704, 1483, true);
            WriteLiteral(@"><i class=""fa fa-edit"" aria-hidden=""true""></i></a>
                    </div>
                    <div class=""row"">
                        <div class=""col-xl-12"">
                            <h4 class=""text-muted mb-4""><i class=""fa fa-gavel pr-2""></i> Agreement</h4>
                        </div>
                        <div class=""col-xl-6"">
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Integrity Pact</label>
                                <p class=""h6 col-12 m-1""><span class=""badge badge-success"">Agree</span></p>
                            </div>
                            <div class=""form-group"">
                                <label class=""col-12 m-1"">Confidentiality</label>
                                <p class=""h6 col-12 m-1""><span class=""badge badge-success"">Agree</span></p>
                            </div>
                        </div>
                        <div class=""col-xl-6"">
                            ");
            WriteLiteral(@"<div class=""form-group"">
                                <label class=""col-12 m-1"">Master Agreement</label>
                                <p class=""h6 col-12 m-1""><span class=""badge badge-success"">Agree</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div> <!-- container -->

</div> <!-- content -->
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
