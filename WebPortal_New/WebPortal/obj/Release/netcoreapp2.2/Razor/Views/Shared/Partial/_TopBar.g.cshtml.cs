#pragma checksum "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\Shared\Partial\_TopBar.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3478beacaadf632bcddda1404b2869e36cd30961"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_Partial__TopBar), @"mvc.1.0.view", @"/Views/Shared/Partial/_TopBar.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/Partial/_TopBar.cshtml", typeof(AspNetCore.Views_Shared_Partial__TopBar))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\_ViewImports.cshtml"
using WebPortal;

#line default
#line hidden
#line 2 "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\_ViewImports.cshtml"
using WebPortal.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3478beacaadf632bcddda1404b2869e36cd30961", @"/Views/Shared/Partial/_TopBar.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"73163c1d5b532b31d8db16d15c0af3b6f6d234b6", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_Partial__TopBar : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/fap-van/assets/images/logo-white.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString(""), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("height", new global::Microsoft.AspNetCore.Html.HtmlString("55"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/fap-van/assets/images/users/avatar-2.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("img-fluid rounded-circle"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("user"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("rounded-circle"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 169, true);
            WriteLiteral("<div class=\"topbar\">\r\n\r\n    <nav class=\"navbar-custom\">\r\n\r\n        <!-- LOGO -->\r\n        <div class=\"logo-box\">\r\n            <a href=\"#\" class=\"logo\">\r\n                ");
            EndContext();
            BeginContext(169, 73, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "3478beacaadf632bcddda1404b2869e36cd309616006", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(242, 4659, true);
            WriteLiteral(@"
            </a>
        </div>

        <ul class=""list-unstyled topbar-right-menu float-right mb-0"">

            <li class=""dropdown notification-list"">
                <a class=""nav-link dropdown-toggle arrow-none"" data-toggle=""dropdown"" href=""#"" role=""button""
                   aria-haspopup=""false"" aria-expanded=""false"">
                    <i class=""fi-bell noti-icon""></i>
                    <span class=""badge badge-notification badge-pill noti-icon-badge"">4</span>
                </a>
                <div class=""dropdown-menu dropdown-menu-right dropdown-menu-animated dropdown-lg"">
                    <!-- item-->
                    <div class=""dropdown-item noti-title"">
                        <h5 class=""m-0""><span class=""float-right""><a href="""" class=""text-dark""><small>Clear All</small></a> </span>Notification</h5>
                    </div>

                    <div class=""slimscroll"" style=""max-height: 230px;"">
                        <!-- item-->
                        <a ");
            WriteLiteral(@"href=""javascript:void(0);"" class=""dropdown-item notify-item"">
                            <div class=""notify-icon bg-success""><i class=""mdi mdi-comment-account-outline""></i></div>
                            <p class=""notify-details"">Complete your Data Vendor<small class=""text-muted"">1 min ago</small></p>
                        </a>

                        <!-- item-->
                        <a href=""javascript:void(0);"" class=""dropdown-item notify-item"">
                            <div class=""notify-icon bg-info""><i class=""mdi mdi-account-plus""></i></div>
                            <p class=""notify-details"">Your data is incomplete<small class=""text-muted"">5 hours ago</small></p>
                        </a>

                        <!-- item-->
                        <a href=""javascript:void(0);"" class=""dropdown-item notify-item"">
                            <div class=""notify-icon bg-danger""><i class=""mdi mdi-heart""></i></div>
                            <p class=""notify-details"">New Ord");
            WriteLiteral(@"er<small class=""text-muted"">3 days ago</small></p>
                        </a>

                        <!-- item-->
                        <a href=""javascript:void(0);"" class=""dropdown-item notify-item"">
                            <div class=""notify-icon bg-success""><i class=""mdi mdi-comment-account-outline""></i></div>
                            <p class=""notify-details"">Complete your Data Vendor<small class=""text-muted"">1 min ago</small></p>
                        </a>

                        <!-- item-->
                        <a href=""javascript:void(0);"" class=""dropdown-item notify-item"">
                            <div class=""notify-icon bg-info""><i class=""mdi mdi-account-plus""></i></div>
                            <p class=""notify-details"">Your data is incomplete<small class=""text-muted"">5 hours ago</small></p>
                        </a>

                        <!-- item-->
                        <a href=""javascript:void(0);"" class=""dropdown-item notify-item"">
            ");
            WriteLiteral(@"                <div class=""notify-icon bg-danger""><i class=""mdi mdi-heart""></i></div>
                            <p class=""notify-details"">New Order<small class=""text-muted"">3 days ago</small></p>
                        </a>
                    </div>

                    <!-- All-->
                    <a href=""javascript:void(0);"" class=""dropdown-item text-center text-primary notify-item notify-all"">
                        View all <i class=""fi-arrow-right""></i>
                    </a>

                </div>
            </li>

            <li class=""dropdown notification-list"">
                <a class=""nav-link dropdown-toggle arrow-none"" data-toggle=""dropdown"" href=""#"" role=""button""
                   aria-haspopup=""false"" aria-expanded=""false"">
                    <i class=""fi-speech-bubble noti-icon""></i>
                    <span class=""badge badge-notification badge-pill noti-icon-badge"">6</span>
                </a>
                <div class=""dropdown-menu dropdown-menu-righ");
            WriteLiteral(@"t dropdown-menu-animated dropdown-lg"">

                    <!-- item-->
                    <div class=""dropdown-item noti-title"">
                        <h5 class=""m-0""><span class=""float-right""><a href="""" class=""text-dark""><small>Clear All</small></a> </span>Inbox</h5>
                    </div>

                    <div class=""slimscroll"" style=""max-height: 230px;"">
                        <!-- item-->
                        <a href=""javascript:void(0);"" class=""dropdown-item notify-item"">
                            <div class=""notify-icon"">");
            EndContext();
            BeginContext(4901, 100, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3478beacaadf632bcddda1404b2869e36cd3096112284", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5001, 412, true);
            WriteLiteral(@" </div>
                            <p class=""notify-details"">PT Customer</p>
                            <p class=""text-muted font-13 mb-0 user-msg"">Hi, How are you? What about our products?</p>
                        </a>

                        <!-- item-->
                        <a href=""javascript:void(0);"" class=""dropdown-item notify-item"">
                            <div class=""notify-icon"">");
            EndContext();
            BeginContext(5413, 100, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3478beacaadf632bcddda1404b2869e36cd3096114043", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5513, 412, true);
            WriteLiteral(@" </div>
                            <p class=""notify-details"">PT Customer</p>
                            <p class=""text-muted font-13 mb-0 user-msg"">Hi, How are you? What about our products?</p>
                        </a>

                        <!-- item-->
                        <a href=""javascript:void(0);"" class=""dropdown-item notify-item"">
                            <div class=""notify-icon"">");
            EndContext();
            BeginContext(5925, 100, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3478beacaadf632bcddda1404b2869e36cd3096115802", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(6025, 412, true);
            WriteLiteral(@" </div>
                            <p class=""notify-details"">PT Customer</p>
                            <p class=""text-muted font-13 mb-0 user-msg"">Hi, How are you? What about our products?</p>
                        </a>

                        <!-- item-->
                        <a href=""javascript:void(0);"" class=""dropdown-item notify-item"">
                            <div class=""notify-icon"">");
            EndContext();
            BeginContext(6437, 100, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3478beacaadf632bcddda1404b2869e36cd3096117561", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(6537, 412, true);
            WriteLiteral(@" </div>
                            <p class=""notify-details"">PT Customer</p>
                            <p class=""text-muted font-13 mb-0 user-msg"">Hi, How are you? What about our products?</p>
                        </a>

                        <!-- item-->
                        <a href=""javascript:void(0);"" class=""dropdown-item notify-item"">
                            <div class=""notify-icon"">");
            EndContext();
            BeginContext(6949, 100, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3478beacaadf632bcddda1404b2869e36cd3096119320", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(7049, 798, true);
            WriteLiteral(@" </div>
                            <p class=""notify-details"">PT Customer</p>
                            <p class=""text-muted font-13 mb-0 user-msg"">Hi, How are you? What about our products?</p>
                        </a>
                    </div>

                    <!-- All-->
                    <a href=""javascript:void(0);"" class=""dropdown-item text-center text-primary notify-item notify-all"">
                        View all <i class=""fi-arrow-right""></i>
                    </a>

                </div>
            </li>

            <li class=""dropdown notification-list"">
                <a class=""nav-link dropdown-toggle nav-user"" data-toggle=""dropdown"" href=""#"" role=""button""
                   aria-haspopup=""false"" aria-expanded=""false"">
                    ");
            EndContext();
            BeginContext(7847, 92, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "3478beacaadf632bcddda1404b2869e36cd3096121478", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(7939, 898, true);
            WriteLiteral(@" <span class=""ml-1"">Admin Vendor <i class=""mdi mdi-chevron-down""></i> </span>
                </a>
                <div class=""dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown"">
                    <!-- item-->
                    <div class=""dropdown-item noti-title"">
                        <h6 class=""text-overflow m-0"">Welcome !</h6>
                    </div>

                    <!-- item-->
                    <a href=""dashboard-profile.html"" class=""dropdown-item notify-item"">
                        <i class=""fi-head""></i> <span>My Account</span>
                    </a>

                    <!-- item-->
                    <a href=""javascript:void(0);"" class=""dropdown-item notify-item"">
                        <i class=""fi-cog""></i> <span>Settings</span>
                    </a>

                    <!-- item-->
                    <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 8837, "\"", 8876, 1);
#line 152 "D:\KERJA\FIRST RESOURCES GROUP\WEB PORTAL\WebPortal_New\WebPortal\Views\Shared\Partial\_TopBar.cshtml"
WriteAttributeValue("", 8844, Url.Action("Logout", "Account"), 8844, 32, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(8877, 660, true);
            WriteLiteral(@" class=""dropdown-item notify-item"">
                        <i class=""fi-power""></i> <span>Logout</span>
                    </a>

                </div>
            </li>

        </ul>

        <ul class=""list-inline menu-left mb-0"">
            <li class=""float-left"">
                <button class=""button-menu-mobile open-left"">
                    <i class=""dripicons-menu""></i>
                </button>
            </li>
            <li>
                <div class=""page-title-box"">
                    <h4 class=""page-title""><span>FAP</span> Group</h4>
                </div>
            </li>
        </ul>

    </nav>

</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
