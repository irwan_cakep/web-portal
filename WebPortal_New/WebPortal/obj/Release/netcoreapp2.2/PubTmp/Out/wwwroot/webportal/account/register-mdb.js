﻿(function ($) {
    'use strict';

    var initalRegister = (function () {
        var registerForm, email, lblEmail, passwordRegister, lblPasswordRegister, confirmPassword, lblConfirmPassword, btnSubmitRegister;

        var registerDom = function () {
            registerForm = $('.form-register');
            email = $('#registerEmail');
            lblEmail = $('#lblRegisterEmail');
            passwordRegister = $('#registerPassword');
            lblPasswordRegister = $('#lblRegisterPassword');
            confirmPassword = $('#registerConfirmPassword');
            lblConfirmPassword = $('#lblRegisterConfirmPassword');
            btnSubmitRegister = $('.btn-submit-register');
        };

        var formValidation = function () {
            var isFormRegisterValid = false,
                isEmailRegister = false,
                isPasswordRegister = false,
                isConfirmPassword = false;

            /* email */
            if (email.val() == '') {
                if ($('.errRegisterEmail').length == 0) {
                    lblEmail.after('<div class="invalid-feedback errRegisterEmail">' + 'Please fill the required field.' + '</div>');
                    isEmailRegister = false;
                }
            } else if (!WEBPORTAL.Utility.CheckEmail(email.val())) {
                $('.errRegisterEmail').remove();
                lblEmail.after('<div class="invalid-feedback errRegisterEmail">' + 'Format email not valid.' + '</div>');

                isEmailRegister = false;
            }
            else {
                $('.errRegisterEmail').remove();

                isEmailRegister = true;
            }

            /* Password */
            if (passwordRegister.val() == '') {
                if ($('.errRegisterPassword').length == 0) {
                    lblPasswordRegister.after('<div class="invalid-feedback errRegisterPassword">' + 'Please fill the required field.' + '</div>');

                    isPasswordRegister = false;
                }
            } else if (passwordRegister.val().length < 5) {
                $('.errRegisterPassword').remove();
                passwordRegister.removeClass('valid override-input-suc').addClass('invalid override-input-err');
                lblPasswordRegister.after('<div class="invalid-feedback errRegisterPassword">' + 'Password minimum 3 characters length.' + '</div>');

                isPasswordRegister = false;
            }
            else {
                passwordRegister.removeClass('invalid override-input-err').addClass('valid override-input-suc');
                $('.errRegisterPassword').remove();

                isPasswordRegister = true;
            }

            /* Confirm Password */
            if (confirmPassword.val() == '') {
                if ($('.errConfirmPassword').length == 0) {
                    lblConfirmPassword.after('<div class="invalid-feedback errConfirmPassword">' + 'Please fill the required field.' + '</div>');

                    isConfirmPassword = false;
                }
            } else if (passwordRegister.val() != confirmPassword.val()) {
                $('.errConfirmPassword').remove();
                confirmPassword.removeClass('valid override-input-suc').addClass('invalid override-input-err');
                lblConfirmPassword.after('<div class="invalid-feedback errConfirmPassword">' + 'Password and Confirm Password not match.' + '</div>');

                isConfirmPassword = false;
            }
            else {
                confirmPassword.removeClass('invalid override-input-err').addClass('valid override-input-suc');
                $('.errConfirmPassword').remove();

                isConfirmPassword = true;
            }

            if (isEmailRegister && isPasswordRegister && isConfirmPassword) {
                isFormRegisterValid = true;
            } else {
                isFormRegisterValid = false;
            }

            return isFormRegisterValid;
        };

        var eventSubmitForm = function () {
            btnSubmitRegister.click(function () {
                if (formValidation()) {
                    console.log('Pass!');

                    var params = {
                        Email: email.val(),
                        Password: passwordRegister.val(),
                        ConfirmPassword: confirmPassword.val(),
                        UserType: WEBPORTAL.UserType.Vendor
                    };

                    $.when(WEBPORTAL.Services.POSTLocal(params, WEBPORTAL.URLContext.Register)).done(function (result, status, xhr) {
                        if (result.code === 400) {
                            $('.errRegisterEmail').remove();
                            email.removeClass('valid override-input-suc').addClass('invalid override-input-err');
                            lblEmail.after('<div class="invalid-feedback errRegisterEmail">' + result.message + '</div>');
                        } else if (result.code === 200) {
                            registerForm.submit();

                            window.location.href = WEBPORTAL.URLContext.AccountRegisterConfirmation + '?email=' + email.val();
                        } else {
                            $('.errConfirmPassword').remove();
                            confirmPassword.removeClass('valid override-input-suc').addClass('invalid override-input-err');
                            lblConfirmPassword.after('<div class="invalid-feedback errPassword">' + result.message + '</div>');
                        }
                    });
                } else {
                    console.log('Fail');

                    registerForm.addClass('was-validated');
                }
            });
        };

        var run = function () {
            registerDom();
            eventSubmitForm();
        };

        return {
            run: run
        };
    })();

    initalRegister.run();
})(jQuery);
