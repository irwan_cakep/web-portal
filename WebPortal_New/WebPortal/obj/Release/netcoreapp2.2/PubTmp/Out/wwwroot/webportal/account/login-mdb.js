﻿(function ($) {
    'use strict';

    var initalLogin = (function () {
        var loginForm, userId, lblUserId, password, lblPassword, btnSubmit, _KEY_LP;

        var registerDom = function () {
            loginForm = $('.form-login');
            userId = $('#loginUserId');
            lblUserId = $('#lblLoginUserId');
            password = $('#loginPassword');
            lblPassword = $('#lblLoginPassword');
            btnSubmit = $('.btn-submit');
            _KEY_LP = $('#_KEY_LP');
        };

        var formValidation = function () {
            var isFormValid = false,
                isUserId = false,
                isPassword = false;

            /* User Id */
            if (_KEY_LP.val() == 'EMP') {
                if (userId.val() == '') {
                    if ($('.errUserId').length == 0) {
                        lblUserId.after('<div class="invalid-feedback errUserId">' + 'Please fill the required field.' + '</div>');

                        isUserId = false;
                    }
                }
                else {
                    $('.errUserId').remove();

                    isUserId = true;
                }
            } else {
                if (userId.val() == '') {
                    if ($('.errUserId').length == 0) {
                        lblUserId.after('<div class="invalid-feedback errUserId">' + 'Please fill the required field.' + '</div>');

                        isUserId = false;
                    }
                } else if (!WEBPORTAL.Utility.CheckEmail(userId.val())) {
                    $('.errUserId').remove();
                    lblUserId.after('<div class="invalid-feedback errUserId">' + 'Format email not valid.' + '</div>');

                    isUserId = false;
                }
                else {
                    $('.errUserId').remove();

                    isUserId = true;
                }
            }

            /* Password */
            if (password.val() == '') {
                if ($('.errPassword').length == 0) {
                    lblPassword.after('<div class="invalid-feedback errPassword">' + 'Please fill the required field.' + '</div>');

                    isPassword = false;
                }
            } else if (password.val().length < 5) {
                $('.errPassword').remove();
                password.removeClass('valid override-input-suc').addClass('invalid override-input-err');
                lblPassword.after('<div class="invalid-feedback errPassword">' + 'Password minimum 3 characters length.' + '</div>');

                isPassword = false;
            }
            else {
                password.removeClass('invalid override-input-err').addClass('valid override-input-suc');
                $('.errPassword').remove();

                isPassword = true;
            }

            if (isUserId && isPassword) {
                isFormValid = true;
            } else {
                isFormValid = false;
            }

            return isFormValid;
        };

        var eventSubmitForm = function () {
            btnSubmit.click(function () {
                if (formValidation()) {
                    console.log('Pass!');

                    var params = {
                        UserId: userId.val(),
                        Password: password.val()
                    };

                    $.when(WEBPORTAL.Services.POSTLocal(params, WEBPORTAL.URLContext.Login)).done(function (result, status, xhr) {
                        if (result.code === 200) {
                            if (result.isUserActive) {

                                loginForm.submit();

                                if (result.sessionUserLevel == 'VENDOR') {
                                    window.location.href = WEBPORTAL.URLContext.VendorDashboard;
                                } else if (result.sessionUserLevel == 'ADMIN') {
                                    window.location.href = WEBPORTAL.URLContext.AdminDashboard;
                                } else if (result.sessionUserLevel == 'JOB_APPLICANT') {
                                    window.location.href = WEBPORTAL.URLContext.JobApplicantDashboard;
                                }
                                else {
                                    window.location.href = WEBPORTAL.URLContext.HomeIndex;
                                }
                            } else {
                                $('.errPassword').remove();
                                password.removeClass('valid override-input-suc').addClass('invalid override-input-err');
                                lblPassword.after('<div class="invalid-feedback errPassword">' + 'Please activate your email before login.' + '</div>');
                            }
                        } else {
                            $('.errPassword').remove();
                            password.removeClass('valid override-input-suc').addClass('invalid override-input-err');
                            lblPassword.after('<div class="invalid-feedback errPassword">' + result.message + '</div>');
                        }
                    });
                } else {
                    console.log('Fail');

                    loginForm.addClass('was-validated');
                }
            });
        };

        var run = function () {
            registerDom();
            eventSubmitForm();
        };

        return {
            run: run
        };
    })();

    initalLogin.run();
})(jQuery);
