﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Helper
{
    public static class GlobalHelper
    {
        public static IEnumerable<object> GetObjectArray<T>(IEnumerable<T> obj)
        {
            return obj.Select(o => o.GetType().GetProperties().Select(p => p.GetValue(o, null)));
        }

        public static string ConstructSeparatedComma(string[] obj)
        {
            return string.Join(",", obj);
        }
    }
}
