﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models
{
    public class EmailFormatViewModel
    {
        IConfiguration Configuration;
        IHostingEnvironment Environment;
        public EmailFormatViewModel(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }
        public string SMTPServer
        {
            get
            {
                return Configuration.GetValue<string>("EmailConfiguration:SMTPServer");
            }
        }
        public string EmailHost
        {
            get
            {
                return Configuration.GetValue<string>("EmailConfiguration:EmailHost");
            }
        }
        public string EmailHostPassword
        {
            get
            {
                return Configuration.GetValue<string>("EmailConfiguration:EmailHostPassword");
            }
        }
        public string LogoUrl
        {
            get
            {
                return Configuration.GetValue<string>("EmailConfiguration:LogoUrl");
            }
        }

        public string Subject { get; set; }
        public string EmailRecipient { get; set; }
        public string Message1 { get; set; }
        public string Message2 { get; set; }
        public string Link { get; set; }
        public string LinkMessage { get; set; }
        public string ConstructEmailBody(EmailFormatViewModel model)
        {
            string body = System.IO.File.ReadAllText(Path.Combine(Environment.WebRootPath, "EmailTemplate\\EmailBody.htm"));

            body = body.Replace("#logoUrl#", model.LogoUrl);
            body = body.Replace("#link#", model.Link);
            body = body.Replace("#linkMessage#", model.LinkMessage);
            body = body.Replace("#message1#", model.Message1);
            body = body.Replace("#message2#", model.Message2);

            return body;
        }
    }
}
