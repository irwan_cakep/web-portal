﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.BusinessTypes
{
    public class BusinessTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
