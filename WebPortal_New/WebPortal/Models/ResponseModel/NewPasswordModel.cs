﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.ResponseModel
{
    public class NewPasswordModel
    {
        public string Token { get; set; }
        public string NewPassword { get; set; }
    }
}
