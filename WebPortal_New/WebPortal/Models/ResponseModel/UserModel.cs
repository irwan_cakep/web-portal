﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.ResponseModel
{
    public class UserModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ForgetPassword { get; set; }
        public string Region { get; set; }
        public string Email { get; set; }
        public string Jabatan { get; set; }
        public string Divisi { get; set; }
        public string ExtensionNumber { get; set; }
        public DateTime? LastLogin { get; set; }
        public DateTime? LastLogout { get; set; }
        public string Session { get; set; }
        public int UserLevel { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string Image { get; set; }
        public bool Isactive { get; set; }
        public string Ip { get; set; }
        public bool IsTemporary { get; set; }
        public DateTime? StartAccess { get; set; }
        public DateTime? EndAccess { get; set; }
        public bool IsUserAd { get; set; }
        public string Domain { get; set; }
        public DateTime? LastAccessFaq { get; set; }
        public DateTime? LastAccessTutorial { get; set; }
        public DateTime? LastAccessDocument { get; set; }
        public string ActivateLink { get; set; }
    }
}
