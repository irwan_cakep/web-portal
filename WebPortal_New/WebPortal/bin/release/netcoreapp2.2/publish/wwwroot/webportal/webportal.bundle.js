﻿/**
 * @namespace WEBPORTAL
 * 
 * */
var WEBPORTAL = WEBPORTAL || {};

/**
 * @module Version
 * 
 * */
WEBPORTAL.Version = '1.0.0';

/**
 * @module Register
 * 
 * */
WEBPORTAL.Register = {};
WEBPORTAL.Register.MaterialSelect = $('.mdb-select').materialSelect();

/**
 * @module Global
 * 
 * */
WEBPORTAL.Global = {};
WEBPORTAL.Global.SetDropdownCaret = $('.select-wrapper.md-form.md-outline span.caret').css('z-index', '3');

/**
 * @module UserType
 * 
 * */
WEBPORTAL.UserType = {};
WEBPORTAL.UserType.Vendor = "V";

/**
 * @module APIEndpoint
 * 
 * */
WEBPORTAL.ApiEndpoint = {};
WEBPORTAL.ApiEndpoint.BaseUrl = "https://localhost:44304";
WEBPORTAL.ApiEndpoint.BusinessFields = "/api/BusinessFieldType/GetBusinessFieldsByGroupType";
WEBPORTAL.ApiEndpoint.Country = "/api/Country/Get";
WEBPORTAL.ApiEndpoint.Region = "/api/Region/Get";
WEBPORTAL.ApiEndpoint.City = "/api/City/Get";

/**
 * @module URLContext
 * 
 * */
WEBPORTAL.URLContext = {};
WEBPORTAL.URLContext.SaveBasicInfo = "/Vendor/SaveBasicInfo";
WEBPORTAL.URLContext.GetBasicInfo = "/Vendor/GetBasicInfo";
WEBPORTAL.URLContext.Login = "/Account/Login";
WEBPORTAL.URLContext.Register = "/Account/Register";
WEBPORTAL.URLContext.VendorData = '/Vendor/Data';
WEBPORTAL.URLContext.VendorDashboard = '/Vendor/Dashboard';
WEBPORTAL.URLContext.AdminDashboard = '/Admin/Dashboard';
WEBPORTAL.URLContext.AdminVendor = '/Admin/Vendor';
WEBPORTAL.URLContext.HomeIndex = '/Home/Dashboard';
WEBPORTAL.URLContext.JobApplicantDashboard = '/JobApplicant/Dashboard';
WEBPORTAL.URLContext.AccountRegisterConfirmation = '/Account/RegisterConfirmation';

/**
 * @module Services
 * 
 */
WEBPORTAL.Services = (function () {
    function get(urlContext) {
        var deferred = $.Deferred();

        $.ajax({
            type: "GET",
            url: WEBPORTAL.ApiEndpoint.BaseUrl + urlContext,
            success: function (result, status, xhr) {            
                deferred.resolve(result);
            },
            error: function (result, status, xhr) {
                deferred.reject(result);
            }
        });

        return deferred.promise();
    };

    function getLocal(urlContext) {
        var deferred = $.Deferred();

        $.ajax({
            type: "GET",
            url: urlContext,
            success: function (result, status, xhr) {
                deferred.resolve(result);
            },
            error: function (result, status, xhr) {
                deferred.reject(result);
            }
        });

        return deferred.promise();
    };

    function postLocal(data, urlContext) {
        var deferred = $.Deferred();

        $.ajax({
            type: "POST",
            data: data,
            url: urlContext,
            success: function (result, status, xhr) {
                deferred.resolve(result);
            },
            error: function (result, status, xhr) {
                deferred.reject(result);
            },
            timeout: 60000
        });

        return deferred.promise();
    }

    return {
        GET: get,
        GETLocal: getLocal,
        POSTLocal: postLocal
    };
})();

/**
 * @module Utility
 * 
 * */
WEBPORTAL.Utility = (function () {
    function showLoading(obj) {
        obj.append('<span class="loading-animation"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i></span>');
    };
    function removeLoading(obj) {
        obj.remove();
    };
    function constructDropdownOptions(elem, data) {
        var option = '<option value="" disabled selected>Select</option>';

        $.each(data, function (key, value) {
            option += "<option value='" + value.id + "'>" + value.name + "</option>";
        });

        elem.html(option);
    };
    function constructDropdownGroupOptions(elem, data) {
        var tempOption = "";

        $.each(data, function (key, value) {
            var optGroup = "<optgroup label='" + value.desc + "'>";

            for (var i = 0; i < value.businessFields.length; i++) {
                optGroup += "<option value='" + value.businessFields[i].id + "'>" + value.businessFields[i].kbli + " " + value.businessFields[i].name + "</option>";
            }

            optGroup += "</optGroup>";
            tempOption += optGroup;         
        });
        elem.html(tempOption);
    };
    function checkEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        return regex.test(email);
    };
    function submitLoading(elem) {
        elem.html('<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>Loading...').addClass('disabled');
    };
    function submitRemoveLoading(elem) {
        elem.find('span').remove();
        elem.removeClass('disabled');
        elem.text('Save');
        elem.html('<i class="fa fa-save pr-2"></i> <span>Save</span>');
    };
    function constructUpdateButton(elem) {
        elem.find('span').remove();
        elem.removeClass('disabled');
        elem.text('Update');
        elem.html('<i class="fa fa-edit pr-2"></i> <span>Update</span>');
    };
    function constructNotificationSuccess() {
        toastr.success('Save Data Success.', 'Success', { timeOut: 5000 });
    };
    function constructNotificationError() {
        toastr.error('Save Data Error.', 'Error', { timeOut: 5000 })
    };
    return {
        ShowLoading: showLoading,
        RemoveLoading: removeLoading,
        ConstructDropdownOptions: constructDropdownOptions,
        ConstructDropdownGroupOptions: constructDropdownGroupOptions,
        CheckEmail: checkEmail,
        SubmitLoading: submitLoading,
        SubmitRemoveLoading: submitRemoveLoading,
        ConstructUpdateButton: constructUpdateButton,
        ConstructNotificationSuccess: constructNotificationSuccess,
        ConstructNotificationError: constructNotificationError
    };
})();

/**
 * @module jQuery
 * 
 */
jQuery.fn.extend({
    configureMaterialSelect: function () {
        $(this).val("")
            .removeAttr('readonly').attr("placeholder", "Select ").prop('required', true)
            .addClass('form-control').css('background-color', '#fff');
    },
    configureMaterialSelectGroup: function () {
        $(this).val("")
            .removeAttr('readonly').attr("placeholder", "Select Max 5 Bussiness Field ").prop('required', true)
            .addClass('form-control').css('background-color', '#fff');
    },
    removeError: function () {
        $(this).on('change', function () {
            if ($(this).val() != '') {
                $(this).parent().next().next().remove();
            }
        });
    }
});




