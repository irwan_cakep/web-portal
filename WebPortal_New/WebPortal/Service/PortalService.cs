﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using WebPortal.Helper;
using WebPortal.Models;
using WebPortal.Models.ResponseModel;
using WebPortal.Models.Vendor;

namespace WebPortal.Service
{
    public class PortalService
    {
        HttpClient Client { get; }
        IConfiguration Configuration { get; }
        public PortalService(HttpClient client, IConfiguration configuration)
        {

            Configuration = configuration;

            client.BaseAddress = new Uri(Configuration.GetValue<string>("WebPortalAPIEndpoint:BaseUrl"));
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            Client = client;
        }

        #region Account
        public async Task<BaseResponse> GetUserByEmail(string email)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetUserByEmail") + "/?email=" + email);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
                result.UserModel = result.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(result.Data)) : null;
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> InsertUser(RegisterViewModel model)
        {
            var result = new BaseResponse();

            try
            {
                UserModel user = new UserModel
                {
                    UserId = model.Email,
                    UserName = model.Email,
                    Email = model.Email,
                    Password = EncryptionHelper.Encrypt(model.Password),
                    UserLevel = model.UserType == "V" ? 2 : model.UserType == "C" ? 3 : 4,
                    ActivateLink = model.ActivationCode
                };

                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:InsertUser"), new JsonContent(user));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> ActivationUser(string activationCode)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsJsonAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:ActivationUser"), activationCode);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> LoginUser(LoginViewModel model)
        {
            BaseResponse result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetUserByEmailOrUserId") + "/?email=" + model.UserId);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();

                result.UserModel = result.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(result.Data)) : null;
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> InsertUserAD(string email)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsJsonAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:InsertUserFromAD"), email);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();

                result.UserModel = result.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(result.Data)) : null;
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> UpdateUserAD(LoginViewModel model)
        {
            var result = new BaseResponse();

            try
            {
                ActiveDirectoryModel param = new ActiveDirectoryModel
                {
                    UserId = model.UserId,
                    Password = model.Password
                };

                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:UpdateUserFromAD"), new JsonContent(param));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();

                result.UserModel = result.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(result.Data)) : null;
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> ResetPassword(string email)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsJsonAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:ResetPassword"), email);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> SetNewPassword(string token, string newPassword)
        {
            var result = new BaseResponse();

            try
            {
                var param = new NewPasswordModel
                {
                    Token = token,
                    NewPassword = EncryptionHelper.Encrypt(newPassword)
                };

                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:SetNewPassword"), new JsonContent(param));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region BusinessFileType
        public async Task<BaseResponse> GetBusinessType()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetBusinessType"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region BusinessFields
        public async Task<BaseResponse> GetBusinessFields()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetBusinessFieldsByGroupType"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region Country
        public async Task<BaseResponse> GetCountries()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetCountries"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region Region
        public async Task<BaseResponse> GetRegions()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetRegions"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region City
        public async Task<BaseResponse> GetCities()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetCities"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region Vendor
        public async Task<BaseResponse> SaveVendorBasicInfo(BasicInfoModel model)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:InsertVendorBasicInfo"), new JsonContent(model));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        public async Task<BaseResponse> GetVendorBasicInfo(string userId)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorBasicInfo") + "/?userId=" + userId);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> GetAllVendor()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetAllVendor"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> GetVendorBankAccount(string userId)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorBankAccount") + "/?masterDataUserId=" + userId);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> SaveVendorBankAccount(BankPaymentModel model)
        {
            var result = new BaseResponse();

            var data = new BankPaymentModel
            {
                MasterDataUserId = model.MasterDataUserId,
                MasterDataCurrencyId = model.MasterDataCurrencyId,
                MasterDataBankId = model.MasterDataBankId,
                Area = model.Area,
                City = model.City,
                AccountNumber = model.AccountNumber,
                AccountName = model.AccountName,
                CreatedBy = model.CreatedBy,
                ModifiyBy = model.CreatedBy
            };

            try
            {
                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:SaveVendorBankAccount"), new JsonContent(data));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region Currency
        public async Task<BaseResponse> GetCurrency()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetCurrency"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region Bank
        public async Task<BaseResponse> GetAllBank()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetAllBank"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion
    }
}
