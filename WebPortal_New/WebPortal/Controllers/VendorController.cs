using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebPortal.Models.ResponseModel;
using WebPortal.Models.Vendor;
using WebPortal.Service;

namespace WebPortal.Controllers
{
    public class VendorController : BaseController
    {
        PortalService _portalService;
        public VendorController(PortalService portalService)
        {
            _portalService = portalService;
        }

        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> Data()
        {
            ViewData["UserLevel"] = UserLevel;
            BasicInfoModel model = new BasicInfoModel();            

            var resBusinessType = await _portalService.GetBusinessType();
            var resBusinessFields = await _portalService.GetBusinessFields();
            var resCountry = await _portalService.GetCountries();
            var resRegion = await _portalService.GetRegions();
            var resCity = await _portalService.GetCities();
            var resBasicInfo = await _portalService.GetVendorBasicInfo(UserId);

            List<Models.BusinessTypes.BusinessTypeModel> businessTypes = resBusinessType.Data != null ? JsonConvert.DeserializeObject<List<Models.BusinessTypes.BusinessTypeModel>>(Convert.ToString(resBusinessType.Data)) : null;
            List<Models.BusinessFields.BusinessFieldTypeModel> businessFieldsByType = resBusinessFields.Data != null ? JsonConvert.DeserializeObject<List<Models.BusinessFields.BusinessFieldTypeModel>>(Convert.ToString(resBusinessFields.Data)) : null;
            List<Models.Country.CountryModel> countries = resCountry.Data != null ? JsonConvert.DeserializeObject<List<Models.Country.CountryModel>>(Convert.ToString(resCountry.Data)) : null;
            List<Models.Region.RegionsModel> regions = resRegion.Data != null ? JsonConvert.DeserializeObject<List<Models.Region.RegionsModel>>(Convert.ToString(resRegion.Data)) : null;
            List<Models.City.CityModel> cities = resCity.Data != null ? JsonConvert.DeserializeObject<List<Models.City.CityModel>>(Convert.ToString(resCity.Data)) : null;

            if (resBasicInfo.Data != null)
            {
                model = resBasicInfo.Data != null ? JsonConvert.DeserializeObject<BasicInfoModel>(Convert.ToString(resBasicInfo.Data)) : null;

                model.BusinessFieldsJson = JsonConvert.SerializeObject(model.MasterBusinessFieldsIdArray);
            }

            model.BussinessTypeOptions = model.ConstructBussinessTypeOptions(businessTypes);
            model.BusinessFieldsOptions = model.ConstructBussinessFieldsOptions(businessFieldsByType);
            model.CountryOptions = model.ConstructCountryOptions(countries);
            model.RegionOptions = model.ConstructRegionsOptions(regions);
            model.CityOptions = model.ConstructCityOptions(cities);

            return View(model);
        }

        public IActionResult Dashboard()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        [HttpGet]
        public async Task<BaseResponse> GetBasicInfo()
        {
            return await _portalService.GetVendorBasicInfo(UserId);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<BaseResponse> SaveBasicInfo(BasicInfoModel model)
        {
            BaseResponse response = new BaseResponse();

            model.MasterDataUserId = Int32.Parse(UserId);

            response = await _portalService.SaveVendorBasicInfo(model);

            return response;
        }

        //public IActionResult BankPayment() => new PartialViewResult { ViewName = "~/Views/Vendor/Partial/_BankPayment.cshtml" };
        public async Task<IActionResult> BankPayment()
        {
            ViewData["UserLevel"] = UserLevel;
            BankPaymentModel model = new BankPaymentModel();

            var resCurrency = await _portalService.GetCurrency();
            var resBank = await _portalService.GetAllBank();

            List<Models.Currency.CurrencyModel> currencies = resCurrency.Data != null ? JsonConvert.DeserializeObject<List<Models.Currency.CurrencyModel>>(Convert.ToString(resCurrency.Data)) : null;
            List<Models.Bank.BankModel> banks = resBank.Data != null ? JsonConvert.DeserializeObject<List<Models.Bank.BankModel>>(Convert.ToString(resBank.Data)) : null;

            model.CurrencyOptions = model.ConstructCurrencyOptions(currencies);
            model.BankOptions = model.ConstructBankOptions(banks);

            return View(model);
        }

        [HttpGet]
        public async Task<string> GetVendorBankAccount()
        {
            var response = await _portalService.GetVendorBankAccount(UserId);

            return JsonConvert.SerializeObject(response.Data);
        }

        [HttpPost]
        public async Task<BaseResponse> SaveVendorBankAccount(BankPaymentModel model)
        {
            model.MasterDataUserId = Int32.Parse(UserId);
            model.CreatedBy = UserLevel;

            var response = await _portalService.SaveVendorBankAccount(model);

            return response;
        }

        public IActionResult Legal()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult Tax()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult Products()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult IntegrityPact()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult MasterAgreeMent()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult Profile()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult Inbox()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult Compose()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult Sent()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult InvoiceSend()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult InvoiceStatus()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult InvoiceDetails(string id)
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

    }
}