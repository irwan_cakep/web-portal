using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using WebPortal.Helper;
using WebPortal.Models;
using WebPortal.Models.ResponseModel;
using WebPortal.Service;

namespace WebPortal.Controllers
{
    public class AccountController : BaseController
    {
        readonly PortalService _portalService;
        readonly IConfiguration _configuration;
        readonly IHostingEnvironment _environment;

        public AccountController(
            PortalService portalService,
            IConfiguration configuration,
            IHostingEnvironment environment)
        {
            _portalService = portalService;
            _configuration = configuration;
            _environment = environment;
        }
        #region Login
        public ViewResult Login()
        {
            return View();
        }

        public IActionResult Logout()
        {
            return View();
        }

        [HttpPost]
        public async Task<BaseResponse> Login(LoginViewModel model)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var userFound = await _portalService.LoginUser(model);

                if (userFound.Code == (int)HttpStatusCode.OK)
                {
                    if (userFound != null)
                    {
                        if (userFound.UserModel != null)
                        {
                            if (userFound.UserModel.IsUserAd)
                            {
                                response = await _portalService.UpdateUserAD(model);

                                response.IsUserActive = userFound.UserModel.Isactive;

                                SetCookie(response.UserModel.Id, response.UserModel.UserLevel);

                                response.SessionUserLevel = UserLevel;
                            }
                            else if (EncryptionHelper.Decrypt(userFound.UserModel.Password) != model.Password)
                            {
                                response.Code = 601;
                                response.Status = HttpStatusCode.BadRequest.ToString();
                                response.Message = "Username and password not match.";
                            }
                            else
                            {
                                response.Code = (int)HttpStatusCode.OK;
                                response.Status = HttpStatusCode.OK.ToString();
                                response.Message = "Login success";

                                response.IsUserActive = userFound.UserModel.Isactive;

                                SetCookie(userFound.UserModel.Id, userFound.UserModel.UserLevel);

                                response.SessionUserLevel = UserLevel;
                            }
                        }
                        else
                        {
                            response = await _portalService.InsertUserAD(model.UserId);

                            if (response.Code != (int)HttpStatusCode.OK)
                            {
                                response.Message = "Email not registered. Please register for get access to our website.";
                            }

                            SetCookie(response.UserModel.Id, response.UserModel.UserLevel);

                            response.SessionUserLevel = UserLevel;
                        }
                    }
                } else
                {
                    response.Message = "Internal Server Error";
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }
        #endregion

        #region Register
        public async Task<ViewResult> Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Register(RegisterViewModel model)
        {
            BaseResponse result = new BaseResponse();

            try
            {
                var userFound = await _portalService.GetUserByEmail(model.Email);

                if (userFound.UserModel != null)
                {
                    if (userFound.UserModel.Email != null)
                    {
                        result.Code = (int)HttpStatusCode.BadRequest;
                        result.Status = HttpStatusCode.BadRequest.ToString();
                        result.Message = "Email already registered.";
                    }
                }
                else
                {
                    model.ActivationCode = Guid.NewGuid().ToString().Replace("-", "");
                    result = await _portalService.InsertUser(model);

                    var emailSetup = await ConstructRegisterEmailFormatModel(model.ActivationCode, model.Email);

                    MailService.SendMail(emailSetup, emailSetup.ConstructEmailBody(emailSetup));
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)HttpStatusCode.InternalServerError;
                result.Status = HttpStatusCode.InternalServerError.ToString();
                result.Message = ex.ToString();
            }

            return Json(result);
        }

        public ViewResult RegisterConfirmation(string email)
        {
            ViewData["email"] = email;

            return View();
        }

        public async Task<ViewResult> Activation(string activationCode)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                response = await _portalService.ActivationUser(activationCode);
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }
            return View(response);
        }
        #endregion

        #region ForgotPassword
        public async Task<ViewResult> ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<BaseResponse> ForgotPassword(string email)
        {
            BaseResponse result = new BaseResponse();

            try
            {
                result = await _portalService.ResetPassword(email);

                if (result.Code == (int)HttpStatusCode.OK)
                {
                    var emailSetup = await ConstructForgotPasswordEmailFormatModel(Convert.ToString(result.Data), email);

                    MailService.SendMail(emailSetup, emailSetup.ConstructEmailBody(emailSetup));

                    result.Message = email;
                }
            }
            catch (Exception ex)
            {

                result.Message = ex.ToString();
            }

            return result;
        }

        public ViewResult ForgotPasswordVerification(string email)
        {
            ViewData["email"] = email;

            return View();
        }

        public async Task<ViewResult> NewPassword(string token)
        {
            ViewData["token"] = token;

            return View();
        }

        [HttpPost]
        public async Task<BaseResponse> NewPassword(string token, string newPassword)
        {
            BaseResponse result = new BaseResponse();

            try
            {
                result = await _portalService.SetNewPassword(token, newPassword);
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<ViewResult> ForgotPasswordConfirmation()
        {
            return View();
        }
        #endregion

        #region Profile
        public async Task<IActionResult> Profile()
        {
            return View();
        }
        #endregion

        #region helper
        private async Task<EmailFormatViewModel> ConstructRegisterEmailFormatModel(string activationCode, string email)
        {
            EmailFormatViewModel formatEmailModel = new EmailFormatViewModel(_configuration, _environment);

            string baseUrl = Request.Scheme + "://" + Request.Host;
            string activationUrl = baseUrl + "/Account/Activation/?activationCode=" + activationCode;

            formatEmailModel.Subject = "Email Activation";
            formatEmailModel.EmailRecipient = email;
            formatEmailModel.Link = activationUrl;
            formatEmailModel.LinkMessage = "Confirm Email Address";
            formatEmailModel.Message1 = "Please confirm your email address by clicking the link below.";
            formatEmailModel.Message2 = "We may need to send you critical information about our service and it is important that we have an accurate email address.";

            return await Task.FromResult(formatEmailModel);
        }

        private async Task<EmailFormatViewModel> ConstructForgotPasswordEmailFormatModel(string token, string email)
        {
            EmailFormatViewModel formatEmailModel = new EmailFormatViewModel(_configuration, _environment);

            string baseUrl = Request.Scheme + "://" + Request.Host;
            string activationUrl = baseUrl + "/Account/NewPassword/?token=" + token;

            formatEmailModel.Subject = "Reset Password";
            formatEmailModel.EmailRecipient = email;
            formatEmailModel.Link = activationUrl;
            formatEmailModel.LinkMessage = "Confirm Email Address";
            formatEmailModel.Message1 = "Please confirm your email address by clicking the link below.";
            formatEmailModel.Message2 = "We may need to send you critical information about our service and it is important that we have an accurate email address.";

            return await Task.FromResult(formatEmailModel);
        }

        private void SetCookie(int userId, int userLevel)
        {
            UserId = userId.ToString();

            switch (userLevel)
            {
                case 0:
                    UserLevel = "ADMIN";
                    break;
                case 2:
                    UserLevel = "VENDOR";
                    break;
                case 3:
                    UserLevel = "CUSTOMER";
                    break;
                case 4:
                    UserLevel = "JOB_APPLICANT";
                    break;
                case 11:
                    UserLevel = "USER_FR";
                    break;
                case 12:
                    UserLevel = "USER_FAP";
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}