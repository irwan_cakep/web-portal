﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models;

namespace WebPortal.Controllers
{
    public class BaseController : Controller
    {
        const string UserIdSessionKey = "_UserId";
        const string UserLevelSessionKey = "_UserLevel";
        protected string UserId
        {
            get { return HttpContext.Session?.GetString(UserIdSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(UserIdSessionKey, value); }
        }
        protected string UserLevel
        {
            get { return HttpContext.Session?.GetString(UserLevelSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(UserLevelSessionKey, value); }
        }
    }
}