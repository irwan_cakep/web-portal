﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendorController : ControllerBase
    {
        IVendorRepository _VendorRepository;
        public VendorController(IVendorRepository vendorRepository)
        {
            _VendorRepository = vendorRepository;
        }
        
        [HttpPost("InsertVendor")]
        public async Task<BaseResponse> InsertVendor([FromBody] MasterDataVendor data)
        {
            return await _VendorRepository.InsertVendor(data);
        }

        [HttpPost("InsertVendorBasicInfo")]
        public async Task<BaseResponse> InsertVendorBasicInfo([FromBody] BasicInfoModel data)
        {
            return await _VendorRepository.InsertVendorBasicInfo(data);
        }

        [HttpGet("GetVendorByUserId")]
        public async Task<BaseResponse> GetVendorByUserId(int userId)
        {
            return await _VendorRepository.GetVendorByUserId(userId);
        }

        [HttpGet("GetAllVendor")]
        public async Task<BaseResponse> GetAllVendor()
        {
            return await _VendorRepository.GetAllVendor();
        }

        [HttpGet("GetVendorBankAccount")]
        public async Task<BaseResponse> GetVendorBankAccount(int masterDataUserId)
        {
            return await _VendorRepository.GetVendorBankAccount(masterDataUserId);
        }

        [HttpPost("SaveVendorBankAccount")]
        public async Task<BaseResponse> SaveVendorBankAccount([FromBody] MappingDataVendorCurrencyBank data)
        {
            return await _VendorRepository.SaveVendorBankAccount(data);
        }
    }
}