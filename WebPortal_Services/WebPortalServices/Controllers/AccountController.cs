﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUserRepository _MsUserRepository;
        private readonly IHistoryLoginRepository _HistoryLoginRepository;
        private readonly IDomainRepository _MsDomainRepository;
        private readonly IResetPasswordRepository _ResetPasswordRepository;
        public AccountController(
            IUserRepository MasterDataUserRepository,
            IHistoryLoginRepository MasterDataHistoryLoginRepository,
            IDomainRepository MasterDataDomainRepository,
            IResetPasswordRepository MasterDataResetPasswordRepository)
        {
            _MsUserRepository = MasterDataUserRepository;
            _HistoryLoginRepository = MasterDataHistoryLoginRepository;
            _MsDomainRepository = MasterDataDomainRepository;
            _ResetPasswordRepository = MasterDataResetPasswordRepository;
        }

        [HttpGet("GetUserByEmail")]
        public async Task<BaseResponse> GetUserByEmail(string email)
        {
            return await _MsUserRepository.GetUserByEmail(email);
        }

        [HttpGet("GetUserByEmailOrUserId")]
        public async Task<BaseResponse> GetUserByEmailOrUserId(string email)
        {
            return await _MsUserRepository.GetUserByEmailOrUserId(email);
        }
        
        [HttpPost("InsertUser")]
        public async Task<BaseResponse> InsertUser([FromBody] MasterDataUser data)
        {
            return await _MsUserRepository.Insert(data);
        }

        [HttpPost("InserMasterDataHistoryLogin")]
        public async Task<BaseResponse> InserMasterDataHistoryLogin([FromBody]MasterDataHistoryLogin data)
        {
            return await _HistoryLoginRepository.Insert(data);
        }

        [HttpPost("InsertUserFromAD")]
        public async Task<BaseResponse> InsertUserFromAD([FromBody]string email)
        {
            return await _MsUserRepository.InsertUserFromAd(email);
        }

        [HttpPost("UpdateUserFromAD")]
        public async Task<BaseResponse> UpdateUserFromAD([FromBody] ActiveDirectoryUserModel data)
        {
            return await _MsUserRepository.UpdateUserFromAd(data);
        }

        [HttpPost("ResetPassword")]
        public async Task<BaseResponse> ResetPassword([FromBody] string email)
        {
            return await _ResetPasswordRepository.ResetPassword(email);
        }

        [HttpGet("GetValidResetPassword")]
        public async Task<BaseResponse> GetValidResetPassword(string token)
        {
            return await _ResetPasswordRepository.GetValidResetPassword(token);
        }

        [HttpPost("SetNewPassword")]
        public async Task<BaseResponse> SetNewPassword([FromBody] NewPasswordModel data)
        {
            return await _ResetPasswordRepository.SetNewPassword(data);
        }

        [HttpPost("ActivationUser")]
        public async Task<BaseResponse> ActivationUser([FromBody] string activationCode)
        {
            return await _MsUserRepository.ActivationUser(activationCode);
        }
    }
}
