﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BusinessFieldTypeController : ControllerBase
    {
        IBusinessFieldsTypeRepository _BusinessFieldsTypeRepository;
        public BusinessFieldTypeController(IBusinessFieldsTypeRepository businessFieldsTypeRepository)
        {
            _BusinessFieldsTypeRepository = businessFieldsTypeRepository;
        }

        [HttpGet("GetBusinessFieldsByGroupType")]
        public async Task<BaseResponse> GetBusinessFieldsByGroupType()
        {
            return await _BusinessFieldsTypeRepository.GetBusinessFieldsByGroupType();
        }
    }
}
