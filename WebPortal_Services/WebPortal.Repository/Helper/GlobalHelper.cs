﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPortal.Repository.Helper
{
    public static class GlobalHelper
    {
        public static async Task<String> GetProperty(Principal principal, String property)
        {
            DirectoryEntry directoryEntry = principal.GetUnderlyingObject() as DirectoryEntry;
            if (directoryEntry.Properties.Contains(property))
                return await Task.FromResult(directoryEntry.Properties[property].Value.ToString());
            else
                return await Task.FromResult(String.Empty);
        }
    }
}
