﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortal.Repository.Concrete
{
    public class BankRepository : IBankRepository
    {
        readonly WebPortalContext _context;
        public BankRepository(WebPortalContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> GetAllBank()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await _context.MasterDataBank.ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }
    }
}
