﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;

namespace WebPortal.Repository.Concrete
{
    public class BusinessFieldsTypeRepository : IBusinessFieldsTypeRepository
    {
        readonly WebPortalContext _context;
        public BusinessFieldsTypeRepository(WebPortalContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> GetBusinessFieldsByGroupType()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from bussinessFieldType in _context.MasterDataBusinessFieldsType
                                    select new
                                    {
                                        bussinessFieldType.Id,
                                        bussinessFieldType.Code,
                                        bussinessFieldType.Name,
                                        businessFields = (from bf in _context.MasterDataBusinessFields
                                                         where bf.MasterDataBusinessFieldTypeId == bussinessFieldType.Id
                                                         select new {
                                                             bf.Id,
                                                             bf.Name,
                                                             bf.Kbli
                                                         })
                                    }).ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }
    }
}
