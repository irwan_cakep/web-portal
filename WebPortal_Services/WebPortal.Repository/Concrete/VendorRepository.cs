﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using System.Reflection;
using WebPortal.Repository.Helper;

namespace WebPortal.Repository.Concrete
{
    public class VendorRepository : IVendorRepository
    {
        readonly WebPortalContext _context;
        public VendorRepository(WebPortalContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> InsertVendor(MasterDataVendor data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    if (_context.MasterDataVendor.Where(n => n.MasterDataUserId == data.MasterDataUserId).Count() == 0)
                    {
                        MasterDataVendor vendor = new MasterDataVendor
                        {
                            Name = data.Name,
                            MasterDataUserId = data.MasterDataUserId,
                            MasterDataBusinessTypeId = data.MasterDataBusinessTypeId,
                            MasterDataCountryId = data.MasterDataCountryId,
                            MasterDataRegionId = data.MasterDataRegionId,
                            MasterDataCityId = data.MasterDataCityId,
                            Address = data.Address,
                            PostalCode = data.PostalCode,
                            CompanyEmail = data.CompanyEmail,
                            Pic = data.Pic,
                            Picemail = data.Picemail,
                            IdCardNumber = data.IdCardNumber,
                            IntegrityPact = data.IntegrityPact,
                            MasterAgreement = data.MasterAgreement,
                            Logo = data.Logo,
                            CreatedDate = DateTime.Now,
                            CreatedBy = data.Name,
                            ModifiedDate = DateTime.Now,
                            ModifiyBy = data.Name,
                            IsDelete = data.IsDelete
                        };

                        await _context.MasterDataVendor.AddAsync(vendor);
                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response = ResponseConstant.SAVE_SUCCESS;
                    }
                    else
                    {
                        response.Code = (int)HttpStatusCode.BadRequest;
                        response.Status = HttpStatusCode.BadRequest.ToString();
                        response.Message = "Email already exist.";

                        dbcxtransaction.Rollback();
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> InsertVendorBasicInfo(BasicInfoModel data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    if (_context.MasterDataVendor.Where(n => n.MasterDataUserId == data.MasterDataUserId).Count() == 0)
                    {
                        MasterDataVendor vendor = new MasterDataVendor
                        {
                            Name = data.Name,
                            MasterDataUserId = data.MasterDataUserId,
                            MasterDataBusinessTypeId = data.MasterDataBusinessTypeId,
                            MasterDataCountryId = data.MasterDataCountryId,
                            MasterDataRegionId = data.MasterDataRegionId,
                            MasterDataCityId = data.MasterDataCityId,
                            Address = data.Address,
                            MailingAddress = data.MailingAddress,
                            PostalCode = data.PostalCode,
                            CompanyEmail = data.CompanyEmail,
                            CompanyPhone = data.CompanyPhone,
                            Pic = data.Pic,
                            Picemail = data.Picemail,
                            IdCardNumber = data.IdCardNumber,
                            Picphone = data.Picphone,
                            IntegrityPact = data.IntegrityPact,
                            MasterAgreement = data.MasterAgreement,
                            Logo = data.Logo,
                            CreatedDate = DateTime.Now,
                            CreatedBy = data.Name,
                            ModifiedDate = DateTime.Now,
                            ModifiyBy = data.Name,
                            IsDelete = data.IsDelete
                        };

                        await _context.MasterDataVendor.AddAsync(vendor);
                        await _context.SaveChangesAsync();

                        // mapping master data vendor with businessfield
                        for (var i = 0; i < data.MasterBusinessFieldsIdArray.Count; i++)
                        {
                            MappingDataVendorBusinessFields mvbf = new MappingDataVendorBusinessFields
                            {
                                MasterDataVendorId = vendor.Id,
                                MasterDataBusinessFieldId = data.MasterBusinessFieldsIdArray[i],
                                CreatedDate = DateTime.Now,
                                CreatedBy = data.Name,
                                ModifiedDate = DateTime.Now,
                                ModifyBy = data.Name
                            };

                            await _context.MappingDataVendorBusinessFields.AddAsync(mvbf);
                            await _context.SaveChangesAsync();
                        }

                        dbcxtransaction.Commit();

                        response.Code = (int)HttpStatusCode.OK;
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Message = "Save Data Success";
                        response.Data = vendor.Id;

                    }
                    else if (_context.MasterDataVendor.Where(n => n.MasterDataUserId == data.MasterDataUserId).Count() != 0)
                    {
                        var vendor = await _context.MasterDataVendor.FirstOrDefaultAsync(n => n.MasterDataUserId == data.MasterDataUserId);

                        vendor.Name = data.Name;
                        vendor.MasterDataUserId = data.MasterDataUserId;
                        vendor.MasterDataBusinessTypeId = data.MasterDataBusinessTypeId;
                        vendor.MasterDataCountryId = data.MasterDataCountryId;
                        vendor.MasterDataRegionId = data.MasterDataRegionId;
                        vendor.MasterDataCityId = data.MasterDataCityId;
                        vendor.Address = data.Address;
                        vendor.MailingAddress = data.MailingAddress;
                        vendor.PostalCode = data.PostalCode;
                        vendor.CompanyEmail = data.CompanyEmail;
                        vendor.CompanyPhone = data.CompanyPhone;
                        vendor.Pic = data.Pic;
                        vendor.Picemail = data.Picemail;
                        vendor.IdCardNumber = data.IdCardNumber;
                        vendor.Picphone = data.Picphone;
                        vendor.IntegrityPact = data.IntegrityPact;
                        vendor.MasterAgreement = data.MasterAgreement;
                        vendor.Logo = data.Logo;
                        vendor.ModifiedDate = DateTime.Now;
                        vendor.ModifiyBy = data.Name;

                        _context.Entry(vendor).State = EntityState.Modified;

                        await _context.SaveChangesAsync();

                        // remove before edit
                        var businessFields = await _context.MappingDataVendorBusinessFields.Where(n => n.MasterDataVendorId == vendor.Id).ToListAsync();
                        _context.MappingDataVendorBusinessFields.RemoveRange(businessFields);

                        await _context.SaveChangesAsync();

                        // mapping master data vendor with businessfield
                        for (var i = 0; i < data.MasterBusinessFieldsIdArray.Count; i++)
                        {
                            MappingDataVendorBusinessFields mvbf = new MappingDataVendorBusinessFields
                            {
                                MasterDataVendorId = vendor.Id,
                                MasterDataBusinessFieldId = data.MasterBusinessFieldsIdArray[i],
                                CreatedDate = DateTime.Now,
                                CreatedBy = data.Name,
                                ModifiedDate = DateTime.Now,
                                ModifyBy = data.Name
                            };

                            await _context.MappingDataVendorBusinessFields.AddAsync(mvbf);
                            await _context.SaveChangesAsync();
                        }

                        dbcxtransaction.Commit();

                        response.Code = (int)HttpStatusCode.OK;
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Message = "Update data success.";
                        response.Data = vendor.Id;
                    }
                    else
                    {
                        response.Code = (int)HttpStatusCode.BadRequest;
                        response.Status = HttpStatusCode.BadRequest.ToString();
                        response.Message = "Email already exist.";

                        dbcxtransaction.Rollback();
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> GetVendorByUserId(int userId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from vendor in _context.MasterDataVendor
                                    where vendor.MasterDataUserId == userId
                                    select new
                                    {
                                        vendor.Id,
                                        vendor.MasterDataUserId,
                                        vendor.MasterDataBusinessTypeId,
                                        vendor.Name,
                                        vendor.Address,
                                        vendor.MailingAddress,
                                        vendor.MasterDataCountryId,
                                        vendor.MasterDataRegionId,
                                        vendor.MasterDataCityId,
                                        vendor.PostalCode,
                                        vendor.CompanyEmail,
                                        vendor.CompanyPhone,
                                        vendor.Pic,
                                        vendor.Picemail,
                                        vendor.IdCardNumber,
                                        vendor.Picphone,
                                        MasterBusinessFieldsIdArray = (from mvbf in _context.MappingDataVendorBusinessFields
                                                                       where mvbf.MasterDataVendorId == vendor.Id
                                                                       select mvbf.MasterDataBusinessFieldId).ToList()
                                    }).FirstOrDefaultAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetAllVendor()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from vendor in _context.MasterDataVendor
                                     select new
                                     {
                                         vendor.Id,
                                         vendor.Name,
                                         vendor.Address,
                                         vendor.CompanyPhone,
                                         BusinessFields = (from bf in _context.MappingDataVendorBusinessFields
                                                           where bf.MasterDataVendorId == vendor.Id
                                                           select new
                                                           {
                                                               bf.MasterDataBusinessField.Name
                                                           })
                                     }).ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetVendorBankAccount(int masterDatauserId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await(from vendorBank in _context.MappingDataVendorCurrencyBank
                                   where vendorBank.MasterDataUserId == masterDatauserId
                                   select new
                                   {
                                       vendorBank.Id,
                                       Currency = vendorBank.MasterDataCurrency.Code,
                                       BankName = vendorBank.MasterDataBank.Name,
                                       Branch = vendorBank.Area,
                                       vendorBank.City,
                                       vendorBank.AccountNumber,
                                       vendorBank.AccountName,
                                       vendorBank.Status                                      
                                   }).ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> SaveVendorBankAccount(MappingDataVendorCurrencyBank data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    MappingDataVendorCurrencyBank vendorBank = new MappingDataVendorCurrencyBank
                    {
                        MasterDataUserId = data.MasterDataUserId,
                        MasterDataCurrencyId = data.MasterDataCurrencyId,
                        MasterDataBankId = data.MasterDataBankId,
                        Area = data.Area,
                        City = data.City,
                        AccountNumber = data.AccountNumber,
                        AccountName = data.AccountName,
                        CreatedDate = DateTime.Now,
                        CreatedBy = data.CreatedBy,
                        ModifiedDate = DateTime.Now,
                        ModifiyBy = data.ModifiyBy,
                        IsDelete = false
                    };

                    await _context.MappingDataVendorCurrencyBank.AddAsync(vendorBank);
                    await _context.SaveChangesAsync();

                    dbcxtransaction.Commit();

                    response = ResponseConstant.SAVE_SUCCESS;
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }
    }
}
