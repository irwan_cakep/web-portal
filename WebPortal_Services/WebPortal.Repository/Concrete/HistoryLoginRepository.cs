﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortal.Repository.Concrete
{
    public class HistoryLoginRepository : IHistoryLoginRepository
    {
        readonly WebPortalContext _context;
        public HistoryLoginRepository(WebPortalContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> Insert(MasterDataHistoryLogin data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    MasterDataHistoryLogin history = new MasterDataHistoryLogin
                    {
                        UserId = data.UserId,
                        Ip = data.Ip,
                        DateLogin = DateTime.Now
                    };

                    await _context.MasterDataHistoryLogin.AddAsync(history);
                    await _context.SaveChangesAsync();

                    dbcxtransaction.Commit();
                    response = ResponseConstant.SAVE_SUCCESS;
                }
                catch (Exception ex)
                {
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();
                    response.Message = ex.ToString();
                }
            }

            return response;
        }
    }
}
