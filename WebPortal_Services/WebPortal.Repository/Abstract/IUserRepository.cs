﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;

namespace WebPortal.Repository.Abstract
{
    public interface IUserRepository
    {
        Task<BaseResponse> Insert(MasterDataUser data);
        Task<BaseResponse> GetUserByEmail(string email);
        Task<BaseResponse> GetUserByEmailOrUserId(string email);
        Task<BaseResponse> InsertUserFromAd(string email);
        Task<BaseResponse> UpdateUserFromAd(ActiveDirectoryUserModel data);
        Task<BaseResponse> ActivationUser(string activationCode);
    }
}
