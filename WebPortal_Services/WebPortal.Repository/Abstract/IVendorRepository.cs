﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;

namespace WebPortal.Repository.Abstract
{
    public interface IVendorRepository
    {
        Task<BaseResponse> InsertVendorBasicInfo(BasicInfoModel data);
        Task<BaseResponse> InsertVendor(MasterDataVendor data);
        Task<BaseResponse> GetVendorByUserId(int userId);
        Task<BaseResponse> GetAllVendor();
        Task<BaseResponse> GetVendorBankAccount(int masterDataUserId);
        Task<BaseResponse> SaveVendorBankAccount(MappingDataVendorCurrencyBank data);
    }
}
