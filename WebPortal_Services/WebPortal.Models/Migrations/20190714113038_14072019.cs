﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebPortal.Models.Migrations
{
    public partial class _14072019 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MasterDataBank",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    Name = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiyBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    IsDelete = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterDataBank", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MasterDataBusinessFieldsType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Desc = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiyBy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    IsDelete = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterDataBusinessFieldsType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MasterDataBusinessType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(unicode: false, maxLength: 5, nullable: true),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Desc = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiyBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    IsDelete = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterDataBusinessType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MasterDataCountry",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    Name = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    PhoneCode = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    Currency = table.Column<string>(unicode: false, maxLength: 5, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiyBy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    IsDelete = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterDataCountry", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MasterDataCurrency",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(unicode: false, maxLength: 5, nullable: true),
                    Name = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiyBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    IsDelete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterDataCurrency", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MasterDataPaymentMethod",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    Name = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    PaymentDuration = table.Column<int>(nullable: false),
                    DP = table.Column<decimal>(type: "decimal(18, 0)", nullable: true),
                    Progressive = table.Column<decimal>(type: "decimal(18, 0)", nullable: true),
                    FullPayment = table.Column<decimal>(type: "decimal(18, 0)", nullable: true),
                    Retention = table.Column<string>(maxLength: 10, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiyBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    IsDelete = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterDataPaymentMethod", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Chat",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FromName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ToName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Field = table.Column<string>(unicode: false, nullable: true),
                    Date = table.Column<DateTime>(type: "datetime", nullable: true),
                    ConnectionId = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Chat", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "T_ContactUs",
                columns: table => new
                {
                    ContactUsId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Topic = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Feedback = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    PostBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    PostDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsClosed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactUs", x => x.ContactUsId);
                });

            migrationBuilder.CreateTable(
                name: "T_ContactUsDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContactUsId = table.Column<int>(nullable: false),
                    Reply = table.Column<string>(unicode: false, nullable: false),
                    PostBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    PostDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_ContactUsDetail", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_ContentChanges",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Modul = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Type = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    NewsId = table.Column<int>(nullable: true),
                    Judul = table.Column<string>(unicode: false, nullable: true),
                    Caption = table.Column<string>(unicode: false, nullable: true),
                    Isi = table.Column<string>(unicode: false, nullable: true),
                    CategoryCode = table.Column<int>(nullable: true),
                    Image = table.Column<string>(unicode: false, nullable: true),
                    ImagePath = table.Column<string>(unicode: false, nullable: true),
                    Attach = table.Column<string>(unicode: false, nullable: true),
                    AttachPath = table.Column<string>(unicode: false, nullable: true),
                    FileId = table.Column<int>(nullable: true),
                    FileName = table.Column<string>(unicode: false, nullable: true),
                    FileGroup = table.Column<int>(nullable: true),
                    Description = table.Column<string>(unicode: false, nullable: true),
                    LocationFile = table.Column<string>(unicode: false, nullable: true),
                    TutorialCode = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    TutorialTitle = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    TutorialDescription = table.Column<string>(unicode: false, nullable: true),
                    VideoName = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    VideoPath = table.Column<string>(unicode: false, nullable: true),
                    TutorialParent = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    FAQId = table.Column<int>(nullable: true),
                    ProjectCode = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    TypeFAQ = table.Column<int>(nullable: true),
                    GroupFAQ = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    Question = table.Column<string>(unicode: false, nullable: true),
                    Answer = table.Column<string>(unicode: false, nullable: true),
                    PostBy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    PostDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsResponded = table.Column<bool>(nullable: false),
                    Response = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    RespondDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_ContentChanges", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_FAQ",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProjectCode = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    TypeFAQ = table.Column<int>(nullable: true),
                    GroupFAQ = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    Q = table.Column<string>(unicode: false, nullable: false),
                    A = table.Column<string>(unicode: false, nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false),
                    ModifyBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_FAQ", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Files",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FileGroup = table.Column<int>(nullable: false, defaultValueSql: "((99))"),
                    NameFile = table.Column<string>(unicode: false, nullable: false),
                    Description = table.Column<string>(unicode: false, nullable: false),
                    LocationFile = table.Column<string>(unicode: false, nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Files", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "T_HistoryLogin",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    DateLogin = table.Column<DateTime>(type: "datetime", nullable: false),
                    IP = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistoryLogin", x => new { x.Id, x.UserId });
                });

            migrationBuilder.CreateTable(
                name: "T_MsApproval",
                columns: table => new
                {
                    UserLevel = table.Column<int>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_MsApproval", x => x.UserLevel);
                });

            migrationBuilder.CreateTable(
                name: "T_MsCategory",
                columns: table => new
                {
                    CategoryCode = table.Column<int>(nullable: false),
                    CategoryName = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_MsCategory", x => x.CategoryCode);
                });

            migrationBuilder.CreateTable(
                name: "T_MsDomain",
                columns: table => new
                {
                    DomainLink = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    DomainName = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_MsDomain", x => x.DomainLink);
                });

            migrationBuilder.CreateTable(
                name: "T_MsFileGroup",
                columns: table => new
                {
                    FileGroup = table.Column<int>(nullable: false),
                    Description = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_MsFileGroup", x => x.FileGroup);
                });

            migrationBuilder.CreateTable(
                name: "T_MsMenu",
                columns: table => new
                {
                    MenuId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MenuCode = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    MenuName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    MenuURL = table.Column<string>(unicode: false, maxLength: 150, nullable: true),
                    MenuGroup = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    MenuParent = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    MenuTitle = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    IsLink = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsLoginRequired = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifyBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MsMenu", x => x.MenuId);
                });

            migrationBuilder.CreateTable(
                name: "T_MsMenuContent",
                columns: table => new
                {
                    Menu = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    MenuContent = table.Column<string>(unicode: false, nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_MsMenuContent", x => x.Menu);
                });

            migrationBuilder.CreateTable(
                name: "T_MsParameter",
                columns: table => new
                {
                    FlagId = table.Column<int>(nullable: false),
                    ParameterCode = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ParentCode = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifyBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_MsParameter", x => new { x.FlagId, x.ParameterCode });
                });

            migrationBuilder.CreateTable(
                name: "T_MsParameter_Flag",
                columns: table => new
                {
                    FlagId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_MsParameter_Flag", x => x.FlagId);
                });

            migrationBuilder.CreateTable(
                name: "T_MsProject",
                columns: table => new
                {
                    ProjectCode = table.Column<string>(unicode: false, maxLength: 10, nullable: false),
                    ProjectName = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false),
                    ModifyBy = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_MsProject", x => x.ProjectCode);
                });

            migrationBuilder.CreateTable(
                name: "T_MsSite",
                columns: table => new
                {
                    SiteCode = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SiteName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Description = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    NavURL = table.Column<string>(unicode: false, nullable: true),
                    Image = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    IsShow = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiteMenu", x => x.SiteCode);
                });

            migrationBuilder.CreateTable(
                name: "T_MsUser_Site",
                columns: table => new
                {
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    SiteCode = table.Column<int>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SiteUserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    SitePassword = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_MsUser_Site", x => new { x.UserId, x.SiteCode });
                });

            migrationBuilder.CreateTable(
                name: "T_MsUserGroup",
                columns: table => new
                {
                    UserLevel = table.Column<int>(nullable: false),
                    Description = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserGroup", x => x.UserLevel);
                });

            migrationBuilder.CreateTable(
                name: "T_MsUserGroup_Menu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MenuCode = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    UserLevel = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_MsUserGroup_Menu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_MsUserGroup_Site",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SiteCode = table.Column<int>(nullable: false),
                    UserLevel = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_MsUserGroup_Site", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_News",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Judul = table.Column<string>(unicode: false, nullable: false),
                    CategoryCode = table.Column<int>(nullable: false),
                    Image = table.Column<string>(unicode: false, nullable: false),
                    ImagePath = table.Column<string>(unicode: false, nullable: true),
                    PostBy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Caption = table.Column<string>(unicode: false, nullable: false),
                    Isi = table.Column<string>(nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifyBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Attach = table.Column<string>(unicode: false, maxLength: 500, nullable: true),
                    AttachPath = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_News", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Question",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Topic = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    Question = table.Column<string>(unicode: false, nullable: false),
                    PostBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    PostDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsClosed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Question", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_QuestionDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuestionId = table.Column<int>(nullable: false),
                    Answer = table.Column<string>(unicode: false, nullable: false),
                    PostBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    PostDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_QuestionDetail", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Recruitment",
                columns: table => new
                {
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    BirthDate = table.Column<DateTime>(type: "date", nullable: false),
                    Email = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Gender = table.Column<string>(unicode: false, maxLength: 1, nullable: false, defaultValueSql: "('M')"),
                    Phone = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    Address = table.Column<string>(unicode: false, maxLength: 500, nullable: false),
                    Country = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    City = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    PostalCode = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Nationality = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Recrutment", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "T_Recruitment_Academy",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Institute = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Location = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    StudyFrom = table.Column<string>(unicode: false, maxLength: 4, nullable: false),
                    StudyTo = table.Column<string>(unicode: false, maxLength: 4, nullable: true),
                    Qualification = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Major = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CertificateFileName = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CertificateFilePath = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Recruitment_Academy", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Recruitment_Academy_NonFormal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Education = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    Institute = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Location = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CertificateFileName = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CertificateFilePath = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Recruitment_Academy_NonFormal", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Recruitment_Document",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Description = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FileName = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    FilePath = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    Notes = table.Column<string>(unicode: false, maxLength: 500, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Recruitment_Document", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Recruitment_Experience",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CompanyName = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    Location = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    State = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    PositionTitle = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    PositionLevel = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    MonthStart = table.Column<string>(unicode: false, maxLength: 3, nullable: false),
                    YearStart = table.Column<string>(unicode: false, maxLength: 4, nullable: false),
                    MonthEnd = table.Column<string>(unicode: false, maxLength: 3, nullable: true),
                    YearEnd = table.Column<string>(unicode: false, maxLength: 4, nullable: true),
                    StillPresent = table.Column<bool>(nullable: false, defaultValueSql: "((1))"),
                    Specialization = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Industry = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Salary = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    SalaryCurrency = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ExperienceDescription = table.Column<string>(unicode: false, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Recruitment_Experience", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Recruitment_Family",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    FamilyName = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Gender = table.Column<string>(unicode: false, maxLength: 1, nullable: false, defaultValueSql: "('M')"),
                    Relation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Recruitment_Family", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Recruitment_Skill",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Skill = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Proficiency = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Recruitment_Skill", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_ResetPassword",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Credential = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    Token = table.Column<string>(unicode: false, maxLength: 32, nullable: false),
                    ExpiredDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsUsed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_ResetPassword", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Tutorial",
                columns: table => new
                {
                    TutorialCode = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Id = table.Column<int>(nullable: true),
                    TutorialTitle = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    TutorialDescription = table.Column<string>(unicode: false, nullable: false),
                    VideoName = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    VideoPath = table.Column<string>(unicode: false, nullable: true),
                    TutorialParent = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifyBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Tutorial", x => x.TutorialCode);
                });

            migrationBuilder.CreateTable(
                name: "T_Vendor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    VendorNumber = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    CompanyName = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    BusinessForm = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    BusinessField = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Country = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    City = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Address = table.Column<string>(unicode: false, maxLength: 500, nullable: false),
                    PostalCode = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Email = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Phone = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    Fax = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    CompanyWeb = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    ContactPerson = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    CPNumber = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    CPEmail = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    IsSubmitted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Vendor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Vendor_Bank",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Bank = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Branch = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    Country = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    City = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Currency = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    AccountNumber = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    AccountName = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Vendor_Bank", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Vendor_Branches",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Country = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    City = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Address = table.Column<string>(unicode: false, maxLength: 500, nullable: false),
                    PostalCode = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Email = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Phone = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    Fax = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    ContactPerson = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    CPNumber = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    CPEmail = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Vendor_Branches", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Vendor_Document",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Description = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    FileName = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    FilePath = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    Notes = table.Column<string>(unicode: false, maxLength: 500, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Vendor_Document", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Vendor_Item",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    SEGMENT1 = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    DESCRIPTION = table.Column<string>(unicode: false, maxLength: 300, nullable: false),
                    PRIMARY_UOM_CODE = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    INVOICEABLE_ITEM_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('Y')"),
                    INVOICE_ENABLED_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('Y')"),
                    ALLOW_ITEM_DESC_UPDATE_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('N')"),
                    ORGANIZATION_CODE = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    TRANSACTION_TYPE = table.Column<string>(unicode: false, maxLength: 50, nullable: true, defaultValueSql: "('CREATE')"),
                    PROCESS_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "((1))"),
                    INVENTORY_ITEM_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('Y')"),
                    STOCK_ENABLED_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('Y')"),
                    MTL_TRANSACTIONS_ENABLED_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('Y')"),
                    RESERVEABLE_TYPE = table.Column<string>(unicode: false, maxLength: 10, nullable: true, defaultValueSql: "((1))"),
                    COSTING_ENABLED_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('Y')"),
                    INVENTORY_ASSET_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('Y')"),
                    PURCHASING_ITEM_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('Y')"),
                    PURCHASING_ENABLED_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('Y')"),
                    RECEIPT_REQUIRED_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('Y')"),
                    MUST_USE_APPROVED_VENDOR_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('N')"),
                    CUSTOMER_ORDER_ENABLED_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('N')"),
                    CUSTOMER_ORDER_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('N')"),
                    SHIPPABLE_ITEM_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('N')"),
                    SO_TRANSACTIONS_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('N')"),
                    RETURNABLE_FLAG = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('N')"),
                    Notes = table.Column<string>(unicode: false, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Vendor_Item", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Vendor_Legal",
                columns: table => new
                {
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SIUP = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    SIUPDocumentName = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    SIUPDocumentPath = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    SIUPValidTo = table.Column<DateTime>(type: "date", nullable: false),
                    TDP = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    TDPDocumentName = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    TDPDocumentPath = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    TDPValidTo = table.Column<DateTime>(type: "date", nullable: false),
                    SKDP = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    SKDPDocumentName = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    SKDPDocumentPath = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    SKDPValidTo = table.Column<DateTime>(type: "date", nullable: true),
                    DeedOfIncorporation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    DoIDocumentName = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    DoIDocumentPath = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    CertificateOfEstablishment = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CoEDocumentName = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CoEDocumentPath = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    DeedOfChange = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    DoCDocumentName = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    DoCDocumentPath = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    CertificateOfChange = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CoCDocumentName = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CoCDocumentPath = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Vendor_Legal", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "T_Vendor_Tax",
                columns: table => new
                {
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NPWP = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    NPWPAddress = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    NPWPDocumentName = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    NPWPDocumentPath = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    SKPKP = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    SKPKPDocumentName = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    SKPKPDocumentPath = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    PPn = table.Column<bool>(nullable: false),
                    PPh = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Vendor_Tax", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "MasterDataBusinessFields",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MasterDataBusinessFieldTypeId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    KBLI = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiyBy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    IsDelete = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterDataBusinessFields", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterDataBusinessFieldsType_MasterDataBusinessFieldsType",
                        column: x => x.MasterDataBusinessFieldTypeId,
                        principalTable: "MasterDataBusinessFieldsType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MasterDataRegion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    Name = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    PhoneCode = table.Column<string>(unicode: false, maxLength: 5, nullable: true),
                    MasterDataCountryId = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiyBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    IsDelete = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterDataRegion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterDataCountry_MasterDataRegion",
                        column: x => x.MasterDataCountryId,
                        principalTable: "MasterDataCountry",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_MsUser",
                columns: table => new
                {
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    UserName = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Password = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ForgetPassword = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Region = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    email = table.Column<string>(unicode: false, maxLength: 1000, nullable: false),
                    SecondaryEmail = table.Column<string>(unicode: false, maxLength: 1000, nullable: true),
                    Jabatan = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Divisi = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ExtensionNumber = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    LastLogin = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastLogout = table.Column<DateTime>(type: "datetime", nullable: true),
                    Session = table.Column<string>(maxLength: 1000, nullable: true),
                    UserLevel = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: true),
                    ModifyBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "date", nullable: true),
                    Image = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Isactive = table.Column<bool>(nullable: false),
                    IP = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    IsTemporary = table.Column<bool>(nullable: false),
                    StartAccess = table.Column<DateTime>(type: "date", nullable: true),
                    EndAccess = table.Column<DateTime>(type: "date", nullable: true),
                    IsUserAD = table.Column<bool>(nullable: false),
                    Domain = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    LastAccessFAQ = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastAccessTutorial = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastAccessDocument = table.Column<DateTime>(type: "datetime", nullable: true),
                    ActivateLink = table.Column<string>(unicode: false, maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MsUser", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_MsUser_UserGroup",
                        column: x => x.UserLevel,
                        principalTable: "T_MsUserGroup",
                        principalColumn: "UserLevel",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_CommentNews",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Email = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Field = table.Column<string>(unicode: false, nullable: false),
                    IdComment = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(type: "date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_CommentNews", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CommentNews_News",
                        column: x => x.IdComment,
                        principalTable: "T_News",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MappingDataBusinessFieldPaymentMethods",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MasterDataBusinessFieldId = table.Column<int>(nullable: false),
                    MasterDataPaymentMethodId = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiyBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    IsDelete = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MappingDataBusinessFieldPaymentMethods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MappingDataBusinessFieldPaymentMethods_MasterDataBusinessFields",
                        column: x => x.MasterDataBusinessFieldId,
                        principalTable: "MasterDataBusinessFields",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MappingDataBusinessFieldPaymentMethods_MasterDataPaymentMethod",
                        column: x => x.MasterDataPaymentMethodId,
                        principalTable: "MasterDataPaymentMethod",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MasterDataCity",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MasterDataCountryId = table.Column<int>(nullable: false),
                    MasterDataRegionId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    Name = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifyBy = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    IsDelete = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterDataCity", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterDataCity_MasterDataCountry",
                        column: x => x.MasterDataCountryId,
                        principalTable: "MasterDataCountry",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MasterDataCity_MasterDataRegion",
                        column: x => x.MasterDataRegionId,
                        principalTable: "MasterDataRegion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MasterDataVendor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    MasterDataUserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    MasterDataBusinessTypeId = table.Column<int>(nullable: false),
                    MasterDataBusinessFieldsId = table.Column<int>(nullable: false),
                    MasterDataCountryId = table.Column<int>(nullable: false),
                    MasterDataRegionId = table.Column<int>(nullable: false),
                    MasterDataCityId = table.Column<int>(nullable: false),
                    Address = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    PostalCode = table.Column<string>(unicode: false, maxLength: 10, nullable: false),
                    CompanyEmail = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    PIC = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    PICEmail = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    IdCardNumber = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    IntegrityPact = table.Column<bool>(nullable: true),
                    MasterAgreement = table.Column<bool>(nullable: true),
                    Logo = table.Column<string>(unicode: false, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiyBy = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    IsDelete = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterDataVendor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterDataVendor_MasterDataBusinessFields",
                        column: x => x.MasterDataBusinessFieldsId,
                        principalTable: "MasterDataBusinessFields",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MasterDataVendor_MasterDataBusinessType",
                        column: x => x.MasterDataBusinessTypeId,
                        principalTable: "MasterDataBusinessType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MasterDataVendor_MasterDataCity",
                        column: x => x.MasterDataCityId,
                        principalTable: "MasterDataCity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MasterDataVendor_MasterDataCountry",
                        column: x => x.MasterDataCountryId,
                        principalTable: "MasterDataCountry",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MasterDataVendor_MasterDataRegion",
                        column: x => x.MasterDataRegionId,
                        principalTable: "MasterDataRegion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MasterDataVendor_MasterDataUser",
                        column: x => x.MasterDataUserId,
                        principalTable: "T_MsUser",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MappingDataVendorCurrencyBank",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MasterDataVendorId = table.Column<int>(nullable: false),
                    MasterDataCurrencyId = table.Column<int>(nullable: false),
                    MasterDataBankId = table.Column<int>(nullable: false),
                    Area = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    City = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    AccountNumber = table.Column<int>(nullable: false),
                    Status = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    AccountName = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiyBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    IsDelete = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MappingDataVendorCurrencyBank", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MappingDataVendorCurrencyBank_MasterDataBank",
                        column: x => x.MasterDataBankId,
                        principalTable: "MasterDataBank",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MappingDataVendorCurrencyBank_MasterDataCurrency",
                        column: x => x.MasterDataCurrencyId,
                        principalTable: "MasterDataCurrency",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MappingDataVendorCurrencyBank_MasterDataVendor",
                        column: x => x.MasterDataVendorId,
                        principalTable: "MasterDataVendor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MappingDataVendorLegal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MasterDataVendorId = table.Column<int>(nullable: false),
                    NIB = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    NIBAttachment = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    BusinessPermit = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    BusinessPermitAttachment = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    SIUP = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    SIUPExpiredDate = table.Column<DateTime>(type: "date", nullable: true),
                    SIUPAttachment = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    TDP = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    TDPExpiredDate = table.Column<DateTime>(type: "date", nullable: true),
                    TDPAttachment = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    MemorandumOfAssociation = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    MemorandumOfAssociationAttachment = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    DecissionLetterMenkumham = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    DecissionLetterMenkumhamAttachment = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    MemorandumOfAssociationChanging = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    MemorandumOfAssociationChangingAttachment = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    DecissionLetterMenkumhamChanging = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    DecissionLetterMenkumhamChangingAttachment = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    ManagementStructure = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    Signing = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiyBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    IsDelete = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MappingDataVendorLegal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MappingDataVendorLegal_MasterDataVendor",
                        column: x => x.MasterDataVendorId,
                        principalTable: "MasterDataVendor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MappingDataBusinessFieldPaymentMethods_MasterDataBusinessFieldId",
                table: "MappingDataBusinessFieldPaymentMethods",
                column: "MasterDataBusinessFieldId");

            migrationBuilder.CreateIndex(
                name: "IX_MappingDataBusinessFieldPaymentMethods_MasterDataPaymentMethodId",
                table: "MappingDataBusinessFieldPaymentMethods",
                column: "MasterDataPaymentMethodId");

            migrationBuilder.CreateIndex(
                name: "IX_MappingDataVendorCurrencyBank_MasterDataBankId",
                table: "MappingDataVendorCurrencyBank",
                column: "MasterDataBankId");

            migrationBuilder.CreateIndex(
                name: "IX_MappingDataVendorCurrencyBank_MasterDataCurrencyId",
                table: "MappingDataVendorCurrencyBank",
                column: "MasterDataCurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_MappingDataVendorCurrencyBank_MasterDataVendorId",
                table: "MappingDataVendorCurrencyBank",
                column: "MasterDataVendorId");

            migrationBuilder.CreateIndex(
                name: "IX_MappingDataVendorLegal_MasterDataVendorId",
                table: "MappingDataVendorLegal",
                column: "MasterDataVendorId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterDataBusinessFields_MasterDataBusinessFieldTypeId",
                table: "MasterDataBusinessFields",
                column: "MasterDataBusinessFieldTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterDataCity_MasterDataCountryId",
                table: "MasterDataCity",
                column: "MasterDataCountryId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterDataCity_MasterDataRegionId",
                table: "MasterDataCity",
                column: "MasterDataRegionId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterDataRegion_MasterDataCountryId",
                table: "MasterDataRegion",
                column: "MasterDataCountryId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterDataVendor_MasterDataBusinessFieldsId",
                table: "MasterDataVendor",
                column: "MasterDataBusinessFieldsId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterDataVendor_MasterDataBusinessTypeId",
                table: "MasterDataVendor",
                column: "MasterDataBusinessTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterDataVendor_MasterDataCityId",
                table: "MasterDataVendor",
                column: "MasterDataCityId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterDataVendor_MasterDataCountryId",
                table: "MasterDataVendor",
                column: "MasterDataCountryId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterDataVendor_MasterDataRegionId",
                table: "MasterDataVendor",
                column: "MasterDataRegionId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterDataVendor_MasterDataUserId",
                table: "MasterDataVendor",
                column: "MasterDataUserId");

            migrationBuilder.CreateIndex(
                name: "IX_T_CommentNews_IdComment",
                table: "T_CommentNews",
                column: "IdComment");

            migrationBuilder.CreateIndex(
                name: "IX_T_MsUser_UserLevel",
                table: "T_MsUser",
                column: "UserLevel");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MappingDataBusinessFieldPaymentMethods");

            migrationBuilder.DropTable(
                name: "MappingDataVendorCurrencyBank");

            migrationBuilder.DropTable(
                name: "MappingDataVendorLegal");

            migrationBuilder.DropTable(
                name: "T_Chat");

            migrationBuilder.DropTable(
                name: "T_CommentNews");

            migrationBuilder.DropTable(
                name: "T_ContactUs");

            migrationBuilder.DropTable(
                name: "T_ContactUsDetail");

            migrationBuilder.DropTable(
                name: "T_ContentChanges");

            migrationBuilder.DropTable(
                name: "T_FAQ");

            migrationBuilder.DropTable(
                name: "T_Files");

            migrationBuilder.DropTable(
                name: "T_HistoryLogin");

            migrationBuilder.DropTable(
                name: "T_MsApproval");

            migrationBuilder.DropTable(
                name: "T_MsCategory");

            migrationBuilder.DropTable(
                name: "T_MsDomain");

            migrationBuilder.DropTable(
                name: "T_MsFileGroup");

            migrationBuilder.DropTable(
                name: "T_MsMenu");

            migrationBuilder.DropTable(
                name: "T_MsMenuContent");

            migrationBuilder.DropTable(
                name: "T_MsParameter");

            migrationBuilder.DropTable(
                name: "T_MsParameter_Flag");

            migrationBuilder.DropTable(
                name: "T_MsProject");

            migrationBuilder.DropTable(
                name: "T_MsSite");

            migrationBuilder.DropTable(
                name: "T_MsUser_Site");

            migrationBuilder.DropTable(
                name: "T_MsUserGroup_Menu");

            migrationBuilder.DropTable(
                name: "T_MsUserGroup_Site");

            migrationBuilder.DropTable(
                name: "T_Question");

            migrationBuilder.DropTable(
                name: "T_QuestionDetail");

            migrationBuilder.DropTable(
                name: "T_Recruitment");

            migrationBuilder.DropTable(
                name: "T_Recruitment_Academy");

            migrationBuilder.DropTable(
                name: "T_Recruitment_Academy_NonFormal");

            migrationBuilder.DropTable(
                name: "T_Recruitment_Document");

            migrationBuilder.DropTable(
                name: "T_Recruitment_Experience");

            migrationBuilder.DropTable(
                name: "T_Recruitment_Family");

            migrationBuilder.DropTable(
                name: "T_Recruitment_Skill");

            migrationBuilder.DropTable(
                name: "T_ResetPassword");

            migrationBuilder.DropTable(
                name: "T_Tutorial");

            migrationBuilder.DropTable(
                name: "T_Vendor");

            migrationBuilder.DropTable(
                name: "T_Vendor_Bank");

            migrationBuilder.DropTable(
                name: "T_Vendor_Branches");

            migrationBuilder.DropTable(
                name: "T_Vendor_Document");

            migrationBuilder.DropTable(
                name: "T_Vendor_Item");

            migrationBuilder.DropTable(
                name: "T_Vendor_Legal");

            migrationBuilder.DropTable(
                name: "T_Vendor_Tax");

            migrationBuilder.DropTable(
                name: "MasterDataPaymentMethod");

            migrationBuilder.DropTable(
                name: "MasterDataBank");

            migrationBuilder.DropTable(
                name: "MasterDataCurrency");

            migrationBuilder.DropTable(
                name: "MasterDataVendor");

            migrationBuilder.DropTable(
                name: "T_News");

            migrationBuilder.DropTable(
                name: "MasterDataBusinessFields");

            migrationBuilder.DropTable(
                name: "MasterDataBusinessType");

            migrationBuilder.DropTable(
                name: "MasterDataCity");

            migrationBuilder.DropTable(
                name: "T_MsUser");

            migrationBuilder.DropTable(
                name: "MasterDataBusinessFieldsType");

            migrationBuilder.DropTable(
                name: "MasterDataRegion");

            migrationBuilder.DropTable(
                name: "T_MsUserGroup");

            migrationBuilder.DropTable(
                name: "MasterDataCountry");
        }
    }
}
