﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPortal.Models.Models
{
    public class NewPasswordModel
    {
        public string Token { get; set; }
        public string NewPassword { get; set; }
    }
}
