﻿using System;
using System.Collections.Generic;
using System.Text;
using WebPortal.Models.Entities;

namespace WebPortal.Models.Models
{
    public class BasicInfoModel : MasterDataVendor
    {
        public List<int> MasterBusinessFieldsIdArray { get; set; }
    }
}
