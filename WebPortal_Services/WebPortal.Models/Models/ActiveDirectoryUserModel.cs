﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPortal.Models.Models
{
    public class ActiveDirectoryUserModel
    {
        public string Domain { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
    }
}
