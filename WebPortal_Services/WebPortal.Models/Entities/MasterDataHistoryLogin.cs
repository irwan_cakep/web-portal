﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataHistoryLogin
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string UserId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime DateLogin { get; set; }
        [Required]
        [Column("IP")]
        [StringLength(50)]
        public string Ip { get; set; }
    }
}
