﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebPortal.Models.Entities
{
    public partial class WebPortalContext : DbContext
    {
        public WebPortalContext()
        {
        }

        public WebPortalContext(DbContextOptions<WebPortalContext> options)
            : base(options)
        {
        }

        public virtual DbSet<MappingDataBusinessFieldPaymentMethods> MappingDataBusinessFieldPaymentMethods { get; set; }
        public virtual DbSet<MappingDataVendorBusinessFields> MappingDataVendorBusinessFields { get; set; }
        public virtual DbSet<MappingDataVendorCurrencyBank> MappingDataVendorCurrencyBank { get; set; }
        public virtual DbSet<MappingDataVendorLegal> MappingDataVendorLegal { get; set; }
        public virtual DbSet<MasterDataBank> MasterDataBank { get; set; }
        public virtual DbSet<MasterDataBusinessFields> MasterDataBusinessFields { get; set; }
        public virtual DbSet<MasterDataBusinessFieldsType> MasterDataBusinessFieldsType { get; set; }
        public virtual DbSet<MasterDataBusinessType> MasterDataBusinessType { get; set; }
        public virtual DbSet<MasterDataCity> MasterDataCity { get; set; }
        public virtual DbSet<MasterDataCountry> MasterDataCountry { get; set; }
        public virtual DbSet<MasterDataCurrency> MasterDataCurrency { get; set; }
        public virtual DbSet<MasterDataDomain> MasterDataDomain { get; set; }
        public virtual DbSet<MasterDataHistoryLogin> MasterDataHistoryLogin { get; set; }
        public virtual DbSet<MasterDataPaymentMethod> MasterDataPaymentMethod { get; set; }
        public virtual DbSet<MasterDataRegion> MasterDataRegion { get; set; }
        public virtual DbSet<MasterDataResetPassword> MasterDataResetPassword { get; set; }
        public virtual DbSet<MasterDataUser> MasterDataUser { get; set; }
        public virtual DbSet<MasterDataUserGroup> MasterDataUserGroup { get; set; }
        public virtual DbSet<MasterDataVendor> MasterDataVendor { get; set; }

        // Unable to generate entity type for table 'dbo.MasterDataRegionTemp'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.MasterDataCityTemp'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=fap-000-056.ad.fap-agri.com;Database=webportal;User Id=webportal; Password=webportal1234;ConnectRetryCount=0");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<MappingDataBusinessFieldPaymentMethods>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiyBy).IsUnicode(false);

                entity.HasOne(d => d.MasterDataBusinessField)
                    .WithMany(p => p.MappingDataBusinessFieldPaymentMethods)
                    .HasForeignKey(d => d.MasterDataBusinessFieldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataBusinessFieldPaymentMethods_MasterDataBusinessFields");

                entity.HasOne(d => d.MasterDataPaymentMethod)
                    .WithMany(p => p.MappingDataBusinessFieldPaymentMethods)
                    .HasForeignKey(d => d.MasterDataPaymentMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataBusinessFieldPaymentMethods_MasterDataPaymentMethod");
            });

            modelBuilder.Entity<MappingDataVendorBusinessFields>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifyBy).IsUnicode(false);

                entity.HasOne(d => d.MasterDataBusinessField)
                    .WithMany(p => p.MappingDataVendorBusinessFields)
                    .HasForeignKey(d => d.MasterDataBusinessFieldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataVendorBusinessFields_MasterDataBusinessFields");
            });

            modelBuilder.Entity<MappingDataVendorCurrencyBank>(entity =>
            {
                entity.Property(e => e.AccountName).IsUnicode(false);

                entity.Property(e => e.AccountNumber).IsUnicode(false);

                entity.Property(e => e.Area).IsUnicode(false);

                entity.Property(e => e.City).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiyBy).IsUnicode(false);

                entity.Property(e => e.Status).IsUnicode(false);

                entity.HasOne(d => d.MasterDataBank)
                    .WithMany(p => p.MappingDataVendorCurrencyBank)
                    .HasForeignKey(d => d.MasterDataBankId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataVendorCurrencyBank_MasterDataBank");

                entity.HasOne(d => d.MasterDataCurrency)
                    .WithMany(p => p.MappingDataVendorCurrencyBank)
                    .HasForeignKey(d => d.MasterDataCurrencyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataVendorCurrencyBank_MasterDataCurrency");

                entity.HasOne(d => d.MasterDataUser)
                    .WithMany(p => p.MappingDataVendorCurrencyBank)
                    .HasForeignKey(d => d.MasterDataUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataVendorCurrencyBank_MasterDataUser");
            });

            modelBuilder.Entity<MappingDataVendorLegal>(entity =>
            {
                entity.Property(e => e.BusinessPermit).IsUnicode(false);

                entity.Property(e => e.BusinessPermitAttachment).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.DecissionLetterMenkumham).IsUnicode(false);

                entity.Property(e => e.DecissionLetterMenkumhamAttachment).IsUnicode(false);

                entity.Property(e => e.DecissionLetterMenkumhamChanging).IsUnicode(false);

                entity.Property(e => e.DecissionLetterMenkumhamChangingAttachment).IsUnicode(false);

                entity.Property(e => e.ManagementStructure).IsUnicode(false);

                entity.Property(e => e.MemorandumOfAssociation).IsUnicode(false);

                entity.Property(e => e.MemorandumOfAssociationAttachment).IsUnicode(false);

                entity.Property(e => e.MemorandumOfAssociationChanging).IsUnicode(false);

                entity.Property(e => e.MemorandumOfAssociationChangingAttachment).IsUnicode(false);

                entity.Property(e => e.ModifiyBy).IsUnicode(false);

                entity.Property(e => e.Nib).IsUnicode(false);

                entity.Property(e => e.Nibattachment).IsUnicode(false);

                entity.Property(e => e.Signing).IsUnicode(false);

                entity.Property(e => e.Siup).IsUnicode(false);

                entity.Property(e => e.Siupattachment).IsUnicode(false);

                entity.Property(e => e.Tdp).IsUnicode(false);

                entity.Property(e => e.Tdpattachment).IsUnicode(false);

                entity.HasOne(d => d.MasterDataVendor)
                    .WithMany(p => p.MappingDataVendorLegal)
                    .HasForeignKey(d => d.MasterDataVendorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataVendorLegal_MasterDataVendor");
            });

            modelBuilder.Entity<MasterDataBank>(entity =>
            {
                entity.Property(e => e.Code).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiyBy).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<MasterDataBusinessFields>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiyBy).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);

                entity.HasOne(d => d.MasterDataBusinessFieldType)
                    .WithMany(p => p.MasterDataBusinessFields)
                    .HasForeignKey(d => d.MasterDataBusinessFieldTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataBusinessFieldsType_MasterDataBusinessFieldsType");
            });

            modelBuilder.Entity<MasterDataBusinessFieldsType>(entity =>
            {
                entity.Property(e => e.Code).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Desc).IsUnicode(false);

                entity.Property(e => e.ModifiyBy).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<MasterDataBusinessType>(entity =>
            {
                entity.Property(e => e.Code).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Desc).IsUnicode(false);

                entity.Property(e => e.ModifiyBy).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<MasterDataCity>(entity =>
            {
                entity.Property(e => e.Code).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifyBy).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);

                entity.HasOne(d => d.MasterDataCountry)
                    .WithMany(p => p.MasterDataCity)
                    .HasForeignKey(d => d.MasterDataCountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataCity_MasterDataCountry");

                entity.HasOne(d => d.MasterDataRegion)
                    .WithMany(p => p.MasterDataCity)
                    .HasForeignKey(d => d.MasterDataRegionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataCity_MasterDataRegion");
            });

            modelBuilder.Entity<MasterDataCountry>(entity =>
            {
                entity.Property(e => e.Code).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Currency).IsUnicode(false);

                entity.Property(e => e.ModifiyBy).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.PhoneCode).IsUnicode(false);
            });

            modelBuilder.Entity<MasterDataCurrency>(entity =>
            {
                entity.Property(e => e.Code).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiyBy).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<MasterDataDomain>(entity =>
            {
                entity.HasKey(e => e.DomainLink)
                    .HasName("PK_T_MsDomain");

                entity.Property(e => e.DomainLink)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DomainName).IsUnicode(false);
            });

            modelBuilder.Entity<MasterDataHistoryLogin>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.UserId })
                    .HasName("PK_HistoryLogin");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.UserId).IsUnicode(false);

                entity.Property(e => e.Ip).IsUnicode(false);
            });

            modelBuilder.Entity<MasterDataPaymentMethod>(entity =>
            {
                entity.Property(e => e.Code).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiyBy).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<MasterDataRegion>(entity =>
            {
                entity.Property(e => e.Code).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.ModifiyBy).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.PhoneCode).IsUnicode(false);

                entity.HasOne(d => d.MasterDataCountry)
                    .WithMany(p => p.MasterDataRegion)
                    .HasForeignKey(d => d.MasterDataCountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataCountry_MasterDataRegion");
            });

            modelBuilder.Entity<MasterDataResetPassword>(entity =>
            {
                entity.Property(e => e.Credential).IsUnicode(false);

                entity.Property(e => e.Token).IsUnicode(false);
            });

            modelBuilder.Entity<MasterDataUser>(entity =>
            {
                entity.Property(e => e.ActivateLink).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Divisi).IsUnicode(false);

                entity.Property(e => e.Domain).IsUnicode(false);

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.ExtensionNumber).IsUnicode(false);

                entity.Property(e => e.ForgetPassword).IsUnicode(false);

                entity.Property(e => e.Image).IsUnicode(false);

                entity.Property(e => e.Ip).IsUnicode(false);

                entity.Property(e => e.Jabatan).IsUnicode(false);

                entity.Property(e => e.ModifyBy).IsUnicode(false);

                entity.Property(e => e.Password).IsUnicode(false);

                entity.Property(e => e.Region).IsUnicode(false);

                entity.Property(e => e.SecondaryEmail).IsUnicode(false);

                entity.Property(e => e.UserId).IsUnicode(false);

                entity.Property(e => e.UserName).IsUnicode(false);

                entity.HasOne(d => d.UserLevelNavigation)
                    .WithMany(p => p.MasterDataUser)
                    .HasForeignKey(d => d.UserLevel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MsUser_UserGroup");
            });

            modelBuilder.Entity<MasterDataUserGroup>(entity =>
            {
                entity.HasKey(e => e.UserLevel)
                    .HasName("PK_UserGroup");

                entity.Property(e => e.UserLevel).ValueGeneratedNever();

                entity.Property(e => e.Description).IsUnicode(false);
            });

            modelBuilder.Entity<MasterDataVendor>(entity =>
            {
                entity.Property(e => e.Address).IsUnicode(false);

                entity.Property(e => e.CompanyEmail).IsUnicode(false);

                entity.Property(e => e.CompanyPhone).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.IdCardNumber).IsUnicode(false);

                entity.Property(e => e.Logo).IsUnicode(false);

                entity.Property(e => e.MailingAddress).IsUnicode(false);

                entity.Property(e => e.ModifiyBy).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.Pic).IsUnicode(false);

                entity.Property(e => e.Picemail).IsUnicode(false);

                entity.Property(e => e.Picphone).IsUnicode(false);

                entity.Property(e => e.PostalCode).IsUnicode(false);

                entity.HasOne(d => d.MasterDataBusinessType)
                    .WithMany(p => p.MasterDataVendor)
                    .HasForeignKey(d => d.MasterDataBusinessTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataVendor_MasterDataBusinessType");

                entity.HasOne(d => d.MasterDataCity)
                    .WithMany(p => p.MasterDataVendor)
                    .HasForeignKey(d => d.MasterDataCityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataVendor_MasterDataCity");

                entity.HasOne(d => d.MasterDataCountry)
                    .WithMany(p => p.MasterDataVendor)
                    .HasForeignKey(d => d.MasterDataCountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataVendor_MasterDataCountry");

                entity.HasOne(d => d.MasterDataRegion)
                    .WithMany(p => p.MasterDataVendor)
                    .HasForeignKey(d => d.MasterDataRegionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataVendor_MasterDataRegion");
            });
        }
    }
}
