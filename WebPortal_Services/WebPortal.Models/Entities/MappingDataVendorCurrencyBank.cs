﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MappingDataVendorCurrencyBank
    {
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        public int MasterDataCurrencyId { get; set; }
        public int MasterDataBankId { get; set; }
        [Required]
        [StringLength(20)]
        public string Area { get; set; }
        [Required]
        [StringLength(20)]
        public string City { get; set; }
        [Required]
        [StringLength(50)]
        public string AccountNumber { get; set; }
        [StringLength(10)]
        public string Status { get; set; }
        [Required]
        [StringLength(20)]
        public string AccountName { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifiyBy { get; set; }
        public bool? IsDelete { get; set; }

        [ForeignKey("MasterDataBankId")]
        [InverseProperty("MappingDataVendorCurrencyBank")]
        public virtual MasterDataBank MasterDataBank { get; set; }
        [ForeignKey("MasterDataCurrencyId")]
        [InverseProperty("MappingDataVendorCurrencyBank")]
        public virtual MasterDataCurrency MasterDataCurrency { get; set; }
        [ForeignKey("MasterDataUserId")]
        [InverseProperty("MappingDataVendorCurrencyBank")]
        public virtual MasterDataUser MasterDataUser { get; set; }
    }
}
