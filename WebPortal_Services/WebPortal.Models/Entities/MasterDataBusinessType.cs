﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataBusinessType
    {
        public MasterDataBusinessType()
        {
            MasterDataVendor = new HashSet<MasterDataVendor>();
        }

        public int Id { get; set; }
        [StringLength(5)]
        public string Code { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string Desc { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifiyBy { get; set; }
        public bool? IsDelete { get; set; }

        [InverseProperty("MasterDataBusinessType")]
        public virtual ICollection<MasterDataVendor> MasterDataVendor { get; set; }
    }
}
