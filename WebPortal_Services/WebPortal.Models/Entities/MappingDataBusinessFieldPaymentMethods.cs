﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MappingDataBusinessFieldPaymentMethods
    {
        public int Id { get; set; }
        public int MasterDataBusinessFieldId { get; set; }
        public int MasterDataPaymentMethodId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifiyBy { get; set; }
        public bool? IsDelete { get; set; }

        [ForeignKey("MasterDataBusinessFieldId")]
        [InverseProperty("MappingDataBusinessFieldPaymentMethods")]
        public virtual MasterDataBusinessFields MasterDataBusinessField { get; set; }
        [ForeignKey("MasterDataPaymentMethodId")]
        [InverseProperty("MappingDataBusinessFieldPaymentMethods")]
        public virtual MasterDataPaymentMethod MasterDataPaymentMethod { get; set; }
    }
}
