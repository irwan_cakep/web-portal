﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataBank
    {
        public MasterDataBank()
        {
            MappingDataVendorCurrencyBank = new HashSet<MappingDataVendorCurrencyBank>();
        }

        public int Id { get; set; }
        [StringLength(10)]
        public string Code { get; set; }
        [Required]
        [StringLength(20)]
        public string Name { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifiyBy { get; set; }
        public bool? IsDelete { get; set; }

        [InverseProperty("MasterDataBank")]
        public virtual ICollection<MappingDataVendorCurrencyBank> MappingDataVendorCurrencyBank { get; set; }
    }
}
