﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataResetPassword
    {
        public int Id { get; set; }
        [Required]
        [StringLength(200)]
        public string Credential { get; set; }
        [Required]
        [StringLength(32)]
        public string Token { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ExpiredDate { get; set; }
        public bool IsUsed { get; set; }
    }
}
