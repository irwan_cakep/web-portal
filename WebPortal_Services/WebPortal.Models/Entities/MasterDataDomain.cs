﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataDomain
    {
        [StringLength(50)]
        public string DomainName { get; set; }
        [StringLength(100)]
        public string DomainLink { get; set; }
    }
}
