﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MappingDataVendorLegal
    {
        public int Id { get; set; }
        public int MasterDataVendorId { get; set; }
        [Column("NIB")]
        [StringLength(20)]
        public string Nib { get; set; }
        [Column("NIBAttachment")]
        [StringLength(256)]
        public string Nibattachment { get; set; }
        [StringLength(20)]
        public string BusinessPermit { get; set; }
        [StringLength(256)]
        public string BusinessPermitAttachment { get; set; }
        [Column("SIUP")]
        [StringLength(20)]
        public string Siup { get; set; }
        [Column("SIUPExpiredDate", TypeName = "date")]
        public DateTime? SiupexpiredDate { get; set; }
        [Column("SIUPAttachment")]
        [StringLength(256)]
        public string Siupattachment { get; set; }
        [Column("TDP")]
        [StringLength(20)]
        public string Tdp { get; set; }
        [Column("TDPExpiredDate", TypeName = "date")]
        public DateTime? TdpexpiredDate { get; set; }
        [Column("TDPAttachment")]
        [StringLength(256)]
        public string Tdpattachment { get; set; }
        [StringLength(20)]
        public string MemorandumOfAssociation { get; set; }
        [StringLength(256)]
        public string MemorandumOfAssociationAttachment { get; set; }
        [StringLength(20)]
        public string DecissionLetterMenkumham { get; set; }
        [StringLength(256)]
        public string DecissionLetterMenkumhamAttachment { get; set; }
        [StringLength(20)]
        public string MemorandumOfAssociationChanging { get; set; }
        [StringLength(256)]
        public string MemorandumOfAssociationChangingAttachment { get; set; }
        [StringLength(20)]
        public string DecissionLetterMenkumhamChanging { get; set; }
        [StringLength(256)]
        public string DecissionLetterMenkumhamChangingAttachment { get; set; }
        [StringLength(256)]
        public string ManagementStructure { get; set; }
        [StringLength(256)]
        public string Signing { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifiyBy { get; set; }
        [StringLength(10)]
        public string IsDelete { get; set; }

        [ForeignKey("MasterDataVendorId")]
        [InverseProperty("MappingDataVendorLegal")]
        public virtual MasterDataVendor MasterDataVendor { get; set; }
    }
}
