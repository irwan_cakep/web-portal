﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataPaymentMethod
    {
        public MasterDataPaymentMethod()
        {
            MappingDataBusinessFieldPaymentMethods = new HashSet<MappingDataBusinessFieldPaymentMethods>();
        }

        public int Id { get; set; }
        [StringLength(10)]
        public string Code { get; set; }
        [Required]
        [StringLength(20)]
        public string Name { get; set; }
        public int PaymentDuration { get; set; }
        [Column("DP", TypeName = "decimal(18, 0)")]
        public decimal? Dp { get; set; }
        [Column(TypeName = "decimal(18, 0)")]
        public decimal? Progressive { get; set; }
        [Column(TypeName = "decimal(18, 0)")]
        public decimal? FullPayment { get; set; }
        [StringLength(10)]
        public string Retention { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifiyBy { get; set; }
        [StringLength(10)]
        public string IsDelete { get; set; }

        [InverseProperty("MasterDataPaymentMethod")]
        public virtual ICollection<MappingDataBusinessFieldPaymentMethods> MappingDataBusinessFieldPaymentMethods { get; set; }
    }
}
