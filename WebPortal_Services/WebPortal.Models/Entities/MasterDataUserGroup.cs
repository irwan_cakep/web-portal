﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataUserGroup
    {
        public MasterDataUserGroup()
        {
            MasterDataUser = new HashSet<MasterDataUser>();
        }

        public int UserLevel { get; set; }
        [Required]
        [StringLength(50)]
        public string Description { get; set; }

        [InverseProperty("UserLevelNavigation")]
        public virtual ICollection<MasterDataUser> MasterDataUser { get; set; }
    }
}
