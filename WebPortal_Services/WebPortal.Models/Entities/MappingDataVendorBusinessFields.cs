﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MappingDataVendorBusinessFields
    {
        public int Id { get; set; }
        public int MasterDataVendorId { get; set; }
        public int MasterDataBusinessFieldId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifyBy { get; set; }

        [ForeignKey("MasterDataBusinessFieldId")]
        [InverseProperty("MappingDataVendorBusinessFields")]
        public virtual MasterDataBusinessFields MasterDataBusinessField { get; set; }
    }
}
