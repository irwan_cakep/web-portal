﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataUser
    {
        public MasterDataUser()
        {
            MappingDataVendorCurrencyBank = new HashSet<MappingDataVendorCurrencyBank>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string UserId { get; set; }
        [Required]
        [StringLength(50)]
        public string UserName { get; set; }
        [StringLength(50)]
        public string Password { get; set; }
        [StringLength(50)]
        public string ForgetPassword { get; set; }
        [StringLength(50)]
        public string Region { get; set; }
        [Required]
        [Column("email")]
        [StringLength(1000)]
        public string Email { get; set; }
        [StringLength(1000)]
        public string SecondaryEmail { get; set; }
        [StringLength(50)]
        public string Jabatan { get; set; }
        [StringLength(50)]
        public string Divisi { get; set; }
        [StringLength(20)]
        public string ExtensionNumber { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LastLogin { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LastLogout { get; set; }
        [StringLength(1000)]
        public string Session { get; set; }
        public int UserLevel { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "date")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifyBy { get; set; }
        [Column(TypeName = "date")]
        public DateTime? ModifyDate { get; set; }
        [StringLength(50)]
        public string Image { get; set; }
        public bool Isactive { get; set; }
        [Column("IP")]
        [StringLength(50)]
        public string Ip { get; set; }
        public bool IsTemporary { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartAccess { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndAccess { get; set; }
        [Column("IsUserAD")]
        public bool IsUserAd { get; set; }
        [StringLength(50)]
        public string Domain { get; set; }
        [Column("LastAccessFAQ", TypeName = "datetime")]
        public DateTime? LastAccessFaq { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LastAccessTutorial { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LastAccessDocument { get; set; }
        [StringLength(32)]
        public string ActivateLink { get; set; }

        [ForeignKey("UserLevel")]
        [InverseProperty("MasterDataUser")]
        public virtual MasterDataUserGroup UserLevelNavigation { get; set; }
        [InverseProperty("MasterDataUser")]
        public virtual ICollection<MappingDataVendorCurrencyBank> MappingDataVendorCurrencyBank { get; set; }
    }
}
