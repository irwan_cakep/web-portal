﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataRegion
    {
        public MasterDataRegion()
        {
            MasterDataCity = new HashSet<MasterDataCity>();
            MasterDataVendor = new HashSet<MasterDataVendor>();
        }

        public int Id { get; set; }
        [StringLength(10)]
        public string Code { get; set; }
        [Required]
        [StringLength(20)]
        public string Name { get; set; }
        [StringLength(5)]
        public string PhoneCode { get; set; }
        public int MasterDataCountryId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifiyBy { get; set; }
        public bool? IsDelete { get; set; }

        [ForeignKey("MasterDataCountryId")]
        [InverseProperty("MasterDataRegion")]
        public virtual MasterDataCountry MasterDataCountry { get; set; }
        [InverseProperty("MasterDataRegion")]
        public virtual ICollection<MasterDataCity> MasterDataCity { get; set; }
        [InverseProperty("MasterDataRegion")]
        public virtual ICollection<MasterDataVendor> MasterDataVendor { get; set; }
    }
}
