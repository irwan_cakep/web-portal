﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataVendor
    {
        public MasterDataVendor()
        {
            MappingDataVendorLegal = new HashSet<MappingDataVendorLegal>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        public int MasterDataUserId { get; set; }
        public int MasterDataBusinessTypeId { get; set; }
        public int MasterDataCountryId { get; set; }
        public int MasterDataRegionId { get; set; }
        public int MasterDataCityId { get; set; }
        [Required]
        [StringLength(100)]
        public string Address { get; set; }
        [Required]
        [StringLength(10)]
        public string PostalCode { get; set; }
        [Required]
        [StringLength(50)]
        public string CompanyEmail { get; set; }
        [Required]
        [Column("PIC")]
        [StringLength(20)]
        public string Pic { get; set; }
        [Required]
        [Column("PICEmail")]
        [StringLength(50)]
        public string Picemail { get; set; }
        [Required]
        [StringLength(100)]
        public string IdCardNumber { get; set; }
        public bool? IntegrityPact { get; set; }
        public bool? MasterAgreement { get; set; }
        public string Logo { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(20)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [StringLength(20)]
        public string ModifiyBy { get; set; }
        [StringLength(10)]
        public string IsDelete { get; set; }
        [StringLength(100)]
        public string MailingAddress { get; set; }
        [StringLength(10)]
        public string CompanyPhone { get; set; }
        [Column("PICPhone")]
        [StringLength(10)]
        public string Picphone { get; set; }

        [ForeignKey("MasterDataBusinessTypeId")]
        [InverseProperty("MasterDataVendor")]
        public virtual MasterDataBusinessType MasterDataBusinessType { get; set; }
        [ForeignKey("MasterDataCityId")]
        [InverseProperty("MasterDataVendor")]
        public virtual MasterDataCity MasterDataCity { get; set; }
        [ForeignKey("MasterDataCountryId")]
        [InverseProperty("MasterDataVendor")]
        public virtual MasterDataCountry MasterDataCountry { get; set; }
        [ForeignKey("MasterDataRegionId")]
        [InverseProperty("MasterDataVendor")]
        public virtual MasterDataRegion MasterDataRegion { get; set; }
        [InverseProperty("MasterDataVendor")]
        public virtual ICollection<MappingDataVendorLegal> MappingDataVendorLegal { get; set; }
    }
}
