﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPortal.Model;
using WebPortal.Models;
using PagedList;
using WebPortal.GeneralClass;

namespace WebPortal.Controllers.DataSetting
{
    [NoDirectAccess]
    [SessionTimeout]
    public class UserGroupController : Controller
    {
        //
        // GET: /UserGroup/

        oGeneral ogeneral = new oGeneral();
        FrInternallEntities db = new FrInternallEntities();

        public ActionResult Index(int? Page)
        {
            int startIndex = 0;
            int loadPage = 0;

            if (Page != null)
            {
                loadPage = Page.HasValue ? Page.Value - 1 : 0;
                startIndex = 20 * loadPage;
            }

            List<T_MsUserGroup> listUserGroup = db.T_MsUserGroup.OrderBy(o => o.UserLevel).Skip(startIndex).Take(20).ToList();

            int totalUserGroup = db.T_MsUserGroup.Count();
            int totalPages = (totalUserGroup / 20) + (totalUserGroup % 20 > 0 ? 1 : 0);

            PagingAndGroup mod = new PagingAndGroup();
            mod.listUserGroup = listUserGroup;
            mod.currentPage = loadPage + 1;
            mod.totalItem = totalUserGroup;
            mod.totalPages = totalPages;

            mod = ogeneral.getPaging(mod);

            return View(mod);
        }

        [HttpGet]
        public ActionResult AddUserGroup()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddUserGroup(T_MsUserGroup userGroup)
        {
            T_MsUserGroup newUserGroup = new T_MsUserGroup();

            newUserGroup.UserLevel = userGroup.UserLevel;
            newUserGroup.Description = userGroup.Description;

            db.T_MsUserGroup.Add(newUserGroup);
            db.SaveChanges();

            return RedirectToAction("Index", "UserGroup");
        }

        [HttpGet]
        public ActionResult EditUserGroup(int userLevel)
        {
            T_MsUserGroup userGroup = db.T_MsUserGroup.Where(w => w.UserLevel == userLevel).FirstOrDefault();

            return View(userGroup);
        }

        [HttpPost]
        public ActionResult EditUserGroup(T_MsUserGroup userGroup)
        {
            var dbUserGroup = db.T_MsUserGroup.Where(a => a.UserLevel.Equals(userGroup.UserLevel)).FirstOrDefault();

            dbUserGroup.UserLevel = userGroup.UserLevel;
            dbUserGroup.Description = userGroup.Description;

            db.SaveChanges();

            TempData["Message"] = "User Group Updated";
            return RedirectToAction("Index", "UserGroup");
        }


        public ActionResult DeleteUserGroup(int userLevel)
        {
            var userGroup = db.T_MsUserGroup.Where(a => a.UserLevel.Equals(userLevel)).FirstOrDefault();
            int countUser = db.T_MsUser.Where(a => a.UserLevel == userLevel).Count();

            if (countUser > 0)
            {
                TempData["Message"] = "User Group is being used";
            }
            else
            {
                db.T_MsUserGroup.Remove(userGroup);
                TempData["Message"] = "User Group Deleted";

                string queryDel = "delete T_MsUserGroup_Menu where UserLevel = " + userLevel.ToString() + " delete T_MsUserGroup_Site where UserLevel = " + userLevel.ToString();
                ogeneral.execQuery(queryDel);
            }

            db.SaveChanges();

            return RedirectToAction("Index", "UserGroup");
        }

        [HttpGet]
        public ActionResult EditPermission(int userLevel)
        {
            List<ListMenuMapping> MenuList = (from msMenu in db.T_MsMenu.Where(w => w.IsLoginRequired && w.IsActive)
                                              join userMenu in db.T_MsUserGroup_Menu.Where(w => w.UserLevel == userLevel) on msMenu.MenuCode equals userMenu.MenuCode into t
                                              from rt in t.DefaultIfEmpty()
                                              select new ListMenuMapping
                                              {
                                                   MenuCode = msMenu.MenuCode,
                                                   MenuName = msMenu.MenuName,
                                                   Checker = rt.UserLevel == null ? false : true
                                              }).OrderBy(o => o.MenuCode).ToList();

            MenuMapping menu = new MenuMapping();
            menu.UserLevel = userLevel;
            menu.Description = db.T_MsUserGroup.Where(w => w.UserLevel == userLevel).Select(s => s.Description).First();
            menu.listMenu = MenuList;

            return View(menu);
        }

        [HttpPost]
        public JsonResult EditPermission(SubmitMenuMapping listSubmit)
        {
            T_MsUserGroup_Menu userMenu = new T_MsUserGroup_Menu();
            userMenu.UserLevel = listSubmit.UserLevel;

            List<SubmitMenu> MenuParent = new List<SubmitMenu>(), MenuParentDistinct = new List<SubmitMenu>(), MenuChild = new List<SubmitMenu>(), MenuChildDistinct = new List<SubmitMenu>(), SubmitParent = new List<SubmitMenu>();

            string queryDel = "delete T_MsUserGroup_Menu where UserLevel = " + listSubmit.UserLevel.ToString();

            if (ogeneral.execQuery(queryDel) == "Success")
            {
                for (int i = 0; i < listSubmit.CheckedMenu.Length; i++)
                {
                    userMenu.MenuCode = listSubmit.CheckedMenu[i];
                    db.T_MsUserGroup_Menu.Add(userMenu);
                    db.SaveChanges();

                    if (listSubmit.CheckedMenu[i].Length == 2)
                    {
                        MenuParent.Add(new SubmitMenu { MenuCode = listSubmit.CheckedMenu[i] });
                    }
                    else
                    {
                        MenuChild.Add(new SubmitMenu { MenuCode = listSubmit.CheckedMenu[i].Substring(0, 2) });
                    }
                }

                MenuParentDistinct = MenuParent.GroupBy(g => g.MenuCode).Select(s => s.First()).ToList();
                MenuChildDistinct = MenuChild.GroupBy(g => g.MenuCode).Select(s => s.First()).ToList();
                SubmitParent = MenuChildDistinct.Where(w => !MenuParentDistinct.Any(a => a.MenuCode == w.MenuCode)).ToList();

                for (int i = 0; i < SubmitParent.Count; i++)
                {
                    userMenu.MenuCode = SubmitParent[i].MenuCode;
                    db.T_MsUserGroup_Menu.Add(userMenu);
                    db.SaveChanges();
                }
            }
            else
            {
                return Json("Failed to Process the Request, Please Try Again", JsonRequestBehavior.AllowGet);
            }

            TempData["Message"] = "Permission Updated";
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EditSitePermission(int userLevel)
        {
            List<ListSiteMapping> SiteList = (from msSite in db.T_MsSite.Where(w => w.IsShow)
                                              join userSite in db.T_MsUserGroup_Site.Where(w => w.UserLevel == userLevel) on msSite.SiteCode equals userSite.SiteCode into t
                                              from rt in t.DefaultIfEmpty()
                                              select new ListSiteMapping
                                              {
                                                  SiteCode = msSite.SiteCode,
                                                  SiteName = msSite.Description,
                                                  Checker = rt.UserLevel == null ? false : true
                                              }).OrderBy(o => o.SiteName).ToList();

            SiteMapping site = new SiteMapping();
            site.UserLevel = userLevel;
            site.Description = db.T_MsUserGroup.Where(w => w.UserLevel == userLevel).Select(s => s.Description).First();
            site.listSite = SiteList;

            return View(site);
        }

        [HttpPost]
        public JsonResult EditSitePermission(SubmitSiteMapping listSubmit)
        {
            T_MsUserGroup_Site userSite = new T_MsUserGroup_Site();
            userSite.UserLevel = listSubmit.UserLevel;

            List<SubmitSite> SiteParent = new List<SubmitSite>(), SiteParentDistinct = new List<SubmitSite>(), SiteChild = new List<SubmitSite>(), SiteChildDistinct = new List<SubmitSite>(), SubmitParent = new List<SubmitSite>();

            string queryDel = "delete T_MsUserGroup_Site where UserLevel = " + listSubmit.UserLevel.ToString();

            if (ogeneral.execQuery(queryDel) == "Success")
            {
                for (int i = 0; i < listSubmit.CheckedSite.Length; i++)
                {
                    userSite.SiteCode = listSubmit.CheckedSite[i];
                    db.T_MsUserGroup_Site.Add(userSite);
                    db.SaveChanges();
                }

                SiteParentDistinct = SiteParent.GroupBy(g => g.SiteCode).Select(s => s.First()).ToList();
                SiteChildDistinct = SiteChild.GroupBy(g => g.SiteCode).Select(s => s.First()).ToList();
                SubmitParent = SiteChildDistinct.Where(w => !SiteParentDistinct.Any(a => a.SiteCode == w.SiteCode)).ToList();

                for (int i = 0; i < SubmitParent.Count; i++)
                {
                    userSite.SiteCode = SubmitParent[i].SiteCode;
                    db.T_MsUserGroup_Site.Add(userSite);
                    db.SaveChanges();
                }
            }
            else
            {
                return Json("Failed to Process the Request, Please Try Again", JsonRequestBehavior.AllowGet);
            }

            TempData["Message"] = "Permission Updated";
            return Json("Success", JsonRequestBehavior.AllowGet);
        }
    }
}
