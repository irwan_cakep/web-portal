﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPortal.GeneralClass;
using WebPortal.Model;

namespace WebPortal.Controllers.Corporate
{
    public class HistoryController : Controller
    {
        //
        // GET: /History/
        FrInternallEntities db = new FrInternallEntities();

        public ActionResult Index()
        {
            List<T_MsMenuContent> listMenuContent = db.T_MsMenuContent.Where(model => model.Menu == "History").ToList();

            return View(listMenuContent);
        }

        [SessionTimeout]
        [HttpGet]
        public ActionResult EditHistory()
        {
            List<T_MsMenuContent> listMenuContent = db.T_MsMenuContent.Where(model => model.Menu == "History").ToList();
            listMenuContent.First().MenuContent = listMenuContent.First().MenuContent.Replace(";", System.Environment.NewLine);

            return View(listMenuContent);
        }

        [SessionTimeout]
        [HttpPost]
        public ActionResult EditHistory(T_MsMenuContent content)
        {
            T_MsMenuContent menuContent = (from n in db.T_MsMenuContent
                                           where n.Menu == content.Menu
                                           select n).First();
            menuContent.MenuContent = content.MenuContent.Replace(System.Environment.NewLine, ";");
            db.SaveChanges();

            return RedirectToAction("Index");
        }

    }
}
