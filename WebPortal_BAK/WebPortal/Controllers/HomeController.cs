﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using WebPortal.GeneralClass;
using WebPortal.Model;
using WebPortal.Models;
using System.DirectoryServices.AccountManagement;
using System.Net;
using System.DirectoryServices.Protocols;
using System.DirectoryServices;
using System.Text.RegularExpressions;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Configuration;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace WebPortal.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        FrInternallEntities db = new FrInternallEntities();
        oGeneral ogeneral = new oGeneral();

        public HomeController()
        {
        }

        public ActionResult CheckRememberMe()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [NoDirectAccess]
        [HttpPost]
        public JsonResult Register(string Email, string Password, string ConfirmPassword, string UserType)
        {
            AjaxReturn result = new AjaxReturn();

            T_MsUser dbUserByEmail = db.T_MsUser.Where(m => m.email == Email).FirstOrDefault();

            if (dbUserByEmail != null)
            {
                result.Message = "Email Has Been Used, Please Use Another Email";
            }
            else
            {
                String linkParam = Guid.NewGuid().ToString().Replace("-", "");

                T_MsUser newUser = new T_MsUser();
                newUser.UserId = Email;
                newUser.UserName = Email;
                newUser.Password = ogeneral.Encrypt(Password);
                newUser.email = Email;
                newUser.UserLevel = UserType == "V" ? 2 : UserType == "C" ? 3 : 4;
                newUser.Isactive = false;
                newUser.CreatedBy = "Register";
                newUser.CreatedDate = DateTime.Now;
                newUser.ActivateLink = linkParam;

                db.T_MsUser.Add(newUser);
                db.SaveChanges();

                //T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == Email).First();
                string IP = Request.ServerVariables["REMOTE_ADDR"];

                Session["LogedUserID"] = newUser.UserId;
                Session["LogedUsername"] = newUser.UserName;
                Session["LogedEmail"] = newUser.email;
                Session["UserLevel"] = newUser.UserLevel;

                newUser.LastLogin = DateTime.Now;
                newUser.IP = IP;
                //dbUser.Isactive = true;
                //MsUser.Session = createSession;

                T_HistoryLogin HistoryLogin = new T_HistoryLogin();
                HistoryLogin.UserId = newUser.UserId;
                HistoryLogin.IP = IP;
                HistoryLogin.DateLogin = DateTime.Now;
                db.T_HistoryLogin.Add(HistoryLogin);
                db.SaveChanges();

                FormsAuthentication.SetAuthCookie(newUser.UserId, true);

                FormatEmailModel formatEmailModel = new FormatEmailModel();

                string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
                string activationUrl = baseUrl + "Home/Activation/?linkParam=" + linkParam;

                formatEmailModel.Subject = "Email Activation";
                formatEmailModel.EmailRecipient = Email;
                formatEmailModel.Link = activationUrl;
                formatEmailModel.LinkMessage = "Confirm Email Address";
                formatEmailModel.Message1 = "Please confirm your email address by clicking the link below.";
                formatEmailModel.Message2 = "We may need to send you critical information about our service and it is important that we have an accurate email address.";

                Mailer.SendMail(formatEmailModel);

                result.Status = true;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [NoDirectAccess]
        public ViewResult RegisterConfirmation(string email)
        {
            ViewBag.Email = email;

            return View();
        }

        public ActionResult LoginPage()
        {
            if (Session["SessionStatus"] != null)
            {
                TempData["SessionStatus"] = Session["SessionStatus"];
                Session["SessionStatus"] = null;
            }

            return View();
        }

        [NoDirectAccess]
        public JsonResult Login(string UserId, string Password)
        {
            AjaxReturn result = new AjaxReturn();
            bool isValid = false;
            string IP = Request.ServerVariables["REMOTE_ADDR"];

            if (UserId == "" || Password == "")
            {
                result.Message = "Please Insert Username and Password";
            }
            else
            {
                T_MsUser dbUser = db.T_MsUser.Where(m => m.UserId == UserId || m.email == UserId).FirstOrDefault();


                if (dbUser != null)
                {
                    string tempUserId = dbUser.UserId;

                    if (dbUser.IsUserAD)
                    {
                        string domain = dbUser.Domain;

                        try
                        {
                            using (var domainContext = new PrincipalContext(ContextType.Domain, domain, tempUserId, Password))
                            {
                                using (var foundUser = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, tempUserId))
                                {
                                    if (foundUser != null)
                                    {
                                        dbUser.UserName = foundUser.GivenName + " " + foundUser.Surname;
                                        dbUser.email = foundUser.EmailAddress;
                                        dbUser.Region = GetProperty(foundUser, "physicalDeliveryOfficeName");
                                        dbUser.Jabatan = GetProperty(foundUser, "title");
                                        dbUser.Divisi = GetProperty(foundUser, "department");
                                        dbUser.ExtensionNumber = foundUser.VoiceTelephoneNumber;
                                        db.SaveChanges();

                                        dbUser = db.T_MsUser.Where(m => m.UserId == tempUserId).FirstOrDefault();
                                        isValid = true;
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            result.Message = e.ToString();
                        }
                    }
                    else if (dbUser.Password == ogeneral.Encrypt(Password)) 
                    {
                        if (dbUser.IsTemporary)
                        {
                            if (DateTime.Now >= dbUser.StartAccess && DateTime.Now <= dbUser.EndAccess)
                                isValid = true;
                            if (DateTime.Now > dbUser.EndAccess)
                                result.Message = "Account Expired";
                            else
                                result.Message = "Account will be Activate on " + dbUser.StartAccess.Value.ToString("dd MMM yyyy") + " to " + dbUser.EndAccess.Value.ToString("dd MMM yyyy");
                        }
                        else
                        {
                            isValid = true;
                        }
                    }
                    else
                    {
                        result.Message = "Password Incorrect";
                    }
                }
                else
                {
                    List<T_MsDomain> listDomain = db.T_MsDomain.ToList();

                    foreach (var dbDomain in listDomain)
                    {
                        string domain = dbDomain.DomainLink;

                        //var domainContext = new PrincipalContext(ContextType.Domain, domain, UserId, Password);
                        var domainContext = new PrincipalContext(ContextType.Domain, domain);

                        try
                        {
                            var foundUser = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, UserId);

                            if (foundUser != null)
                            {
                                isValid = true;

                                T_MsUser newUser = new T_MsUser();
                                newUser.UserId = UserId;
                                newUser.UserName = foundUser.GivenName + " " + foundUser.Surname;
                                newUser.email = foundUser.EmailAddress;
                                newUser.UserLevel = domain.Contains("first-resources") ? 11 : domain.Contains("fap") ? 12 : domain.Contains("borneo") ? 13 : 1;
                                newUser.Region = GetProperty(foundUser, "physicalDeliveryOfficeName");
                                newUser.Jabatan = GetProperty(foundUser, "title");
                                newUser.Divisi = GetProperty(foundUser, "department");
                                newUser.ExtensionNumber = foundUser.VoiceTelephoneNumber;
                                newUser.IsUserAD = true;
                                newUser.Isactive = true;
                                newUser.Domain = domain;

                                db.T_MsUser.Add(newUser);
                                db.SaveChanges();

                                dbUser = db.T_MsUser.Where(m => m.UserId == UserId).FirstOrDefault();

                                break;
                            }
                        }
                        catch (Exception e)
                        {
                            result.Message = e.ToString();
                        }
                    }
                }

                if (isValid)
                {
                    Session["LogedUserID"] = dbUser.UserId;
                    Session["LogedUsername"] = dbUser.UserName;
                    Session["LogedEmail"] = dbUser.email;
                    Session["UserLevel"] = dbUser.UserLevel;

                    dbUser.LastLogin = DateTime.Now;
                    dbUser.IP = IP;
                    //dbUser.Isactive = true;
                    //MsUser.Session = createSession;

                    T_HistoryLogin HistoryLogin = new T_HistoryLogin();
                    HistoryLogin.UserId = dbUser.UserId;
                    HistoryLogin.IP = IP;
                    HistoryLogin.DateLogin = DateTime.Now;
                    db.T_HistoryLogin.Add(HistoryLogin);
                    db.SaveChanges();

                    FormsAuthentication.SetAuthCookie(dbUser.UserId, true);

                    result.Status = true;
                    result.Message = "Success";
                    result.AccountStatus = dbUser.Isactive;
                }

            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [NoDirectAccess]
        public JsonResult LoginGoogle(string UserId, string Username, string Email)
        {
            AjaxReturn result = new AjaxReturn();

            T_MsUser dbUser = db.T_MsUser.Where(m => m.UserId == UserId).FirstOrDefault();

            if (dbUser != null)
            {
                string IP = Request.ServerVariables["REMOTE_ADDR"];

                Session["LogedUserID"] = dbUser.UserId;
                Session["LogedUsername"] = dbUser.UserName;
                Session["LogedEmail"] = dbUser.email;
                Session["UserLevel"] = dbUser.UserLevel;

                dbUser.LastLogin = DateTime.Now;
                dbUser.IP = IP;
                //dbUser.Isactive = true;
                //MsUser.Session = createSession;

                T_HistoryLogin HistoryLogin = new T_HistoryLogin();
                HistoryLogin.UserId = dbUser.UserId;
                HistoryLogin.IP = IP;
                HistoryLogin.DateLogin = DateTime.Now;
                db.T_HistoryLogin.Add(HistoryLogin);
                db.SaveChanges();

                FormsAuthentication.SetAuthCookie(dbUser.UserId, true);

                result.Status = true;
                result.AccountStatus = dbUser.Isactive;

                if (dbUser.ActivateLink != null)
                {
                    result.Message = "OK";
                }
                else
                {
                    result.GeneratedString = ogeneral.Encrypt(dbUser.UserId);
                }
            }
            else
            {
                //String linkParam = Guid.NewGuid().ToString().Replace("-", "");

                T_MsUser newUser = new T_MsUser();
                newUser.UserId = UserId;
                newUser.UserName = Username;
                newUser.email = Email;
                newUser.UserLevel = 1;
                newUser.Isactive = false;
                newUser.CreatedBy = "Google";
                newUser.CreatedDate = DateTime.Now;
                //newUser.ActivateLink = linkParam;

                db.T_MsUser.Add(newUser);
                db.SaveChanges();

                result.GeneratedString = ogeneral.Encrypt(UserId);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [NoDirectAccess]
        public ActionResult ChooseTypePage(string regId)
        {
            Session["regId"] = regId;

            return View();
        }

        [NoDirectAccess]
        public JsonResult ChooseType(string regId, string Type)
        {
            AjaxReturn result = new AjaxReturn();

            regId = ogeneral.Decrypt(regId);

            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == regId).First();
            string IP = Request.ServerVariables["REMOTE_ADDR"];

            String linkParam = Guid.NewGuid().ToString().Replace("-", "");

            dbUser.UserLevel = Type == "V" ? 2 : Type == "C" ? 3 : 4;
            dbUser.ActivateLink = linkParam;
            dbUser.LastLogin = DateTime.Now;
            dbUser.IP = IP;
            db.SaveChanges();


            Session["LogedUserID"] = dbUser.UserId;
            Session["LogedUsername"] = dbUser.UserName;
            Session["LogedEmail"] = dbUser.email;
            Session["UserLevel"] = dbUser.UserLevel;

            //dbUser.Isactive = true;
            //MsUser.Session = createSession;

            T_HistoryLogin HistoryLogin = new T_HistoryLogin();
            HistoryLogin.UserId = dbUser.UserId;
            HistoryLogin.IP = IP;
            HistoryLogin.DateLogin = DateTime.Now;
            db.T_HistoryLogin.Add(HistoryLogin);
            db.SaveChanges();

            FormsAuthentication.SetAuthCookie(dbUser.UserId, true);

            result.Message = "Account registered, please check your email to get your account activated";
            result.GeneratedString = linkParam;

            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [NoDirectAccess]
        public ActionResult ResetPage()
        {
            return View();
        }

        [NoDirectAccess]
        public JsonResult ResetPassword(string Credential)
        {
            AjaxReturn result = new AjaxReturn();

            T_MsUser dbUser = db.T_MsUser.Where(m => m.email == Credential).FirstOrDefault();

            if (dbUser == null)
            {
                result.Message = "Email is invalid";
            }
            else
            {
                string query = "update T_ResetPassword set IsUsed = 1 where Credential = '" + Credential + "' AND IsUsed = 0";
                ogeneral.execQuery(query);

                String token = Guid.NewGuid().ToString().Replace("-", "");
                string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

                T_ResetPassword newReset = new T_ResetPassword();
                newReset.Credential = Credential;
                newReset.Token = token;
                newReset.ExpiredDate = DateTime.Now.AddDays(1);
                db.T_ResetPassword.Add(newReset);
                db.SaveChanges();

                FormatEmailModel formatEmailModel = new FormatEmailModel();

                formatEmailModel.Subject = "Reset Password";
                formatEmailModel.EmailRecipient = dbUser.email;
                formatEmailModel.Link = baseUrl + "Home/NewPassword/?Credential=" + token;
                formatEmailModel.LinkMessage = "Reset Password";
                formatEmailModel.Message1 = "We've send to you a link for reset password.";
                formatEmailModel.Message2 = "We may need to send you critical information about our service and it is important that we have an accurate email address.";

                Mailer.SendMail(formatEmailModel);

                result.Status = true;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [NoDirectAccess]
        public ActionResult ResetPageConfirmation(string email)
        {
            ViewBag.Email = email;

            return View();
        }

        public ActionResult NewPassword(string Credential)
        {
            Session["RequestResetToken"] = Credential;

            return View();
        }

        [NoDirectAccess]
        [HttpPost]
        public JsonResult NewPassword(string Token, string NewPassword)
        {
            AjaxReturn result = new AjaxReturn();

            T_ResetPassword dbReset = db.T_ResetPassword.Where(w => w.Token == Token && !w.IsUsed && w.ExpiredDate > DateTime.Now).FirstOrDefault();

            if (dbReset != null)
            {
                T_MsUser dbUser = db.T_MsUser.Where(w => w.email == dbReset.Credential).First();
                dbUser.Password = ogeneral.Encrypt(NewPassword);

                dbReset.IsUsed = true;

                db.SaveChanges();

                result.Status = true;
                result.Message = "Password Reset Success";
            }
            else
            {
                result.Message = "Token expired, please request new token";
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ViewResult ResetPasswordSuccess() 
        {
            return View();
        }

        public ActionResult Activation(string linkParam)
        {
            T_MsUser dbUser = db.T_MsUser.Where(w => w.ActivateLink == linkParam && !w.Isactive).FirstOrDefault();

            if (dbUser != null)
            {
                dbUser.Isactive = true;
                db.T_MsUser.Add(dbUser);
                db.Entry(dbUser).State = EntityState.Modified;
                db.SaveChanges();
            }

            return View();
        }

        public ActionResult Index()
        {

            if (Session["LogedUserId"] == null)
            {
                return RedirectToAction("LoginPage");
            }

            List<T_MsSite> listSite = new List<T_MsSite>();

            if (Session["UserLevel"] != null)
            {
                int userLevel = int.Parse(Session["UserLevel"].ToString());

                listSite = (from userSite in db.T_MsUserGroup_Site.Where(w => w.UserLevel == userLevel)
                            join msSite in db.T_MsSite.Where(w => w.IsShow) on userSite.SiteCode equals msSite.SiteCode
                            select msSite).ToList();
            }

            List<T_News> listlatestNews = db.T_News.OrderByDescending(o => o.ModifyDate == null ? o.CreatedDate : o.ModifyDate).Take(3).ToList();

            PagingAndGroup mod = new PagingAndGroup();
            mod.listSiteMenu = listSite;
            mod.listLatestNews = listlatestNews;

            return View(mod);
        }

        [NoDirectAccess]
        public JsonResult EditSecondaryEmail(string UserId, string SecondaryEmail)
        {
            string result = "";

            Match match = Regex.Match(SecondaryEmail, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);


            if (SecondaryEmail == "")
            {
                result = "Please Insert Secondary Email";
            }
            else if (!match.Success)
            {
                result = "Invalid Email Format";
            }
            else
            {
                T_MsUser dbUser = db.T_MsUser.Where(m => m.UserId == UserId).FirstOrDefault();

                if (dbUser != null)
                {
                    dbUser.SecondaryEmail = SecondaryEmail;
                    dbUser.ModifyBy = UserId;
                    dbUser.ModifyDate = DateTime.Now;
                    db.SaveChanges();

                    result = "Success";
                    TempData["EditSecondaryEmail"] = "Secondary Email Updated";
                }
                else
                {
                    result = "Edit Secondary Email Failed, Please Try Again Later.";
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [NoDirectAccess]
        public JsonResult EditSiteMapping(string UserId, int siteCode, string siteUserId, string sitePassword)
        {
            string result = "";

            if (siteUserId == "" || sitePassword == "")
            {
                result = "Please Insert User ID and Password";
            }
            else
            {
                T_MsUser_Site dbUserSite = db.T_MsUser_Site.Where(m => m.UserId == UserId && m.SiteCode == siteCode).FirstOrDefault();

                if (dbUserSite != null)
                {
                    dbUserSite.SiteUserId = siteUserId;
                    dbUserSite.SitePassword = ogeneral.Encrypt(sitePassword);
                    db.SaveChanges();
                }
                else
                {
                    T_MsUser_Site newUserSite = new T_MsUser_Site();
                    newUserSite.UserId = UserId;
                    newUserSite.SiteCode = siteCode;
                    newUserSite.SiteUserId = siteUserId;
                    newUserSite.SitePassword = ogeneral.Encrypt(sitePassword);
                    db.T_MsUser_Site.Add(newUserSite);
                    db.SaveChanges();
                }

                result = "Success";
                TempData["EditSiteMapping"] = "User Site Updated";
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [NoDirectAccess]
        public JsonResult Logout(string UserId)
        {
            var v = db.T_MsUser.Where(a => a.UserId.Equals(UserId)).FirstOrDefault();
            db.Entry(v).State = EntityState.Modified;
            db.SaveChanges();
            ogeneral.Logout(UserId);
            FormsAuthentication.SignOut();
            ogeneral.clearSession(Session);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public String GetProperty(Principal principal, String property)
        {
            DirectoryEntry directoryEntry = principal.GetUnderlyingObject() as DirectoryEntry;
            if (directoryEntry.Properties.Contains(property))
                return directoryEntry.Properties[property].Value.ToString();
            else
                return String.Empty;
        }

        public void sendEMailThroughOUTLOOK()
        {
            try
            {
                // Create the Outlook application.
                Outlook.Application oApp = new Outlook.Application();
                // Create a new mail item.
                Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                // Set HTMLBody. 
                //add the body of the email
                oMsg.HTMLBody = "Hello, Jawed your message body will go here!!";

                ////Add an attachment.
                //String sDisplayName = "MyAttachment";
                //int iPosition = (int)oMsg.Body.Length + 1;
                //int iAttachType = (int)Outlook.OlAttachmentType.olByValue;
                ////now attached the file
                //Outlook.Attachment oAttach = oMsg.Attachments.Add
                //                             (@"C:\\fileName.jpg", iAttachType, iPosition, sDisplayName);

                //Subject line
                oMsg.Subject = "Your Subject will go here.";
                // Add a recipient.
                Outlook.Recipients oRecips = (Outlook.Recipients)oMsg.Recipients;
                // Change the recipient in the next line if necessary.
                Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add("suwandri.lie@first-resources.int");
                oRecip.Resolve();
                // Send.
                oMsg.Send();
                // Clean up.
                oRecip = null;
                oRecips = null;
                oMsg = null;
                oApp = null;
            }//end of try block
            catch (Exception ex)
            {
            }//end of catch
        }//end of Email Method

        public ActionResult LoginBanner()
        {
            return View();
        }
    }
}
