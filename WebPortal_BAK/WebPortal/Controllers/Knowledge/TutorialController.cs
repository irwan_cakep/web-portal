﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using WebPortal.GeneralClass;
using WebPortal.Model;
using WebPortal.Models;

namespace WebPortal.Controllers.Knowledge
{
    [NoDirectAccess]
    [SessionTimeout]
    public class TutorialController : Controller
    {
        //
        // GET: /Tutorial/
        oGeneral ogeneral = new oGeneral();
        FrInternallEntities db = new FrInternallEntities();

        public ActionResult Index(string stringSearch, int? Page, string Project)
        {
            int startIndex = 0;
            int loadPage = 0;

            if (Page != null)
            {
                loadPage = Page.HasValue ? Page.Value - 1 : 0;
                startIndex = 5 * loadPage;
            }

            if (Project != null)
            {
                Session["TutorialProject"] = Project;
            }

            string loadProject = Session["TutorialProject"] != null ? Session["TutorialProject"].ToString() : "";

            if (loadProject != "")
                Project = loadProject;
            else
                Project = null;

            List<TutorialWithParent> listTutorial = (from tutorial in db.T_Tutorial.Where(w => w.VideoName != null && (w.TutorialParent.Substring(0, 1) == loadProject || Project == null))
                                                     join parent in db.T_Tutorial on tutorial.TutorialParent equals parent.TutorialCode into t
                                                     from rt in t.DefaultIfEmpty()
                                                     join parentLv2 in db.T_Tutorial on rt.TutorialParent equals parentLv2.TutorialCode into t2
                                                     from rt2 in t2.DefaultIfEmpty()
                                                     join parentLv3 in db.T_Tutorial on rt2.TutorialParent equals parentLv3.TutorialCode into t3
                                                     from rt3 in t3.DefaultIfEmpty()
                                                     join parentLv4 in db.T_Tutorial on rt3.TutorialParent equals parentLv4.TutorialCode into t4
                                                     from rt4 in t4.DefaultIfEmpty()
                                                     join parentLv5 in db.T_Tutorial on rt4.TutorialParent equals parentLv5.TutorialCode into t5
                                                     from rt5 in t5.DefaultIfEmpty()
                                                     join parentLv6 in db.T_Tutorial on rt5.TutorialParent equals parentLv6.TutorialCode into t6
                                                     from rt6 in t6.DefaultIfEmpty()
                                                     select new TutorialWithParent
                                                     {
                                                         Id = tutorial.Id.Value,
                                                         TutorialCode = tutorial.TutorialCode,
                                                         TutorialTitle = tutorial.TutorialTitle,
                                                         TutorialDescription = tutorial.TutorialDescription,
                                                         VideoName = tutorial.VideoName,
                                                         VideoPath = tutorial.VideoPath,
                                                         TutorialParent = tutorial.TutorialParent,
                                                         TutorialParentTitle = rt6.TutorialTitle == null ? (rt5.TutorialTitle == null ? (rt4.TutorialTitle == null ? (rt3.TutorialTitle == null ? (rt2.TutorialTitle == null ? rt.TutorialTitle : rt2.TutorialTitle + " > " + rt.TutorialTitle) : rt3.TutorialTitle + " > " + rt2.TutorialTitle + " > " + rt.TutorialTitle) : rt4.TutorialTitle + " > " + rt3.TutorialTitle + " > " + rt2.TutorialTitle + " > " + rt.TutorialTitle) : rt5.TutorialTitle + " > " + rt4.TutorialTitle + " > " + rt3.TutorialTitle + " > " + rt2.TutorialTitle + " > " + rt.TutorialTitle) : rt6.TutorialTitle + " > " + rt5.TutorialTitle + " > " + rt4.TutorialTitle + " > " + rt3.TutorialTitle + " > " + rt2.TutorialTitle + " > " + rt.TutorialTitle,
                                                         CreatedBy = tutorial.CreatedBy,
                                                         CreatedDate = tutorial.CreatedDate,
                                                         ModifyBy = tutorial.ModifyBy,
                                                         ModifyDate = tutorial.ModifyDate
                                                     }).OrderByDescending(o => o.ModifyDate == null ? o.CreatedDate : o.ModifyDate).ToList();

            if (stringSearch != null)
            {
                Session["tutorialSearch"] = stringSearch;
            }

            string loadStringSearch = Session["tutorialSearch"] != null ? Session["tutorialSearch"].ToString() : "";

            if (loadStringSearch != "")
                stringSearch = loadStringSearch;
            else
                stringSearch = null;

            if (stringSearch != null)
                listTutorial = listTutorial.Where(w => w.TutorialTitle.ToLower().Contains(stringSearch.ToLower()) || w.TutorialDescription.ToLower().Contains(stringSearch.ToLower()) || w.TutorialParentTitle.ToLower().Contains(stringSearch.ToLower()) || w.CreatedBy.ToLower().Contains(stringSearch.ToLower()) || (w.ModifyDate == null ? w.CreatedDate : w.ModifyDate.GetValueOrDefault()).ToString("dd MMMM yyyy").ToLower().Contains(stringSearch.ToLower())).ToList();

            int totalTutorial = db.T_Tutorial.Where(w => w.VideoName != null).Count();
            int totalPages = (listTutorial.Count / 5) + (listTutorial.Count % 5 > 0 ? 1 : 0);

            listTutorial = listTutorial.Skip(startIndex).Take(5).ToList();

            List<T_Tutorial> latestTutorial = db.T_Tutorial.OrderByDescending(o => o.ModifyDate == null ? o.CreatedDate : o.ModifyDate).Take(3).ToList();

            List<Grouping> listProject = (from tutorial in db.T_Tutorial.Where(w => w.VideoName != null)
                                          group tutorial by tutorial.TutorialParent.Substring(0, 1) into grp
                                          join parent in db.T_Tutorial on grp.FirstOrDefault().TutorialParent.Substring(0, 1) equals parent.TutorialCode
                                          select new Grouping
                                          {
                                              ProjectCode = parent.TutorialCode,
                                              TutorialProject = parent.TutorialTitle,
                                              TotalTutorial = grp.Count(c => c.TutorialCode != null)
                                          }).OrderBy(o => o.TutorialProject).ToList();

            string userId = Session["LogedUserId"].ToString();
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();
            dbUser.LastAccessTutorial = DateTime.Now;
            db.SaveChanges();

            PagingAndGroup mod = new PagingAndGroup();
            mod.listTutorial = listTutorial;
            mod.listLatestTutorial = latestTutorial;
            mod.listTutorialProject = listProject;
            mod.stringSearch = stringSearch;
            mod.currentPage = loadPage + 1;
            mod.totalItem = totalTutorial;
            mod.totalPages = totalPages;

            mod = ogeneral.getPaging(mod);

            return View(mod);
        }

        public ActionResult ViewTutorial(string tutorialCode)
        {
            List<TutorialWithParent> listTutorial = (from tutorial in db.T_Tutorial.Where(w => w.TutorialCode == tutorialCode)
                                                     join parent in db.T_Tutorial on tutorial.TutorialParent equals parent.TutorialCode into t
                                                     from rt in t.DefaultIfEmpty()
                                                     join parentLv2 in db.T_Tutorial on rt.TutorialParent equals parentLv2.TutorialCode into t2
                                                     from rt2 in t2.DefaultIfEmpty()
                                                     join parentLv3 in db.T_Tutorial on rt2.TutorialParent equals parentLv3.TutorialCode into t3
                                                     from rt3 in t3.DefaultIfEmpty()
                                                     join parentLv4 in db.T_Tutorial on rt3.TutorialParent equals parentLv4.TutorialCode into t4
                                                     from rt4 in t4.DefaultIfEmpty()
                                                     join parentLv5 in db.T_Tutorial on rt4.TutorialParent equals parentLv5.TutorialCode into t5
                                                     from rt5 in t5.DefaultIfEmpty()
                                                     join parentLv6 in db.T_Tutorial on rt5.TutorialParent equals parentLv6.TutorialCode into t6
                                                     from rt6 in t6.DefaultIfEmpty()
                                                     select new TutorialWithParent
                                                     {
                                                         Id = tutorial.Id.Value,
                                                         TutorialCode = tutorial.TutorialCode,
                                                         TutorialTitle = tutorial.TutorialTitle,
                                                         TutorialDescription = tutorial.TutorialDescription,
                                                         VideoName = tutorial.VideoName,
                                                         VideoPath = tutorial.VideoPath,
                                                         TutorialParent = tutorial.TutorialParent,
                                                         TutorialParentTitle = rt6.TutorialTitle == null ? (rt5.TutorialTitle == null ? (rt4.TutorialTitle == null ? (rt3.TutorialTitle == null ? (rt2.TutorialTitle == null ? rt.TutorialTitle : rt2.TutorialTitle + " > " + rt.TutorialTitle) : rt3.TutorialTitle + " > " + rt2.TutorialTitle + " > " + rt.TutorialTitle) : rt4.TutorialTitle + " > " + rt3.TutorialTitle + " > " + rt2.TutorialTitle + " > " + rt.TutorialTitle) : rt5.TutorialTitle + " > " + rt4.TutorialTitle + " > " + rt3.TutorialTitle + " > " + rt2.TutorialTitle + " > " + rt.TutorialTitle) : rt6.TutorialTitle + " > " + rt5.TutorialTitle + " > " + rt4.TutorialTitle + " > " + rt3.TutorialTitle + " > " + rt2.TutorialTitle + " > " + rt.TutorialTitle,
                                                         CreatedBy = tutorial.CreatedBy,
                                                         CreatedDate = tutorial.CreatedDate,
                                                         ModifyBy = tutorial.ModifyBy,
                                                         ModifyDate = tutorial.ModifyDate
                                                     }).ToList();


            List<T_Tutorial> latestTutorial = db.T_Tutorial.OrderByDescending(o => o.ModifyDate == null ? o.CreatedDate : o.ModifyDate).Take(3).ToList();

            List<Grouping> listProject = (from tutorial in db.T_Tutorial.Where(w => w.VideoName != null)
                                          group tutorial by tutorial.TutorialParent.Substring(0, 1) into grp
                                          join parent in db.T_Tutorial on grp.FirstOrDefault().TutorialParent.Substring(0, 1) equals parent.TutorialCode
                                          select new Grouping
                                          {
                                              ProjectCode = parent.TutorialCode,
                                              TutorialProject = parent.TutorialTitle,
                                              TotalTutorial = grp.Count(c => c.TutorialCode != null)
                                          }).OrderBy(o => o.TutorialProject).ToList();

            PagingAndGroup mod = new PagingAndGroup();
            mod.listTutorial = listTutorial;
            mod.listLatestTutorial = latestTutorial;
            mod.listTutorialProject = listProject;
            mod.totalItem = db.T_Tutorial.Where(w => w.VideoName != null).ToList().Count;

            return View(mod);
        }

        public ActionResult ListTutorial(string stringSearch, int? Page)
        {
            int startIndex = 0;
            int loadPage = 0;

            if (Page != null)
            {
                loadPage = Page.HasValue ? Page.Value - 1 : 0;
                startIndex = 20 * loadPage;
            }

            List<TutorialWithParent> listTutorialGroup = (from tutorial in db.T_Tutorial.Where(w => w.TutorialParent == null)
                                                          select new TutorialWithParent
                                                          {
                                                              Id = tutorial.Id.Value,
                                                              TutorialCode = tutorial.TutorialCode,
                                                              TutorialTitle = tutorial.TutorialTitle,
                                                              TutorialDescription = tutorial.TutorialDescription,
                                                              VideoName = tutorial.VideoName,
                                                              VideoPath = tutorial.VideoPath,
                                                              TutorialParent = null,
                                                              TutorialParentTitle = null,
                                                              CreatedBy = tutorial.CreatedBy,
                                                              CreatedDate = tutorial.CreatedDate,
                                                              ModifyBy = tutorial.ModifyBy,
                                                              ModifyDate = tutorial.ModifyDate,
                                                              IsChanged = false
                                                          }).ToList();

            List<TutorialWithParent> listTutorial = (from tutorial in db.T_Tutorial.Where(w => w.VideoName != null)
                                                     join parent in db.T_Tutorial on tutorial.TutorialParent equals parent.TutorialCode into t
                                                     from rt in t.DefaultIfEmpty()
                                                     join changes in db.T_ContentChanges.Where(w => w.TutorialCode != null && !w.IsResponded) on tutorial.TutorialCode equals changes.TutorialCode into t2
                                                     from rt2 in t2.DefaultIfEmpty()
                                                     select new TutorialWithParent
                                                     {
                                                         Id = tutorial.Id.Value,
                                                         TutorialCode = tutorial.TutorialCode,
                                                         TutorialTitle = tutorial.TutorialTitle,
                                                         TutorialDescription = tutorial.TutorialDescription,
                                                         VideoName = tutorial.VideoName,
                                                         VideoPath = tutorial.VideoPath,
                                                         TutorialParent = tutorial.TutorialParent,
                                                         TutorialParentTitle = rt.TutorialTitle,
                                                         CreatedBy = tutorial.CreatedBy,
                                                         CreatedDate = tutorial.CreatedDate,
                                                         ModifyBy = tutorial.ModifyBy,
                                                         ModifyDate = tutorial.ModifyDate,
                                                         IsChanged = rt2.TutorialCode == null ? false : true
                                                     }).Where(w => !w.IsChanged).OrderBy(o => o.TutorialCode).ToList();

            if (stringSearch != null)
            {
                Session["listTutorialSearch"] = stringSearch;
            }

            string loadStringSearch = Session["listTutorialSearch"] != null ? Session["listTutorialSearch"].ToString() : "";

            if (loadStringSearch != "")
                stringSearch = loadStringSearch;
            else
                stringSearch = null;

            if (stringSearch != null)
                listTutorial = listTutorial.Where(w => (w.TutorialTitle.ToLower().Contains(stringSearch.ToLower()) || w.TutorialDescription.ToLower().Contains(stringSearch.ToLower()) || w.CreatedBy.ToLower().Contains(stringSearch.ToLower()) || (w.ModifyDate == null ? w.CreatedDate : w.ModifyDate.GetValueOrDefault()).ToString("dd MMMM yyyy").ToLower().Contains(stringSearch.ToLower())) || w.TutorialParent == null).ToList();


            int totalTutorial = db.T_Tutorial.Where(w => w.VideoName != null).Count();
            int totalPages = (listTutorial.Count / 20) + (listTutorial.Count % 20 > 0 ? 1 : 0);

            listTutorial = listTutorial.Skip(startIndex).Take(20).ToList();
            listTutorial = listTutorialGroup.Union(listTutorial).ToList();

            PagingAndGroup mod = new PagingAndGroup();
            mod.listTutorial = listTutorial;
            mod.stringSearch = stringSearch;
            mod.currentPage = loadPage + 1;
            mod.totalItem = totalTutorial;
            mod.totalPages = totalPages;
            mod.startIndexItem = startIndex + 1;

            mod = ogeneral.getPaging(mod);

            return View(mod);
        }

        [HttpGet]
        public ActionResult UploadTutorial()
        {
            List<DropdownModel> parentList = (from tutorial in db.T_Tutorial.Where(w => w.VideoName == null)
                                              select new DropdownModel
                                              {
                                                  stringValue = tutorial.TutorialCode,
                                                  Text = (tutorial.TutorialParent != null ? (tutorial.TutorialParent.Length > 1 ? (tutorial.TutorialParent.Length > 4 ? (tutorial.TutorialParent.Length > 7 ? (tutorial.TutorialParent.Length > 10 ? (tutorial.TutorialParent.Length > 13 ? "- - - - - - " + tutorial.TutorialTitle : "- - - - - " + tutorial.TutorialTitle) : "- - - - " + tutorial.TutorialTitle) : "- - - " + tutorial.TutorialTitle) : "- - " + tutorial.TutorialTitle) : "- " + tutorial.TutorialTitle) : tutorial.TutorialTitle)
                                              }).ToList();
            ViewBag.parentList = new SelectList(parentList, "stringValue", "Text");

            return View();
        }

        [HttpPost]
        public ActionResult UploadTutorial(T_Tutorial upload, HttpPostedFileBase uploadVideo)
        {
            if (uploadVideo != null)
            {
                if (uploadVideo.ContentLength > 0 && uploadVideo.ContentLength <= 20000000)
                {
                    string Year = DateTime.Now.Year.ToString(), Month = "0" + DateTime.Now.Month.ToString(), Day = "0" + DateTime.Now.Day.ToString(), Hour = "0" + DateTime.Now.Hour.ToString(), Minute = "0" + DateTime.Now.Minute.ToString(), Second = "0" + DateTime.Now.Second.ToString();
                    string Time = Year + Month.Substring(Month.Length - 2) + Day.Substring(Day.Length - 2) + "_" + Hour.Substring(Hour.Length - 2) + Minute.Substring(Minute.Length - 2) + Second.Substring(Second.Length - 2);

                    Directory.CreateDirectory(WebConfigurationManager.AppSettings["VideoPathFolder"] + Time);
                    List<T_MsApproval> listApproval = db.T_MsApproval.ToList();

                    int userLevel = int.Parse(Session["UserLevel"].ToString());

                    if (listApproval.Any(a => a.UserLevel == userLevel))
                    {
                        var videoname = Path.GetFileName(uploadVideo.FileName);
                        var path = Path.Combine(Server.MapPath("~/Content/tutorial/" + Time), videoname);
                        uploadVideo.SaveAs(path);

                        string lastTutorialCode = (from a in db.T_Tutorial.Where(w => w.TutorialParent == upload.TutorialParent).DefaultIfEmpty()
                                                   select new
                                                   {
                                                       value = a != null ? a.TutorialCode : "00"
                                                   }).OrderByDescending(o => o.value).Select(s => s.value).FirstOrDefault();
                        int addCode = int.Parse(lastTutorialCode.Substring(lastTutorialCode.LastIndexOf(".") + 1, 2)) + 1;
                        string TutorialCode = upload.TutorialParent + "." + ("0" + addCode.ToString()).Substring(("0" + addCode.ToString()).Length - 2);

                        T_Tutorial addTutorial = new T_Tutorial();
                        addTutorial.TutorialCode = TutorialCode;
                        addTutorial.TutorialTitle = upload.TutorialTitle;
                        addTutorial.TutorialDescription = upload.TutorialDescription;
                        addTutorial.TutorialParent = upload.TutorialParent;
                        addTutorial.VideoName = upload.VideoName;
                        addTutorial.VideoPath = "/Content/tutorial/" + Time + "/" + videoname;
                        addTutorial.CreatedBy = Session["LogedUserId"].ToString();
                        addTutorial.CreatedDate = DateTime.Now;
                        db.T_Tutorial.Add(addTutorial);
                        TempData["Message"] = "Tutorial Uploaded";
                    }
                    else
                    {
                        var videoname = Path.GetFileName(uploadVideo.FileName);
                        var path = Path.Combine(Server.MapPath("~/Content/tutorial/" + Time), videoname);
                        uploadVideo.SaveAs(path);

                        T_ContentChanges files = new T_ContentChanges();
                        files.Modul = "Tutorial";
                        files.Type = "Add";
                        files.TutorialTitle = upload.TutorialTitle;
                        files.TutorialDescription = upload.TutorialDescription;
                        files.VideoName = upload.VideoName;
                        files.VideoPath = "/Content/tutorial/" + Time + "/" + videoname; ;
                        files.TutorialParent = upload.TutorialParent;
                        files.PostBy = Session["LogedUserID"].ToString();
                        files.PostDate = DateTime.Now;
                        db.T_ContentChanges.Add(files);
                        TempData["Message"] = "Tutorial Uploaded, Waiting for Approval By SuperAdmin";
                    }

                    db.SaveChanges();
                    return RedirectToAction("ListTutorial");
                }
                else
                {
                    ModelState.AddModelError("VideoName", "Maximal video size is 20mb");
                }
            }
            else
            {
                ModelState.AddModelError("VideoName", "Please Choose Tutorial Video");
            }

            List<DropdownModel> parentList = (from tutorial in db.T_Tutorial.Where(w => w.VideoName == null)
                                              select new DropdownModel
                                              {
                                                  stringValue = tutorial.TutorialCode,
                                                  Text = (tutorial.TutorialParent != null ? (tutorial.TutorialParent.Length > 1 ? (tutorial.TutorialParent.Length > 4 ? (tutorial.TutorialParent.Length > 7 ? (tutorial.TutorialParent.Length > 10 ? (tutorial.TutorialParent.Length > 13 ? "- - - - - - " + tutorial.TutorialTitle : "- - - - - " + tutorial.TutorialTitle) : "- - - - " + tutorial.TutorialTitle) : "- - - " + tutorial.TutorialTitle) : "- - " + tutorial.TutorialTitle) : "- " + tutorial.TutorialTitle) : tutorial.TutorialTitle)
                                              }).ToList();
            ViewBag.parentList = new SelectList(parentList, "stringValue", "Text");

            return View(upload);
        }

        [HttpGet]
        public ActionResult CreateParent()
        {
            List<DropdownModel> parentList = (from tutorial in db.T_Tutorial.Where(w => w.VideoName == null)
                                              select new DropdownModel
                                              {
                                                  stringValue = tutorial.TutorialCode,
                                                  Text = (tutorial.TutorialParent != null ? (tutorial.TutorialParent.Length > 1 ? (tutorial.TutorialParent.Length > 4 ? (tutorial.TutorialParent.Length > 7 ? (tutorial.TutorialParent.Length > 10 ? (tutorial.TutorialParent.Length > 13 ? "- - - - - - " + tutorial.TutorialTitle : "- - - - - " + tutorial.TutorialTitle) : "- - - - " + tutorial.TutorialTitle) : "- - - " + tutorial.TutorialTitle) : "- - " + tutorial.TutorialTitle) : "- " + tutorial.TutorialTitle) : tutorial.TutorialTitle)
                                              }).ToList();
            ViewBag.parentList = new SelectList(parentList, "stringValue", "Text");

            return View();
        }

        [HttpPost]
        public ActionResult CreateParent(T_Tutorial newParent)
        {
            TempData["Message"] = "Tutorial Parent Created";

            string lastTutorialCode = (from a in db.T_Tutorial.Where(w => w.TutorialParent == newParent.TutorialParent).DefaultIfEmpty()
                                       select new
                                       {
                                           value = a != null ? a.TutorialCode : "00"
                                       }).OrderByDescending(o => o.value).Select(s => s.value).FirstOrDefault();
            int addCode = int.Parse(lastTutorialCode.Substring(lastTutorialCode.LastIndexOf(".") + 1, 2)) + 1;
            string TutorialCode = newParent.TutorialParent + "." + ("0" + addCode.ToString()).Substring(("0" + addCode.ToString()).Length - 2);

            T_Tutorial addParent = new T_Tutorial();
            addParent.TutorialCode = TutorialCode;
            addParent.TutorialTitle = newParent.TutorialTitle;
            addParent.TutorialDescription = newParent.TutorialDescription;
            addParent.TutorialParent = newParent.TutorialParent;
            addParent.CreatedBy = Session["LogedUserId"].ToString();
            addParent.CreatedDate = DateTime.Now;
            db.T_Tutorial.Add(addParent);
            db.SaveChanges();

            List<DropdownModel> parentList = (from tutorial in db.T_Tutorial.Where(w => w.VideoName == null)
                                              select new DropdownModel
                                              {
                                                  stringValue = tutorial.TutorialCode,
                                                  Text = (tutorial.TutorialParent != null ? (tutorial.TutorialParent.Length > 1 ? (tutorial.TutorialParent.Length > 4 ? (tutorial.TutorialParent.Length > 7 ? (tutorial.TutorialParent.Length > 10 ? (tutorial.TutorialParent.Length > 13 ? "- - - - - - " + tutorial.TutorialTitle : "- - - - - " + tutorial.TutorialTitle) : "- - - - " + tutorial.TutorialTitle) : "- - - " + tutorial.TutorialTitle) : "- - " + tutorial.TutorialTitle) : "- " + tutorial.TutorialTitle) : tutorial.TutorialTitle)
                                              }).ToList();
            ViewBag.parentList = new SelectList(parentList, "stringValue", "Text");

            return RedirectToAction("CreateParent");
        }

        [HttpPost]
        public ActionResult SubmitParent(T_Tutorial newParent)
        {
            TempData["Message"] = "Tutorial Parent Created";

            string lastTutorialCode = (from a in db.T_Tutorial.Where(w => w.TutorialParent == newParent.TutorialParent).DefaultIfEmpty()
                                       select new
                                       {
                                           value = a != null ? a.TutorialCode : "00"
                                       }).OrderByDescending(o => o.value).Select(s => s.value).FirstOrDefault();
            int addCode = int.Parse(lastTutorialCode.Substring(lastTutorialCode.LastIndexOf(".") + 1, 2)) + 1;
            string TutorialCode = newParent.TutorialParent + "." + ("0" + addCode.ToString()).Substring(("0" + addCode.ToString()).Length - 2);

            T_Tutorial addParent = new T_Tutorial();
            addParent.TutorialCode = TutorialCode;
            addParent.TutorialTitle = newParent.TutorialTitle;
            addParent.TutorialDescription = newParent.TutorialDescription;
            addParent.TutorialParent = newParent.TutorialParent;
            addParent.CreatedBy = Session["LogedUserId"].ToString();
            addParent.CreatedDate = DateTime.Now;
            db.T_Tutorial.Add(addParent);
            db.SaveChanges();

            return RedirectToAction("UploadTutorial");
        }

        [HttpGet]
        public ActionResult EditTutorial(string tutorialCode)
        {
            T_Tutorial data = db.T_Tutorial.Where(w => w.TutorialCode == tutorialCode).FirstOrDefault();

            List<DropdownModel> parentList = (from tutorial in db.T_Tutorial.Where(w => w.VideoName == null)
                                              select new DropdownModel
                                              {
                                                  stringValue = tutorial.TutorialCode,
                                                  Text = (tutorial.TutorialParent != null ? (tutorial.TutorialParent.Length > 1 ? (tutorial.TutorialParent.Length > 4 ? (tutorial.TutorialParent.Length > 7 ? (tutorial.TutorialParent.Length > 10 ? (tutorial.TutorialParent.Length > 13 ? "- - - - - - " + tutorial.TutorialTitle : "- - - - - " + tutorial.TutorialTitle) : "- - - - " + tutorial.TutorialTitle) : "- - - " + tutorial.TutorialTitle) : "- - " + tutorial.TutorialTitle) : "- " + tutorial.TutorialTitle) : tutorial.TutorialTitle)
                                              }).ToList();
            ViewBag.parentList = new SelectList(parentList, "stringValue", "Text");

            return View(data);
        }

        [HttpPost]
        public ActionResult EditTutorial(T_Tutorial tutorial, HttpPostedFileBase uploadVideo)
        {
            var dbTutorial = db.T_Tutorial.Where(a => a.TutorialCode.Equals(tutorial.TutorialCode)).FirstOrDefault();

            string Year = DateTime.Now.Year.ToString(), Month = "0" + DateTime.Now.Month.ToString(), Day = "0" + DateTime.Now.Day.ToString(), Hour = "0" + DateTime.Now.Hour.ToString(), Minute = "0" + DateTime.Now.Minute.ToString(), Second = "0" + DateTime.Now.Second.ToString();
            string Time = Year + Month.Substring(Month.Length - 2) + Day.Substring(Day.Length - 2) + "_" + Hour.Substring(Hour.Length - 2) + Minute.Substring(Minute.Length - 2) + Second.Substring(Second.Length - 2);

            List<T_MsApproval> listApproval = db.T_MsApproval.ToList();

            int userLevel = int.Parse(Session["UserLevel"].ToString());

            if (listApproval.Any(a => a.UserLevel == userLevel))
            {
                string lastTutorialCode = (from a in db.T_Tutorial.Where(w => w.TutorialParent == tutorial.TutorialParent).DefaultIfEmpty()
                                           select new
                                           {
                                               value = a != null ? a.TutorialCode : "00"
                                           }).OrderByDescending(o => o.value).Select(s => s.value).FirstOrDefault();
                int addCode = int.Parse(lastTutorialCode.Substring(lastTutorialCode.LastIndexOf(".") + 1, 2)) + 1;
                string TutorialCode = tutorial.TutorialParent + "." + ("0" + addCode.ToString()).Substring(("0" + addCode.ToString()).Length - 2);

                T_Tutorial addTutorial = new T_Tutorial();
                addTutorial.TutorialCode = TutorialCode;
                addTutorial.TutorialTitle = tutorial.TutorialTitle;
                addTutorial.TutorialDescription = tutorial.TutorialDescription;
                addTutorial.TutorialParent = tutorial.TutorialParent;
                addTutorial.CreatedBy = dbTutorial.CreatedBy;
                addTutorial.CreatedDate = dbTutorial.CreatedDate;
                addTutorial.ModifyBy = Session["LogedUserId"].ToString();
                addTutorial.ModifyDate = DateTime.Now;

                if (uploadVideo != null)
                {
                    Directory.CreateDirectory(WebConfigurationManager.AppSettings["VideoPathFolder"] + Time);
                    System.IO.File.Delete(WebConfigurationManager.AppSettings["VideoPathFolder"] + dbTutorial.VideoPath.Substring(18, 16) + dbTutorial.VideoName);
                    Directory.Delete(WebConfigurationManager.AppSettings["VideoPathFolder"] + dbTutorial.VideoPath.Substring(18, 15));

                    var videoname = Path.GetFileName(uploadVideo.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/tutorial/" + Time), videoname);
                    uploadVideo.SaveAs(path);

                    addTutorial.VideoName = videoname;
                    addTutorial.VideoPath = "/Content/tutorial/" + Time + "/" + videoname;
                }
                else
                {
                    addTutorial.VideoName = dbTutorial.VideoName;
                    addTutorial.VideoPath = dbTutorial.VideoPath;
                }

                db.T_Tutorial.Add(addTutorial);
                db.T_Tutorial.Remove(dbTutorial);
                TempData["Message"] = "Tutorial Updated";
            }
            else
            {
                T_ContentChanges changes = new T_ContentChanges();
                changes.Modul = "Tutorial";
                changes.Type = "Update";
                changes.TutorialCode = tutorial.TutorialCode;
                changes.TutorialTitle = tutorial.TutorialTitle;
                changes.TutorialDescription = tutorial.TutorialDescription;
                changes.TutorialParent = tutorial.TutorialParent;
                changes.PostBy = Session["LogedUserID"].ToString();
                changes.PostDate = DateTime.Now;

                if (uploadVideo != null)
                {
                    Directory.CreateDirectory(WebConfigurationManager.AppSettings["VideoPathFolder"] + Time);

                    var videoname = Path.GetFileName(uploadVideo.FileName);
                    var videopath = Path.Combine(Server.MapPath("~/Content/tutorial/" + Time), videoname);
                    uploadVideo.SaveAs(videopath);

                    changes.VideoName = videoname;
                    changes.VideoPath = "/Content/tutorial/" + Time + "/" + videoname;
                }
                else
                {
                    changes.VideoName = dbTutorial.VideoName;
                    changes.VideoPath = dbTutorial.VideoPath;
                }

                db.T_ContentChanges.Add(changes);
                TempData["Message"] = "Tutorial Updated, Waiting for Approval By SuperAdmin";
            }

            db.SaveChanges();
            return RedirectToAction("ListTutorial");
        }

        public ActionResult DeleteTutorial(string tutorialCode)
        {
            var dbTutorial = db.T_Tutorial.Where(a => a.TutorialCode == tutorialCode).FirstOrDefault();
            List<T_MsApproval> listApproval = db.T_MsApproval.ToList();

            int userLevel = int.Parse(Session["UserLevel"].ToString());

            if (listApproval.Any(a => a.UserLevel == userLevel))
            {
                System.IO.File.Delete(WebConfigurationManager.AppSettings["VideoPathFolder"] + dbTutorial.VideoPath.Substring(18, 16) + dbTutorial.VideoName);
                Directory.Delete(WebConfigurationManager.AppSettings["VideoPathFolder"] + dbTutorial.VideoPath.Substring(18, 15));

                db.T_Tutorial.Remove(dbTutorial);
                TempData["Message"] = "Tutorial Deleted";
            }
            else
            {
                T_ContentChanges changes = new T_ContentChanges();
                changes.Modul = "Tutorial";
                changes.Type = "Delete";
                changes.TutorialCode = dbTutorial.TutorialCode;
                changes.TutorialTitle = dbTutorial.TutorialTitle;
                changes.TutorialDescription = dbTutorial.TutorialDescription;
                changes.TutorialParent = dbTutorial.TutorialParent;
                changes.VideoName = dbTutorial.VideoName;
                changes.VideoPath = dbTutorial.VideoPath;
                changes.PostBy = Session["LogedUserID"].ToString();
                changes.PostDate = DateTime.Now;

                db.T_ContentChanges.Add(changes);
                TempData["Message"] = "Tutorial Deleted, Waiting for Approval By SuperAdmin";
            }

            db.SaveChanges();
            return RedirectToAction("ListTutorial");
        }


        [HttpGet]
        public ActionResult Test()
        {
            List<T_MsMenu> listMenu = db.T_MsMenu.ToList();

            return View(listMenu);
        }

        //[HttpPost]
        //public JsonResult Test(Test test)
        //{
        //    Test _model = new Test();
        //    return Json(new { Html = _model }, JsonRequestBehavior.AllowGet);

        //    //if (test.RoleName == null || test.RoleName.Trim().Length == 0)
        //    //{
        //    //    _model = new RequestResultModel();
        //    //    _model.Title = "Warning";
        //    //    _model.Message = "Role Name is empty. Please, enter role name.";
        //    //    _model.InfoType = RequestResultInfoType.Warning;

        //    //    return Json(new { Html = _model }, JsonRequestBehavior.AllowGet);
        //    //}

        //    //Role roleOldValue = new Role();
        //    //string SaveMode = "Insert";

        //    //if (Info.RoleID > 0)
        //    //{
        //    //    SaveMode = "Update";
        //    //    roleOldValue = Roles.GetById(Info.RoleID);

        //    //    Role role = Roles.GetById(Info.RoleID);
        //    //    List<Role> roleExists = Roles.GetByName(Info.RoleName);

        //    //    if (role.IsDeleted == true)
        //    //    {
        //    //        _model = new RequestResultModel();
        //    //        _model.Title = "Warning";
        //    //        _model.Message = String.Format("Data has been deleted by '{0}'.", role.ModifiedBy);
        //    //        _model.InfoType = RequestResultInfoType.Warning;

        //    //        return Json(new { Html = _model, IsClose = "close" }, JsonRequestBehavior.AllowGet);
        //    //    }

        //    //    bool valid = roleExists.Where(x => x.RoleID != Info.RoleID).Count() > 0 ? false : true;
        //    //    if (!valid)
        //    //    {
        //    //        _model = new RequestResultModel();
        //    //        _model.Title = "Warning";
        //    //        _model.Message = String.Format("'{0}' already exists. Please, change role name and try again.", roleExists.FirstOrDefault().Name.ToString());
        //    //        _model.InfoType = RequestResultInfoType.Warning;

        //    //        return Json(new { Html = _model }, JsonRequestBehavior.AllowGet);
        //    //    }
        //    //    using (TransactionScope scope = new TransactionScope())
        //    //    {
        //    //        role.Name = Info.RoleName;
        //    //        role.CreatedBy = HttpContext.Session["GGI"].ToString();
        //    //        role.ModifiedBy = HttpContext.Session["GGI"].ToString();

        //    //        role.Save();
        //    //        Save_AuditTrail_Role(SaveMode, roleOldValue, role);

        //    //        SaveRoleActivity(Info, role.RoleID);

        //    //        _model.Message = String.Format("'{0}' has been updated.", role.Name.ToString());
        //    //        _model.InfoType = RequestResultInfoType.Success;

        //    //        scope.Complete();
        //    //        scope.Dispose();
        //    //    }

        //    //    return Json(new { Html = _model }, JsonRequestBehavior.AllowGet);
        //    //}
        //    //else
        //    //{
        //    //    List<Role> roleExists = Roles.GetByName(Info.RoleName);

        //    //    if (roleExists.Count() > 0)
        //    //    {
        //    //        _model.Title = "Warning";
        //    //        _model.Message = String.Format("'{0}' already exists. Please, change Role Name and try again.", roleExists.FirstOrDefault().Name.ToString());
        //    //        _model.InfoType = RequestResultInfoType.Warning;

        //    //        return Json(new { Html = _model }, JsonRequestBehavior.AllowGet);
        //    //    }

        //    //    using (TransactionScope scope = new TransactionScope())
        //    //    {
        //    //        Role role = new Role();
        //    //        role.Name = Info.RoleName;
        //    //        role.CreatedBy = HttpContext.Session["GGI"].ToString();
        //    //        role.ModifiedBy = HttpContext.Session["GGI"].ToString();

        //    //        role.Save();

        //    //        Save_AuditTrail_Role(SaveMode, roleOldValue, role);

        //    //        SaveRoleActivity(Info, role.RoleID);

        //    //        _model.Message = String.Format("Role Name '{0}' has been created.", role.Name.ToString());
        //    //        _model.InfoType = RequestResultInfoType.Success;

        //    //        scope.Complete();
        //    //        scope.Dispose();
        //    //    }
        //    //    return Json(new { Html = _model }, JsonRequestBehavior.AllowGet);
        //    //}
        //}

    }
}
