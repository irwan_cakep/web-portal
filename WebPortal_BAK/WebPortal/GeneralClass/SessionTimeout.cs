﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebPortal.GeneralClass
{
    public class SessionTimeoutAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            if (HttpContext.Current.Session["LogedUserId"] == null)
            {
                filterContext.Result = new RedirectResult("~/Home/Index");
                HttpContext.Current.Session["SessionStatus"] = "Your Session is Over, Please Login Again";
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}