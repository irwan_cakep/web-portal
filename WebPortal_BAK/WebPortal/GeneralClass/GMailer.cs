﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace WebPortal.GeneralClass
{
    public class GMailer
    {
        public static string GmailUsername { get; set; }
        public static string GmailPassword { get; set; }
        public static string GmailHost { get; set; }
        public static int GmailPort { get; set; }
        public static bool GmailSSL { get; set; }

        public string ToEmail { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsHtml { get; set; }

        static GMailer()
        {
            GmailHost = "smtp.gmail.com";
            GmailPort = 25; // Gmail can use ports 25, 465 & 587; but must be 25 for medium trust environment.
            GmailSSL = true;
        }

        public void Send()
        {
            SmtpClient smtp = new SmtpClient();
            smtp.Host = GmailHost;
            smtp.Port = GmailPort;
            smtp.EnableSsl = GmailSSL;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(GmailUsername, GmailPassword);

            using (var message = new MailMessage(GmailUsername, ToEmail))
            {
                message.Subject = Subject;
                message.Body = Body;
                message.IsBodyHtml = IsHtml;
                smtp.Send(message);
            }
        }

        public void SendEmail()
        {
            GMailer.GmailUsername = "suwandri.lie@first-resources.int";
            GMailer.GmailPassword = "eee12345";

            GMailer mailer = new GMailer();
            mailer.ToEmail = "suwandri.lie@first-resources.int";
            mailer.Subject = "Verify your email id";
            mailer.Body = "Thanks for Registering your account";
            mailer.IsHtml = true;
            mailer.Send();
        }


        public void test(string name, string recipient, string address, string email, string info)
        {
            MailMessage mailMsg = new MailMessage();
            mailMsg.To.Add(new MailAddress(email));
            // From
            MailAddress mailAddress = new MailAddress("user@live.com");
            mailMsg.From = mailAddress;

            //Content
            mailMsg.Subject = "Secret Santa";
            mailMsg.Body = name + ", your target is " + recipient + ". Please spread the holiday cheer with a soft cap of $20! From an automated mail service";

            //SmtpClient
            SmtpClient smtpConnection = new SmtpClient("smtp.live.com", 587);
            smtpConnection.Credentials = new System.Net.NetworkCredential("user@live.com", "password");
            smtpConnection.UseDefaultCredentials = false;

            smtpConnection.EnableSsl = true;
            smtpConnection.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpConnection.Send(mailMsg);
        }
    }
}