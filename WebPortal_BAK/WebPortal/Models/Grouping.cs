﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPortal.Models
{
    public class Grouping
    {
        public int CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public int TotalNews { get; set; }

        public string ProjectCode { get; set; }
        public string TutorialProject { get; set; }
        public int TotalTutorial { get; set; }
    }
}