﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPortal.Model;

namespace WebPortal.Models
{
    public class AjaxReturn
    {
        public bool Status { get; set; }
        public string Message { get; set; }

        public string GeneratedString { get; set; }
        public bool AccountStatus { get; set; }

        public List<DropdownModel> listDropdownData { get; set; }

        public T_Recruitment_Family recruitmentFamily { get; set; }
        public T_Recruitment_Academy recruitmentAcademy { get; set; }
        public T_Recruitment_Academy_NonFormal recruitmentAcademy_NonFormal { get; set; }
        public T_Recruitment_Experience recruitmentExperience { get; set; }
        public T_Recruitment_Document recruitmentDocument { get; set; }

        public T_Vendor_Bank vendorBank { get; set; }
        public T_Vendor_Branches vendorBranches { get; set; }
        public T_Vendor_Document vendorDocument { get; set; }
        public T_Vendor_Item vendorItem { get; set; }
    }
}