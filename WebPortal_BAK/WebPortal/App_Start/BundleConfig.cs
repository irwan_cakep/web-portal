﻿using System.Web;
using System.Web.Optimization;

namespace WebPortal
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery.customvalidation.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/popper.js",
                        "~/Scripts/bootstrap-notify.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/CSS").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/bootstrap-notify.css",
                        "~/Content/Site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));


            // Bootstrap Bundles
            bundles.Add(new StyleBundle("~/Design/assets/css").Include(
                        "~/Design/assets/css/bootstrap.css",
                        "~/Design/assets/css/bootstrap-responsive.css",
                        "~/Design/assets/css/fancybox/jquery.fancybox.css",
                        "~/Design/assets/css/jcarousel.css",
                        "~/Design/assets/css/flexslider.css",
                        "~/Design/assets/css/style.css"));

            // Bootsnipp Bundles
            bundles.Add(new StyleBundle("~/Design/assets/css-bootsnipp").Include(
                        "~/Design/assets/css/bootsnipp.css",
                        "~/Design/assets/css/bootsnipp-style.css"));

            // Bootsnipp Banner Bundles
            bundles.Add(new StyleBundle("~/Design/assets/css-bootsnipp-banner").Include(
                        "~/Design/assets/css/bootsnipp-banner.css"));

            bundles.Add(new StyleBundle("~/Design/assets/skins").Include(
                        "~/Design/assets/skins/default.css",
                        "~/Design/assets/css/jquery.ui.core.css",
                        "~/Design/assets/css/jquery.ui.datepicker.css",
                        "~/Design/assets/css/jquery.ui.theme.css"));

            bundles.Add(new ScriptBundle("~/Design/assets/js").Include(
                        "~/Design/assets/js/jquery.js",
                        "~/Design/assets/js/jquery.easing.1.3.js",
                        "~/Design/assets/js/bootstrap.js",
                        "~/Design/assets/js/bootstrap-notify.js",
                        "~/Design/assets/js/jcarousel/jquery.jcarousel.js",
                        "~/Design/assets/js/jquery.fancybox.pack.js",
                        "~/Design/assets/js/jquery.fancybox-media.js",
                        "~/Design/assets/js/google-code-prettify/prettify.js",
                        "~/Design/assets/js/portfolio/jquery.quicksand.js",
                        "~/Design/assets/js/portfolio/setting.js",
                        "~/Design/assets/js/jquery.flexslider.js",
                        "~/Design/assets/js/jquery.nivo.slider.js",
                        "~/Design/assets/js/modernizr.custom.js",
                        "~/Design/assets/js/jquery.ba-cond.js",
                        "~/Design/assets/js/jquery.slitslider.js",
                        "~/Design/assets/js/animate.js",
                        "~/Design/assets/js/jquery-1.8.2.js",
                        "~/Design/assets/js/jquery.ui.core.js",
                        "~/Design/assets/js/jquery.ui.datepicker.js",
                        "~/Design/assets/js/jquery.ui.widget.js"));

            bundles.Add(new ScriptBundle("~/Design/assets/js-custom").Include(
                        "~/Design/assets/js/custom.js"));
        }
    }
}