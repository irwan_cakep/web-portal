﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPortal.Model
{
    class Metadata
    {
    }

    public class T_FilesMetaData
    {

        public int ID { get; set; }

        [Required(ErrorMessage = "Please Choose File")]
        public string NameFile { get; set; }

        [Required(ErrorMessage = "Please Fill File Description")]
        public string Description { get; set; }

        public string LocationFile { get; set; }

        [Required(ErrorMessage = "Please Choose File Group")]
        public int FileGroup { get; set; }

        public string CreatedBy { get; set; }

        public Nullable<System.DateTime> CreatedDate { get; set; }

    }

    public class T_CommentNewsMetaData
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public string Email { get; set; }

        [Required(ErrorMessage = "Please Fill Comment")]
        public string Field { get; set; }

        public string IdComment { get; set; }

        public DateTime Date { get; set; }
    }

    public class T_NewsMetaData
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please Fill News Title")]
        public string Judul { get; set; }

        [Required(ErrorMessage = "Please Fill News Caption")]
        public string Caption { get; set; }

        [Required(ErrorMessage = "Please Choose News Category")]
        public int CategoryCode { get; set; }

        [Required(ErrorMessage = "Please Fill News Content")]
        public string Isi { get; set; }

        [Required(ErrorMessage = "Please Choose News Image"), FileExtensions(Extensions = ".jpg,.png,.jpeg,.bmp", ErrorMessage = "Incorrect File Format")]
        public string Image { get; set; }

        public string Attach { get; set; }

        public string PostBy { get; set; }

        public string CreatedBy { get; set; }

        public Nullable<System.DateTime> CreatedDate { get; set; }
    }

    public class T_TutorialMetaData
    {
        public int Id { get; set; }

        public string TutorialCode { get; set; }

        [Required(ErrorMessage = "Please Fill Tutorial Title")]
        public string TutorialTitle { get; set; }

        [Required(ErrorMessage = "Please Fill Tutorial Description")]
        public string TutorialDescription { get; set; }

        [FileExtensions(Extensions = ".mp4,.wmv", ErrorMessage = "Incorrect File Format")]
        public string VideoName { get; set; }

        public string VideoPath { get; set; }

        [Required(ErrorMessage = "Please Choose Tutorial Parent")]
        public string TutorialParent { get; set; }

        public string CreatedBy { get; set; }

        public Nullable<System.DateTime> CreatedDate { get; set; }
    }

    public class T_MsUserMetaData
    {
        [Required(ErrorMessage = "Please Fill User ID")]
        public string UserId { get; set; }

        [Required(ErrorMessage = "Please Fill User Name")]
        public string UserName { get; set; }

        public string Password { get; set; }

        public string Region { get; set; }

        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        public string email { get; set; }

        public string Jabatan { get; set; }

        public string Divisi { get; set; }

        [Required(ErrorMessage = "Please Choose User Level")]
        public int UserLevel { get; set; }

        public string CreatedBy { get; set; }

        public Nullable<System.DateTime> CreatedDate { get; set; }

        public string Image { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Date is not valid")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? StartAccess { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Date is not valid")]
        public DateTime? EndAccess { get; set; }
    }

    public class T_MsProjectMetaData
    {
        [Required(ErrorMessage = "Please Fill Project Code")]
        public string ProjectCode { get; set; }

        [Required(ErrorMessage = "Please Fill Project Name")]
        public string ProjectName { get; set; }

        public string CreatedBy { get; set; }
        
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }

    public class T_FAQMetaData
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please Choose Project")]
        public string ProjectCode { get; set; }
        
        public string TypeFAQ { get; set; }

        [Required(ErrorMessage = "Please Choose Group FAQ")]
        public string GroupFAQ { get; set; }

        [Required(ErrorMessage = "Please Fill FAQ Question")]
        public string Q { get; set; }

        [Required(ErrorMessage = "Please Fill FAQ Answer")]
        public string A { get; set; }
        
        public string CreatedBy { get; set; }
        
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }
}
